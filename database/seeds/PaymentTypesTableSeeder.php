<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET foreign_key_checks=0');
        DB::table('payment_types')->truncate();
        DB::statement('SET foreign_key_checks=1');
        $records = [
            ['code' => 0, 'name' => 'Numerar'],
            ['code' => 1, 'name' => 'Card'],
            ['code' => 11, 'name' => 'Certificat'],
            ['code' => 2, 'name' => 'Transfer Bank'],
        ];
        DB::table('payment_types')->insert($records);
    }
    
}
