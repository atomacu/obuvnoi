<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET foreign_key_checks=0');
        DB::table('settings')->truncate();
        DB::statement('SET foreign_key_checks=1');
        $records = [
            ['name' => 'logoff_timer', 'label' => 'LogOff таймер', 'value' => '500', 'type' => 'counter', 'activated' => 1],
            ['name' => 'round_lei', 'label' => 'Округлять до 5 лей', 'value' => '1', 'type' => 'checkbox', 'activated' => 1],
            ['name' => 'multiply_payment', 'label' => 'Смешанная оплата', 'value' => '1', 'type' => 'checkbox', 'activated' => 1],
            ['name' => 'print_copies', 'label' => 'Количество копий чеков', 'value' => '2', 'type' => 'counter', 'activated' => 1],
            ['name' => 'shift_z_report', 'label' => 'Z отчёт при закрытии смены', 'value' => '1', 'type' => 'checkbox', 'activated' => 1],
            ['name' => 'shift_pause', 'label' => 'Пауза при закрытии смены', 'value' => '4', 'type' => 'counter', 'activated' => 1],
            ['name' => 'close_order_without_kkm', 'label' => 'Закрывать чек без кассового аппарата', 'value' => '1', 'type' => 'checkbox', 'activated' => 1],
            ['name' => 'company_name', 'label' => 'Название компании', 'value' => '', 'type' => 'textarea', 'activated' => 1],
            ['name' => 'company_address', 'label' => 'Адрес компании', 'value' => '', 'type' => 'textarea', 'activated' => 1],
            ['name' => 'last_sync', 'label' => 'Last sync', 'value' => '', 'type' => '', 'activated' => 0],
        ];

        DB::table('settings')->insert($records);
    }
}
