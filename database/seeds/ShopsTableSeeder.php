<?php

use Illuminate\Database\Seeder;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks=0');
        DB::table('shops')->truncate();
        DB::statement('SET foreign_key_checks=1');
        $records = [
            ['name' => 'Shop', 'dept_uuid' => 'D5SZ5CFA', 'address' => '', 'activated' => 1],
        ];

        DB::table('shops')->insert($records);
    }
}
