<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks=0');
        DB::table('users')->truncate();
        DB::statement('SET foreign_key_checks=1');
        $records = [
            ['name' => 'Администратор', 'password' => app('hash')->make('0000'), 'permission' => 2, 'activated' => 1],
        ];

        DB::table('users')->insert($records);
    }
}
