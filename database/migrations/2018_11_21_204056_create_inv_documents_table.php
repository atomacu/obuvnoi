<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('sections');
            $table->integer('from_section_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            //$table->foreign('user_id')->references('id')->on('users');
            $table->integer('received_user_id')->unsigned()->nullable();
            //$table->foreign('received_user_id')->references('id')->on('users');
            $table->timestamp('date_at')->nullable();
            $table->char('number');
            $table->text('notes')->nullable();
            $table->char('type', 20);
            $table->boolean('local')->default(false);
            $table->boolean('synced')->default(false);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_documents');
    }
}
