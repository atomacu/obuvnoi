<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvDocumentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_document_products', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('document_id')->unsigned();
            $table->foreign('document_id')->references('id')->on('inv_documents');
            $table->integer('serial_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->tinyInteger('quantity_res');
            $table->tinyInteger('quantity');
            $table->boolean('scanned')->default(0);
            $table->timestamp('confirm_at')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_document_products');
    }
}
