<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;
class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->uuid('order_detail_uuid');
            $table->integer('serial_id')->unsigned()->nullable();
            //$table->foreign('serial_id')->references('id')->on('serials');
            $table->integer('product_id')->unsigned()->nullable();
            //$table->foreign('product_id')->references('id')->on('products');
            $table->decimal('price', 8, 2);
            $table->integer('quantity');
            $table->decimal('discount_percent', 8, 2)->nullable();
            $table->decimal('discount_amount', 8, 2)->nullable();
            $table->decimal('sum', 8, 2);
            $table->string('model');
            $table->string('barcode');
            $table->string('color');
            $table->string('size');
            $table->uuid('color_uuid');
            $table->uuid('size_uuid');
            $table->decimal('total', 8, 2);
            $table->string('consultant')->nullable();
            $table->integer('consultant_id')->unsigned()->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
