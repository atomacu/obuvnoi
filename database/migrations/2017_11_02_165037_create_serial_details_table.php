<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerialDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serial_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('serial_id')->unsigned();
            $table->foreign('serial_id')->references('id')->on('serials');
            $table->decimal('weight', 8, 2);
            $table->decimal('price_gramm', 8, 2);
            $table->decimal('size', 8, 2);
            $table->string('color', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serial_details');
    }
}
