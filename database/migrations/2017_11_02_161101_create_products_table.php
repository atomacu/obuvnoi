<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->uuid('uuid')->index();
            $table->uuid('parent_uuid')->nullable();
            $table->tinyInteger('product_type')->default(1);
            $table->string('name');
            $table->string('barcode', 32);
            $table->string('vendorcode', 32);
            $table->decimal('price', 8, 2);
            $table->boolean('check_serial')->default(1);
            $table->boolean('allow_discount')->default(1);
            $table->tinyInteger('vat');
            $table->tinyInteger('is_folder');
            $table->boolean('activated')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
