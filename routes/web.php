<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return view('home');
});
$router->get('/reports', function () use ($router) {
    return view('reports');
});
$router->post('/login', 'AuthController@postLogin');
$router->get('/cashbox', function () use ($router) {
    return view('cashbox');
});


$router->get('/pic/{id}', 'PictureController@showPicture');
$router->post('/addpic', 'PictureController@savePicture');
$router->put('/delpic/{id}', 'PictureController@deletePicture');


$router->get('/orders/{order_id}/print', 'OrderController@printOrder');
$router->get('/data/shops', 'DataController@getShops');

$router->get('/get-settings', 'AppController@getSettings');
$router->get('/data/products', 'DataController@products');
$router->get('/data/tree/{product_id}', 'DataController@treeElement');
$router->get('/data/tree/', 'DataController@tree');
$router->get('/data/consultants', 'DataController@getConsultants');
$router->get('/data/payment-types', 'DataController@paymentTypes');
$router->get('/data/sections', 'DataController@getSections');
$router->get('/data/serials/{product_id}', 'DataController@serials');
$router->get('/data/clients[/{idno}]', 'DataController@clients');
$router->post('/data/serials', 'DataController@getSerial');
$router->post('/data/getbybarcode', 'DataController@getbybarcode');
$router->post('/data/discount-card', 'DataController@getDiscountCard');
$router->post('/data/certificate', 'DataController@getCertificate');
$router->post('/data/check-stocks', 'DataController@checkStocks');

$router->get('/data/recepts', 'DataController@Recepts');
$router->get('/data/receptsOrder/{id}', 'DataController@ReceptsOrder');
$router->post('/data/recepts', 'DataController@ReceptsStore');
$router->put('/data/recepts/{id}', 'DataController@ReceptsUpdate');

$router->get('/data/colors', 'DataController@Colors');
$router->get('/data/productColors/{id}', 'DataController@ProductColors');

$router->get('/data/sizes', 'DataController@Sizes');
$router->get('/data/productSizes/{id}', 'DataController@ProductSizes');

$router->post('/report', 'ReportController@index');
$router->post('/report/print', 'ReportController@print');

$router->group(['middleware' => 'jwt.auth'], function ($router) {
    $router->post('/logout', 'AuthController@postLogout');
    $router->get('/account', function (Request $request) {
        return $request->user();
    });

    $router->get('/orders', 'OrderController@index');
    $router->post('/orders[/]', 'OrderController@store');
    $router->put('/orders/{id}', 'OrderController@update');
    $router->post('/orders/{id}', 'OrderController@close');
    $router->get('/order/{id}', 'OrderController@findOrder');
    $router->post('/orders/{order_id}/print', 'OrderController@printOrder');
    $router->post('/search-barcode[/]', 'OrderController@searchBarcode');

    $router->get('/order-details/{order_id}', 'OrderController@orderDetails');
    $router->post('/order-details[/]', 'OrderController@orderDetailsStore');
    $router->put('/order-details/{id}', 'OrderController@orderDetailsUpdate');
    $router->delete('/order-details/{id}', 'OrderController@orderDetailsRemove');

    $router->get('/order-payments/{order_id}', 'OrderController@orderPayments');
    $router->post('/order-payments', 'OrderController@orderPaymentsStore');
    $router->post('/order-payments-print-reciep/{payment_id}', 'OrderController@orderPaymentsPrintReciep');
    $router->post('/order-payments-print-fiscal/{payment_id}', 'OrderController@orderPaymentsPrintFiscal');
    $router->put('/order-payments/{id}', 'OrderController@orderPaymentsUpdate');
    $router->delete('/order-payments/{id}', 'OrderController@orderPaymentsRemove');

    $router->post('/print-fiscal', 'AppController@printFiscal');
    $router->post('/close-shift', 'AppController@closeShift');
    $router->post('/accounts-journal', 'ReportController@accountsJournal');
    $router->post('/recept-journal', 'ReportController@receptJournal');
    $router->get('/get-unsynced-orders', 'AppController@getUnsyncedOrders');

});

//Inventory
$router->get('/inv/documents/{type}[/{section_id}]', 'InventoryController@documents');
$router->get('/inv/products/{document_id}', 'InventoryController@products');
$router->post('/inv/document/{type}', 'InventoryController@storeDocument');
$router->delete('/inv/document', 'InventoryController@removeDocument');
$router->post('/inv/confirm-document', 'InventoryController@confirmDocument');
$router->post('/inv/record', 'InventoryController@storeRecord');
$router->put('/inv/record', 'InventoryController@updateRecord');


$router->group(['prefix' => 'admin'], function ($router) {
    $router->get('/account', function (Request $request) {
        if (!$request->user() || !$request->user()->isAdmin()) {
            return response('You have no permissions.', 401);
        }
        return $request->user();
    });

    $router->get('/', 'Admin\AdminController@index');

    $router->get('/shops', 'Admin\ShopController@index');
    $router->get('/shops/data', 'Admin\ShopController@getData');
    $router->post('/shops/data[/]', 'Admin\ShopController@addData');
    $router->put('/shops/data/{id}', 'Admin\ShopController@updateData');
    $router->delete('/shops/data/{id}', 'Admin\ShopController@removeData');

    $router->get('/users', 'Admin\UserController@index');
    $router->get('/users/data', 'Admin\UserController@getData');
    $router->post('/users/data[/]', 'Admin\UserController@addData');
    $router->put('/users/data/{id}', 'Admin\UserController@updateData');
    $router->delete('/users/data/{id}', 'Admin\UserController@removeData');

    $router->get('/printers', 'Admin\PrinterController@index');
    $router->get('/printers/data', 'Admin\PrinterController@getData');
    $router->post('/printers/data[/]', 'Admin\PrinterController@addData');
    $router->put('/printers/data/{id}', 'Admin\PrinterController@updateData');
    $router->delete('/printers/data/{id}', 'Admin\PrinterController@removeData');

    $router->get('/discount-cards', 'Admin\DiscountCardController@index');
    $router->get('/discount-cards/data', 'Admin\DiscountCardController@getData');
    $router->post('/discount-cards', 'Admin\DiscountCardController@postData');

    $router->get('/certificates', 'Admin\CertificateController@index');
    $router->get('/certificates/data', 'Admin\CertificateController@getData');
    $router->post('/certificates/data[/]', 'Admin\CertificateController@addData');
    $router->put('/certificates/data/{id}', 'Admin\CertificateController@updateData');
    $router->delete('/certificates/data/{id}', 'Admin\CertificateController@removeData');


    $router->get('/settings', 'Admin\SettingController@index');
    $router->post('/settings', 'Admin\SettingController@updateData');

});


$router->group(['prefix' => 'sync'], function () {
    Route::get('update', 'SyncController@updateProducts');
    Route::get('full-sync', 'SyncController@fullSync');
    Route::get('part-sync', 'SyncController@partSync');
    Route::get('products', 'SyncController@getProducts');
    Route::post('products', 'SyncController@postProducts');
    Route::get('cards', 'SyncController@getCards');
    Route::post('cards', 'SyncController@postCards');
    Route::get('stocks', 'SyncController@getStocks');
    Route::post('stocks', 'SyncController@postStocks');

    Route::get('colors', 'SyncController@getColors');
    Route::post('colors', 'SyncController@postColors');

    Route::get('sizes', 'SyncController@getSizes');
    Route::post('sizes', 'SyncController@postSizes');

    Route::get('pictures', 'SyncController@getPictures');
    Route::post('pictures', 'SyncController@postPictures');

    Route::get('users', 'SyncController@getUsers');
    Route::post('users', 'SyncController@postUsers');
    Route::get('discount-cards', 'SyncController@getCards');
    Route::get('push-orders', 'SyncController@pushOrders');
    Route::post('push-orders2', 'SyncController@pushOrders2');
    Route::get('get-unsynced-orders-and-shifts', 'SyncController@getUnsyncedOrdersAndShifts');
    Route::post('sync-closed-order/{id}', 'SyncController@syncClosedOrder');
    Route::post('sync-closed-shift/{id}', 'SyncController@syncClosedShift');
    Route::get('inv-documents/{type}', 'InventoryController@documents');
});
