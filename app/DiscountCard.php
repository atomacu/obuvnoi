<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DiscountCard extends Model
{
    //
    protected $fillable = [
        'uuid', 'name', 'number', 'owner_name','first_name', 'last_name', 'birthday_at', 'phone_mobile', 'percent', 'sum_sales', 'activated', 'comment'
    ];
    protected $appends = [
        'full_name'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('activated', function(Builder $builder) {
            //if (!\Request::is('admin/*')) 
            	$builder->whereActivated(1);
        });
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

}
