<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    
    protected $fillable = [
        'order_id', 
        'order_detail_uuid',
        'serial_id', 
        'product_id',
         'price', 
         'quantity', 
         'discount_percent', 
         'discount_amount', 
         'total',
         'sum',
         'model', 
         'barcode', 
         'modelcode', 
         'color_uuid',
         'size_uuid',
         'color',
         'size',
         'consultant', 
         'consultant_id'
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function serial()
    {
        return $this->belongsTo(Serial::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function certificate()
    {
        return $this->hasOne(Certificate::class, 'order_detail_id');
    }

    public function consultant()
    {
        return $this->belongsTo(User::class, 'consultant_id');
    }

}
