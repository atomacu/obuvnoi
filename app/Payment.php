<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable = [
        'order_id', 'payment_type_id', 'user_uuid',  'amount', 'comment', 'payed', 'user_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }

}
