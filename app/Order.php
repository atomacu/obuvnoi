<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Order extends Model
{
    //
    use Excludable;

    protected $fillable = [
        'uuid', 'shop_id', 'user_id', 'section_id', 'discount_card_id', 'client_id', 'notes', 'amount', 'closed', 'synced', 'updated_at', 'payment_type_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function discountCard()
    {
        return $this->belongsTo(DiscountCard::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function paymentTypes()
    {
        return $this->hasMany(OrderPaymentType::class);
    }

    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

}
