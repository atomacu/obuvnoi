<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //

    protected $fillable = [
        'order_id', 'order_detail_id', 'number', 'amount', 'name', 'phone', 'valid_at', 'activated'
    ];
    
    protected $dates = [
        'valid_at',
        'created_at',
        'updated_at',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function orderdetail()
    {
        return $this->belongsTo(OrderDetail::class);
    }


}
