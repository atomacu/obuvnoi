<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    //
    protected $fillable = [
        'dept_uuid', 'section_id', 'name', 'address', 'activated'
    ];

    public function printers()
    {
        return $this->hasMany(Printer::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

}
