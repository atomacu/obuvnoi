<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Serial extends Model
{
    //
    protected $fillable = [
        'uuid', 'product_id', 'barcode', 'vendorcode', 'number', 'price', 'stock', 'section_id', 'activated'
    ];

    /*protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('activated', function(Builder $builder) {
            $builder->whereActivated(1);
        });
    }*/

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function details()
    {
        return $this->hasOne(SerialDetails::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

}
