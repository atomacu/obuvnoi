<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerialDetails extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'serial_id', 'weight', 'price_gramm', 'size', 'color'
    ];

    public function serial()
    {
        return $this->belongsTo(Serial::class);
    }

    public function metals()
    {
        return $this->hasMany(Metal::class);
    }

    public function stones()
    {
        return $this->hasMany(Stone::class);
    }
    
}
