<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



  
class Size extends Model
{
protected $fillable = [
'uuid',
'barcodeId', 
'name',
'IntSize',
 'EuroSize',
  'Growth'
];

protected $dates = [
    'created_at',
    'updated_at',
];
}


