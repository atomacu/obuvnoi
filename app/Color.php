<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



  
class Color extends Model
{
protected $fillable = [
'uuid',  'barcodeId', 'name'
];
protected $dates = [
    'created_at',
    'updated_at',
];
}


