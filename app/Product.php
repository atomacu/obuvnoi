<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'parent_id', 'uuid', 'parent_uuid', 'product_type', 'name', 'barcode', 'vendorcode', 'modelcode', 'price', 'check_serial','is_folder', 'allow_discount', 'vat', 'activated'
    ];

    public function childrenUUID()
    {
        return $this->hasMany(Product::class, 'parent_uuid', 'uuid');
    }

    public function children()
    {
        return $this->hasMany(Product::class, 'parent_id', 'id');
    }

    public function serials()
    {
        return $this->hasMany(Serial::class);
    }

}
