<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Printer;
use App\Shift;
use App\Order;
use App\Setting;
use App\Shop;

class AppController extends Controller
{
    //

    protected $shop_id;
    protected $shop;

    public function __construct(Request $request) {
        $this->shop_id = $request->cookie('shop_id');
        $this->shop = Shop::find($this->shop_id);
    }

    public function index(Request $request)
    {
    	
    }

    public function printFiscal(Request $request)
    {
        $prepareData = [
            "Reason" => $request->reason,
            "Command" => $request->command,
            "Body" => [],
            "Details" => [],
            "Payments" => []
        ];

        $printer = $this->shop->printers()->fiscal()->first();

        if ($printer) {
            $client = new Client();
            $res = $client->request('POST', $printer->path, [
                'json' => $prepareData
            ]);
            $resp = [];
            if ($res->getStatusCode() == 200) {
                //$resp = json_decode($res->getBody());
            }
        }
        return response()->json(['status' => 'ok']);
    }

    public function closeShift(Request $request)
    {
        $this->shop->shifts()->create(['user_id' => $request->user()->id]);
    }

    public function getSettings(Request $request)
    {
        $settingsArr = [];
        $settings = Setting::all();
        foreach ($settings as $setting) {
            $settingsArr[$setting->name] = $setting->value;
        }
        $shop = Shop::find($this->shop_id);

        $shift = $this->shop->shifts()->latest()->first();
        $printerReceipt = $this->shop->printers()->receipt()->count();
        $printerFiscal = $this->shop->printers()->fiscal()->count();

        $shop = Shop::find($this->shop_id);
        $data = [
            'last_shift_at' => (($shift) ? $shift->created_at->subSeconds(10)->toDateTimeString() : ''),
            'printer_receipt' => $printerReceipt,
            'printer_fiscal' => $printerFiscal,
            'settings' => $settingsArr,
            'shop_name' => $shop->name,
            'shop_id' => $shop->id
        ];

        return response()->json($data);
    }

    public function getUnsyncedOrders(Request $request)
    {
        $unsyncedOrders = $this->shop->orders()->whereRaw('(closed = 1 OR closed = 71 OR closed = 66)')->where('synced', 0)->count();
        if ($unsyncedOrders){
        return response()->json($unsyncedOrders);}else return 200;
        
    }

}
