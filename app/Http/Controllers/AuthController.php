<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Auth;
use Carbon\Carbon;
use App\User;

class AuthController extends Controller
{
    //
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'pin_code' => 'required',
        ]);

        try {
            $users = User::all();
            foreach ($users as $user) {
                if (app('hash')->check($request->pin_code, $user->getAuthPassword()) && $user->activated) {
                    $token = $this->jwt->fromUser($user);
                    Auth::login($user);
                    break;
                }
            }
            if (empty($token)) {
                return response()->json(['message' => 'Пользователь не найден'], 404);
            }
            /*
            if (!$token = $this->jwt->attempt($request->only('pin_code'))) {
                return response()->json(['message' => 'Пользователь не найден'], 404);
            }
            */
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['message' => 'Сессия истекла'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['message' => 'Сессия недействительна'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['message' => 'Сессия отсутствует (' . $e->getMessage() . ')'], $e->getStatusCode());
        }

        Auth::user()->update(['visited_at' => Carbon::now()]);

        $response = compact('token');
		$response['user'] = Auth::user();
        
        return response()->json($response);
    }

    public function postLogout(Request $request)
    {
        $this->jwt->invalidate($this->jwt->getToken());

        return response()->json([
            'message' => 'Вы успешно вышли!'
        ], 200);
    }

}
