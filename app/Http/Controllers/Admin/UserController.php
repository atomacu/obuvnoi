<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class UserController extends Controller
{
    //

    public function index(Request $request)
    {
     	return view('admin.users');
    }

    public function getData()
    {
        $users = User::all();

        return response()->json($users); 
    }

    public function addData(Request $request) {
        $user = User::create($request->all());

        return response()->json(['id' => $user->id]);
    }

    public function updateData(Request $request, $id) {
        $user = User::find($id);
        $user->update($request->all());

        return response()->json(['status' => 'ok']);
    }

    public function removeData($id) {
        $user = User::find($id);
        $user->delete();

        return response()->json(['status' => 'ok']);
    }

}
