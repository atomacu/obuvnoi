<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Certificate;

class CertificateController extends Controller
{
    //

    public function index(Request $request)
    {
     	return view('admin.certificates');
    }

    public function getData()
    {
        $records = Certificate::all();

        return response()->json($records); 
    }

    public function addData(Request $request) {
        $record = Certificate::create($request->all());

        return response()->json(['id' => $record->id]);
    }

    public function updateData(Request $request, $id) {
        $record = Certificate::find($id);
        
        $fields = $request->except(['order_id', 'order_detail_id']);
        $record->update($fields);

        return response()->json(['status' => 'ok']);
    }

    public function removeData($id) {
        $record = Certificate::find($id);
        $record->delete();

        return response()->json(['status' => 'ok']);
    }

}
