<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Shop;

class ShopController extends Controller
{
    //

    public function index(Request $request)
    {
     	return view('admin.shops');
    }

    public function getData()
    {
        $shops = Shop::all();

        return response()->json($shops); 
    }

    public function addData(Request $request) {
        $shop = Shop::create($request->all());

        return response()->json(['id' => $shop->id]);
    }

    public function updateData(Request $request, $id) {
        $shop = Shop::find($id);
        $shop->update($request->all());

        return response()->json(['status' => 'ok']);
    }

    public function removeData($id) {
        $shop = Shop::find($id);
        $shop->delete();

        return response()->json(['status' => 'ok']);
    }

}
