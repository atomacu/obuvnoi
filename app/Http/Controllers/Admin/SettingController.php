<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Setting;

class SettingController extends Controller
{
    //

    public function index(Request $request)
    {
        $settings = Setting::where('activated', true)->get();

     	return view('admin.settings', compact('settings'));
    }

    public function updateData(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            $record = Setting::where('name', $key)->first();
            $record->update(['value' => $value]);
        }
        return response()->json(['status' => 'ok']);
    }

}
