<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DiscountCard;
use Webpatser\Uuid\Uuid;
class DiscountCardController extends Controller
{
    //

    public function index(Request $request)
    {
     	return view('admin.discount-cards');
    }

    public function getData()
    {
        $records = DiscountCard::all();

        return response()->json($records); 
    }
    public function postData(Request $request){

            $discount= json_decode($request->dataDiscount,true);
            $endpoint_url=env('1C_HOST');
            $string_json = '{
                "Reason": "NewClient",
                "Department": "'.env('dep_UUID').'",
                "UUID": "21e0b922-f2a8-4134-afea-01e2334357e9",
                "Clients": {
                    
                    "Client1": [
                        {
                        "UUID": "'.Uuid::generate(4).'",
                        "FirstName": "'.$discount['FirstName'].'",
                        "SecondName": "'.$discount['SecondName'].'",
                        "Middlename": "'.$discount['Middlename'].'",
                        "Tel": "'.$discount['Tel'].'",
                        "Email": "'.$discount['Email'].'",
                        "Adress": "'.$discount['Adress'].'",
                        "Sex": "'.$discount['Sex'].'",
                        "Lang": "'.$discount['Lang'].'",
                        "CardNr": "'.$discount['CardNr'].'",
                        "CardProcent": "'.$discount['CardProcent'].'",
                        "CardType": "'.$discount['CardType'].'",
                        "CardUUID":  "'.Uuid::generate(4).'",
                        "Enabled": "'.$discount['Enabled'].'"
                        }
                    ]
                            }
             }';
        //   return $string_json;
             $username=env('1C_USERNAME');
             $password =env('1C_PASSWORD');
            $client = new \GuzzleHttp\Client();
            $options= array(
            'auth' => [
                $username,
                $password
            ],
            'headers'  =>[ 'Content-type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json'],
            'body' => $string_json,
            );
            try {
                $jsonArray = $client->post($endpoint_url, $options);
                $response=$jsonArray->getBody();
            } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            } }  
            return $response;    
    }

}
