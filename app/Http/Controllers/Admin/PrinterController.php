<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Printer;

class PrinterController extends Controller
{
    //

    public function index(Request $request)
    {
     	return view('admin.printers');
    }

    public function getData()
    {
        $printers = Printer::with('shop')->get();

        return response()->json($printers); 
    }

    public function addData(Request $request) {
        $printer = Printer::create($request->all());

        return response()->json(['id' => $printer->id]);
    }

    public function updateData(Request $request, $id) {
        $printer = Printer::find($id);
        $printer->update($request->all());

        return response()->json(['status' => 'ok']);
    }

    public function removeData($id) {
        $printer = Printer::find($id);
        $printer->delete();

        return response()->json(['status' => 'ok']);
    }

}
