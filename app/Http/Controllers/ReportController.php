<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Anam\PhantomMagick\Converter;
use GuzzleHttp\Client;
use Carbon\Carbon;

use App\Order;
use App\OrderDetail;
use App\Printer;
use App\Shift;
use App\Shop;
use App\Section;
use App\DiscountCard;
use App\Product;


class ReportController extends Controller
{
    //
    protected $print = false;
    protected $shop_id;
    protected $companyInfo = [];

    public function __construct(Request $request) {
        $this->shop_id = $request->cookie('shop_id');

        if ($request->has('shop_id'))
            $this->shop_id = $request->shop_id;
    }

    public function index(Request $request)
    {

    	$reportType = $request->report;

        $shop = Shop::find($this->shop_id);

        $shop_logo = 'data:image/png;base64,' . base64_encode(file_get_contents(base_path() . '/public/images/ignored_dir/logo.png'));
        $this->companyInfo['shop_logo'] = $shop_logo;
        $this->companyInfo['shop_address'] = str_replace("\n", '<br>', $shop->address);


    	switch ($reportType) {
    		case '1':
    			return $this->salesAnalysis($request);
    			break;
            case '10':
    			return $this->Recept($request);
    			break;
    		case '2':
    			return $this->shiftReport($request);
    			break;
            case '3':
                return $this->productBalance($request);
                break;
            case '4':
                return $this->productBalanceReport($request);
                break;
            case '5':
                return $this->productBalancePrint($request);
                break;
             case '6':
                return $this->clients($request);
                break;
            case '7':
                return $this->clientsReport($request);
                break;
            case '8':
                return $this->clientsReportPrint($request);
                break;
                default:
    			# code...
    			break;
    	}
    }
    public function print(Request $request)
    {
        $this->print = true;

        $this->index($request);
    }

    /*public function salesAnalysis_old(Request $request)
    {
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;
        $companyInfo = $this->companyInfo;

        $records = \DB::table('order_details')
            ->select('serials.number AS serial_number', 'products.name AS product_name')
            ->addSelect(\DB::raw('SUM(order_details.quantity) as quantity_sum'))
            ->addSelect(\DB::raw('SUM(order_details.price) as price_sum'))
            ->addSelect(\DB::raw('SUM(order_details.quantity * order_details.price) as sum'))
            ->addSelect(\DB::raw('SUM(order_details.discount_amount) as discount_sum'))
            ->addSelect(\DB::raw('SUM(order_details.total) as total_sum'))
            ->join('serials', 'serials.id', '=', 'order_details.serial_id')
            ->join('products', 'products.id', '=', 'serials.product_id')
            ->join('orders', 'orders.id', '=', 'order_details.order_id')
            ->where('orders.shop_id', $this->shop_id)
            ->where('orders.closed', '>', 0)
            ->whereBetween('order_details.created_at', [$dateFrom, $dateTo])
            ->groupBy('products.id', 'serials.id')
            ->orderBy('products.name')
            ->get();

        $data = compact('records', 'dateFrom', 'dateTo', 'companyInfo');
        if ($this->print) {
            $contents = view('reports.sales_analysis', $data)->render();
            $file_path = Storage::put('report-sales.html', $contents);
            $file_path_out = storage_path('app/report-sales.png');
            $conv = new Converter();
            $conv->source(storage_path('app/report-sales.html'))
                ->toPng(['width' => 400, 'quality' => 50])
                ->save($file_path_out);

            $printerReceipt = Shop::find($this->shop_id)->receipt()->first();
            try {
                $client = new Client();
                $res = $client->request('POST', $printerReceipt->path, [
                    'multipart' => [
                        ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                    ]
                ]);
            } catch (\Exception $e) {
                return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
            }

            return ['status' => 'ok'];
        }
        return view('reports.sales_analysis', $data);
    }*/

    public function salesAnalysis(Request $request)
    {
        $dateFrom = $request->dateFrom;
        if (empty($dateFrom))
            $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
        $dateTo = $request->dateTo;
        if (empty($dateTo))
            $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
        $companyInfo = $this->companyInfo;

        $records = Shop::find($this->shop_id)->orders()->whereRaw('(closed = 1 OR closed = 71)')->whereBetween('updated_at', [$dateFrom, $dateTo])->with(['user', 'section', 'paymentTypes.paymentType', 'details.serial.details', 'details.product', 'details.consultant'])->orderBy('section_id')->orderBy('updated_at')->get();
        $sections =Section::all();


    //    $result = array();


    //     foreach ($records as $record) {

    //                 foreach ($record->details as $detail) {
    //                         $id = $detail->product_id;
    //                         if (isset($result[$id])) {
    //                             $result[$id][0]['quantity']=$result[$id][0]['quantity']+1 ;
    //                             $result[$id][0]['total']+=($detail->price*$detail->quantity) ;
    //                         } else {
    //                                 $result[$id] = array($detail);
    //                         }       $result[$id][0]['total']=$result[$id][0]['price']*$result[$id][0]['quantity'] ;
    //                 }
    //     }

    //     $price = array();
	// 	foreach (	$result as $key => $result)
	// 	{
	// 		array_push($price, $result[0]);
    //     }
    //     array_multisort(array_column($price, 'total'), SORT_DESC, $price);



    //             return $price;




    	$data = compact('records', 'dateFrom', 'dateTo', 'companyInfo','sections');
        if ($this->print) {
            $contents = view('reports.sales_analysis', $data)->render();
            $file_path = Storage::put('report-sales.html', $contents);
            $file_path_out = storage_path('app/report-sales.png');
            $conv = new Converter();
             $conv->source(storage_path('app/report-sales.html'))
                 ->toPng(['width' => 560, 'quality' => 40])
                 ->save($file_path_out);

            $printerReceipt = Shop::find(1)->printers()->receipt()->first();
            try {
                $client = new Client();
                $res = $client->request('POST', $printerReceipt->path, [
                    'multipart' => [
                        ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                    ]
                ]);
            } catch (\Exception $e) {
                return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
            }

            return ['status' => 'ok'];
        }
    	return view('reports.sales_analysis', $data);
    }

    public function shiftReport(Request $request)
    {
    	$dateFrom = $request->dateFrom;
        if (empty($dateFrom))
            $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
    	$dateTo = $request->dateTo;
        if (empty($dateTo))
            $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
        $companyInfo = $this->companyInfo;

        $records = Shop::find($this->shop_id)->orders()->whereRaw('(closed = 1 OR closed = 71 OR closed = 66)')->whereBetween('updated_at', [$dateFrom, $dateTo])->with(['user', 'paymentTypes', 'details'])->orderBy('user_id')->orderBy('updated_at')->get();

    	$data = compact('records', 'dateFrom', 'dateTo', 'companyInfo');
        if ($this->print) {
            $contents = view('reports.shifts', $data)->render();
            $file_path = Storage::put('report-shifts.html', $contents);
            $file_path_out = storage_path('app/report-shifts.png');
            $conv = new Converter();
            $conv->source(storage_path('app/report-shifts.html'))
                ->toPng(['width' => 560, 'quality' => 40])
                ->save($file_path_out);

            $printerReceipt = Shop::find($this->shop_id)->printers()->receipt()->first();
            if ($printerReceipt){
                try {
                    $client = new Client();
                    $res = $client->request('POST', $printerReceipt->path, [
                        'multipart' => [
                            ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                        ]
                    ]);
                } catch (\Exception $e) {
                    return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
                }

            }

            return ['status' => 'ok'];
        }
    	return view('reports.shifts', $data);
    }

    public function accountsJournal(Request $request)
    {
        $dateFrom = $request->dateFrom;
        if (empty($dateFrom))
            $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
        $dateTo = $request->dateTo;
        if (empty($dateTo))
            $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();

        $shifts = Shift::select('shifts.id AS r_id', 'users.name AS user_name', \DB::raw('NULL AS section_name, NULL AS closed, NULL AS quantity_sum, NULL AS sum, NULL AS discount_sum, NULL AS total_sum'), 'shifts.created_at', 'shifts.updated_at', 'shifts.synced')
            ->addSelect(\DB::raw('NULL AS paymentTypes'))
            ->where('shifts.shop_id', $this->shop_id)
            ->whereBetween('shifts.created_at', [$dateFrom, $dateTo])
            ->join('users', 'users.id', '=', 'shifts.user_id');

        $orders = \DB::table('orders')
            ->select('orders.id AS r_id', 'users.name AS user_name', 'sections.name AS section_name', 'orders.closed')
            ->addSelect(\DB::raw('SUM(order_details.quantity) as quantity_sum'))
            ->addSelect(\DB::raw('SUM(order_details.quantity * order_details.price) as sum'))
            ->addSelect(\DB::raw('SUM(order_details.discount_amount) as discount_sum'))
            ->addSelect(\DB::raw('SUM(order_details.total) as total_sum'))
            ->addSelect('orders.created_at', 'orders.updated_at', 'orders.synced')
            ->addSelect(\DB::raw('payment_types.name AS paymentTypes'))
            ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->leftJoin('sections', 'sections.id', '=', 'orders.section_id')
            ->leftJoin('payment_types', 'payment_types.id', '=', 'orders.payment_type_id')
            ->where('orders.shop_id', $this->shop_id)
            ->whereBetween('orders.updated_at', [$dateFrom, $dateTo])
            ->groupBy('orders.id');

        $records = $shifts->union($orders)->orderBy('updated_at')->get();

        return response()->json($records);
    }

    public function receptJournal(Request $request)
    {//NULL AS section_name,
        //'sections.name AS section_name'
        $id = json_decode($request->id);
        $shifts = Shift::select('shifts.id AS r_id', 'users.name AS user_name', \DB::raw(' NULL AS closed, NULL AS quantity_sum, NULL AS sum, NULL AS discount_sum, NULL AS total_sum'), 'shifts.created_at', 'shifts.updated_at', 'shifts.synced')
                ->where('shifts.shop_id', $this->shop_id)
                //->whereBetween('shifts.created_at', [$dateFrom, $dateTo])
                ->join('users', 'users.id', '=', 'shifts.user_id');
        $orders = \DB::table('orders')
            ->select('orders.id AS r_id', 'users.name AS user_name', 'orders.closed')
            ->addSelect(\DB::raw('SUM(order_details.quantity) as quantity_sum'))
            ->addSelect(\DB::raw('SUM(order_details.quantity * order_details.price) as sum'))
            ->addSelect(\DB::raw('SUM(order_details.discount_amount) as discount_sum'))
            ->addSelect(\DB::raw('SUM(order_details.total) as total_sum'))
            ->addSelect('orders.created_at', 'orders.updated_at', 'orders.synced')
            ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('users', 'users.id', '=', 'orders.user_id')
           // ->join('sections', 'sections.id', '=', 'orders.section_id')
            //->where('orders.shop_id', $this->shop_id)
            ->where('orders.discount_card_id',  $id )
            ->groupBy('orders.id');
      //  $rec=$orders->get();
        $records = $shifts->union($orders)->orderBy('updated_at')->get();

        return response()->json($records);
    }


        public function  productBalance(Request $request){
            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $EndDate=(new \DateTime($dateTo))->format('d.m.Y H:i:s');
            $BeginDate=(new \DateTime($dateFrom))->format('d.m.Y H:i:s');
            $product_br=substr($request->product_code,6,6);

           try{ $product=Product::where('barcode',$product_br)->first();}catch (ClientException $e) {}
            if(!empty($product)){

                $string_json = '{
                    "Reason":"ProductsBalance",
                    "Department":"468A726E",
                    "UUID":"02ba880d-8081-48b9-b445-9bb7c1075d6b",
                    "StorageList":[],
                    "ProductList":[{"UUID":"'.$product->uuid.'", "name": "'.$product->uuid.'"}],
                     "BeginDate":"'.$BeginDate.'",
                     "EndDate":"'.$EndDate.'",
                     "ShowEmpty":"0"
                    
                 }';
            } else {
                $string_json = '{
                    "Reason":"ProductsBalance",
                    "Department":"468A726E",
                    "UUID":"02ba880d-8081-48b9-b445-9bb7c1075d6b",
                    "StorageList":[],
                    "ProductList":[],
                     "BeginDate":"'.$BeginDate.'",
                     "EndDate":"'.$EndDate.'",
                     "ShowEmpty":"0"
                    
                 }';
            }
            $endpoint_url=env('1C_HOST');

             $username=env('1C_USERNAME');
            $password =env('1C_PASSWORD');
            $client = new \GuzzleHttp\Client();
            $options= array(
            'auth' => [
                $username,
                $password
            ],
            'headers'  =>[ 'Content-type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json'],
            'body' => $string_json,
            );
            try {
                $jsonArray = $client->post($endpoint_url, $options);
                $response=$jsonArray->getBody()->getContents();
            } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            } }  return $response;
        }
        public function  productBalanceReport(Request $request){
            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $products=json_decode($request->products,true);
            $data = compact('products', 'dateFrom', 'dateTo', 'companyInfo');

             return view('reports.product_balance', $data);

        }
        public function  productBalancePrint(Request $request){
            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $products=json_decode($request->products,true);
            $data = compact('products', 'dateFrom', 'dateTo', 'companyInfo');

                $contents = view('reports.product_balance', $data)->render();
                $file_path = Storage::put('product_balance.html', $contents);
                $file_path_out = storage_path('app/product_balance.png');
                $conv = new Converter();
                $conv->source(storage_path('app/product_balance.html'))
                    ->toPng(['width' => 560, 'quality' => 1])
                    ->save($file_path_out);
                $printerReceipt = Shop::find($this->shop_id)->printers()->receipt()->first();
                try {
                    $client = new Client();
                    $res = $client->request('POST',$printerReceipt->path, [
                        'multipart' => [
                            ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                        ]
                    ]);
                } catch (\Exception $e) {
                    return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
                }
                return ['status' => 'ok'];
             return view('reports.product_balance', $data);

        }
        public function clients(Request $request)
        {
            $client_number=$request->client_uuid;

           try{
               $client=DiscountCard::where('number',$client_number)->first();

            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $EndDate=(new \DateTime($dateTo))->format('d.m.Y H:i:s');
            $BeginDate=(new \DateTime($dateFrom))->format('d.m.Y H:i:s');
            $ShowEmpty=$request->showEmpty;
            $endpoint_url=env('1C_HOST');
            $string_json = '{
                "Reason":"ClientData",
                "Department":"'.env('dep_UUID').'",
                "UUID":"21e0b922-f2a8-4134-afea-01e2334357e9",
                "ClientsList":["'.$client->uuid.'"],
                 "BeginDate":"'.$BeginDate.'",
                 "EndDate":"'.$EndDate.'",
                 "ShowEmpty":"'.$ShowEmpty.'"
                
             }';

             $username=env('1C_USERNAME');
            $password =env('1C_PASSWORD');
            $client = new \GuzzleHttp\Client();
            $options= array(
            'auth' => [
                $username,
                $password
            ],
            'headers'  =>[ 'Content-type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json'],
            'body' => $string_json,
            );
            try {
                $jsonArray = $client->post($endpoint_url, $options);
                $response=$jsonArray->getBody()->getContents();
            } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            } }  return $response;

             }catch (\Exception $e) {
                $dateFrom = $request->dateFrom;
                if (empty($dateFrom))
                    $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
                $dateTo = $request->dateTo;
                if (empty($dateTo))
                    $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
                $companyInfo = $this->companyInfo;
                $EndDate=(new \DateTime($dateTo))->format('d.m.Y H:i:s');
                $BeginDate=(new \DateTime($dateFrom))->format('d.m.Y H:i:s');
                $ShowEmpty=$request->showEmpty;
                $endpoint_url=env('1C_HOST');;
                $string_json = '{
                    "Reason":"ClientData",
                    "Department":"468A726E",
                    "UUID":"21e0b922-f2a8-4134-afea-01e2334357e9",
                    "ClientsList":[],
                     "BeginDate":"'.$BeginDate.'",
                     "EndDate":"'.$EndDate.'",
                     "ShowEmpty":"'.$ShowEmpty.'"
                    
                 }';

                 $username=env('1C_USERNAME');
                $password =env('1C_PASSWORD');
                $client = new \GuzzleHttp\Client();
                $options= array(
                'auth' => [
                    $username,
                    $password
                ],
                'headers'  =>[ 'Content-type' => 'application/json; charset=utf-8',
                'Accept' => 'application/json'],
                'body' => $string_json,
                );
                try {
                    $jsonArray = $client->post($endpoint_url, $options);
                    $response=$jsonArray->getBody()->getContents();
                } catch (ClientException $e) {
                echo $e->getRequest() . "\n";
                if ($e->hasResponse()) {
                    echo $e->getResponse() . "\n";
                } }  return $response;
            }
        }
        public function clientsReport(Request $request)
        {
            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $clients=json_decode($request->clients,true);
            $data = compact('clients', 'dateFrom', 'dateTo', 'companyInfo');

             return view('reports.clients', $data);
        }
        public function clientsReportPrint(Request $request)
        {
            $dateFrom = $request->dateFrom;
            if (empty($dateFrom))
                $dateFrom = Carbon::now()->setTime(00, 00, 00)->toDateTimeString();
            $dateTo = $request->dateTo;
            if (empty($dateTo))
                $dateTo = Carbon::now()->setTime(23, 59, 59)->toDateTimeString();
            $companyInfo = $this->companyInfo;
            $clients=json_decode($request->clients,true);
            $data = compact('clients', 'dateFrom', 'dateTo', 'companyInfo');

                $contents = view('reports.clients', $data)->render();
                $file_path = Storage::put('clients.html', $contents);
                $file_path_out = storage_path('app/clients.png');
                $conv = new Converter();
                $conv->source(storage_path('app/clients.html'))
                    ->toPng(['width' => 560, 'quality' => 40])
                    ->save($file_path_out);
                $printerReceipt = Shop::find($this->shop_id)->printers()->receipt()->first();
                try {
                    $client = new Client();
                    $res = $client->request('POST',$printerReceipt->path, [
                        'multipart' => [
                            ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                        ]
                    ]);
                } catch (\Exception $e) {
                    return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
                }

                return ['status' => 'ok'];

             return view('reports.clients', $data);
        }
        public function Recept(Request $request)
        {
            $records = $request;

             $companyInfo = $this->companyInfo;
             $order = Order::find($records['order_id']);
             if ($order) {
                 $orderDetails = $order->details()->with(['product', ])->orderby('created_at', 'DESC')->get();//'serial.product', 'serial.details.metals', 'serial.details.stones', 'serial.section'
             } else $orderDetails = [];
            //  return $orderDetails;
            $records['birthday_at']=(new \DateTime($records['birthday_at']))->format('d.m.Y');
             $data = compact('records','companyInfo','orderDetails');



            if ($this->print) {
                $contents = view('reports.recept', $data)->render();
                $file_path = Storage::put('recept.html', $contents);
                $file_path_out = storage_path('app/recept.png');
                $conv = new Converter();
                 $conv->source(storage_path('app/recept.html'))
                     ->toPng(['width' => 560, 'quality' => 1])
                     ->save($file_path_out);

                $printerReceipt = Shop::find(1)->printers()->receipt()->first();
                try {
                    $client = new Client();
                    $res = $client->request('POST', $printerReceipt->path, [
                        'multipart' => [
                            ['name' => 'file', 'contents' => fopen($file_path_out, 'r')]
                        ]
                    ]);
                } catch (\Exception $e) {
                    return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!'];
                }

                return ['status' => 'ok'];
            }
            return view('reports.recept', $data);
        }
}
