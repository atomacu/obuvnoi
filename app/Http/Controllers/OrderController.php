<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Order;
use App\OrderDetail;
use App\Printer;
use App\Setting;
use App\Shop;
use App\Certificate;
use App\Product;
use App\Serial;
use App\Payment;
use App\PaymentType;
use App\OrderPaymentType;
use Webpatser\Uuid\Uuid;

class OrderController extends Controller
{
    protected $request;
    protected $certificate_type_code = 11;
    protected $shop_id;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->shop_id = $request->cookie('shop_id');
    }

    public function index(Request $request)
    {
        if ($request->user()->isAdmin()) {
            $records = Order::query()->where('shop_id', $this->shop_id)->whereRaw('(closed = 0 OR closed = 70)')->orderby('created_at', 'DESC')->with(['discountCard', 'shop', 'user', 'details'])->exclude('section_id')->get();
        } else {
            $records = $request->user()->orders()->where('shop_id', $this->shop_id)->whereRaw('(closed = 0 OR closed = 70)')->orderby('created_at', 'DESC')->with(['discountCard', 'shop', 'user', 'details'])->exclude('section_id')->get();
        }

        if ($records) {
            return response()->json($records);
        } else {
            return 'No Orders';
        }
    }

    public function store(Request $request)
    {
        $shop = Shop::find($this->shop_id);
        $closed = 0;
        if ($request->has('closed'))
            $closed = $request->closed;
        $record = Order::create(['shop_id' => $this->shop_id, 'section_id' => $shop->section_id, 'user_id' => $request->user()->id, 'closed' => $closed])
            ->load(['discountCard', 'shop', 'user']);

        return response()->json($record);
    }

    public function update(Request $request, $id)
    {
        try {
            $record = Order::find($id);
            $record->update($request->all());

            //$record->paymentTypes()->delete();


        } catch (\Exception $e) {
            return response()->json(['status' => 'err']);
        }
        if ($record->closed == 66)
            foreach ($record->details as $order_detail) {
                // if ($order_detail->serial) {
                //     if ($order_detail->serial->stock == 0)
                //         $order_detail->serial->update(['stock' => 1]);
                // }
                // if ($order_detail->certificate)
                //     $order_detail->certificate->delete();
            }

        return response()->json(['status' => 'ok']);
    }

    public function close(Request $request, $id)
    {
        $record = Order::with(['details', 'paymentTypes', 'shop', 'client', 'user'])->find($id);

        if ($request->has('client_id')) {
            $client_id = $request->client_id;
            if (!$client_id) {
                $client_id = $record->client()->create(['name' => $request->client_name, 'idno' => $request->client_idno])->id;
            }
            $record->update(['client_id' => $client_id]);
        }

        $change_value = $request->change_value;

        foreach ($request->all() as $key => $value) {
            $payment_type_arr = preg_split('/-/', $key);

            if (count($payment_type_arr) > 1) {
                $payment_type = $payment_type_arr[1];
                /*if (!empty($value) && is_numeric($payment_type) && (float)$value > 0) {
                    $record->update(['payment_type_id' => (int)$payment_type]);
                }*/
            } else {
                $payment_type = $payment_type_arr[0];
            }

            if (!empty($value)) {
                if (is_numeric($payment_type)) {
                    $payment = $record->paymentTypes()->create(['payment_type_id' => $payment_type, 'payment_uuid' => (string)Uuid::generate(4), 'amount' => $value]);
                    if ($change_value && $payment->paymentType->code == 0) {
                        $payment->update(['amount' => $value - $change_value]);
                        $payment->update(['change' => $change_value]);
                    }
                    //$certificate_value = $value;
                    if ($payment->paymentType->code == $this->certificate_type_code && $request->has('certificate_id')) {
                        $certificate_id = array_map('intval', explode(',', $request->certificate_id));
                        $certificates = Certificate::whereIn('id', $certificate_id)->get();
                        foreach ($certificates as $index => $certificate) {
                            if ($index == 0) {
                                $payment->update(['certificate_id' => $certificate->id, 'amount' => $certificate->amount]);
                            } else {
                                $payment = $record->paymentTypes()->create(['payment_type_id' => $payment_type, 'certificate_id' => $certificate->id, 'amount' => $certificate->amount]);
                            }
                            $payment->certificate->update(['activated' => 0]);
                        }
                        //if ($request->has('certificate_value') && $request->certificate_value) $certificate_value = $request->certificate_value;
                        //$payment->update(['certificate_id' => $request->certificate_id, 'amount' => $certificate_value]);
                        //$payment->certificate->update(['activated' => 0]);
                    }
                }
            }
        }

        $res = $this->printOrder($record);
        if ($res['status'] == 'ok') {
            try {
                if ($record->closed == 0) {
                    foreach ($record->details as $order_detail) {
                        if ($order_detail->serial) {
                            $stock = $order_detail->serial->stock;
                            $order_detail->serial->update(['stock' => $stock - 1]);
                        }
                    }
                    $record->update(['closed' => 1]);
                }
                if ($record->closed == 70) {
                    foreach ($record->details as $order_detail) {
                        if ($order_detail->serial) {
                            $stock = $order_detail->serial->stock;
                            if ($order_detail->quantity > 0) {
                                $order_detail->serial->update(['stock' => $stock - 1]);
                            } else {
                                $order_detail->serial->update(['stock' => $stock + 1]);
                            }
                        }
                    }
                    $record->update(['closed' => 71]);
                }
            } catch (\Exception $e) {
                foreach ($record->paymentTypes()->get() as $pt) {
                    if ($pt->certificate) {
                        $pt->certificate->update(['activated' => 1]);
                    }
                }
                $record->paymentTypes()->delete();
                return ['status' => 'error', 'message' => $e->getMessage()];
            }
        } else {
            foreach ($record->paymentTypes()->get() as $pt) {
                if ($pt->certificate)
                    $pt->certificate->update(['activated' => 1]);
            }
            $record->paymentTypes()->delete();
        }

        return response()->json($res);
    }

    public function printOrder(Order $order, $order_id = null)
    {
        if ($order_id)
            $order = Order::find($order_id);

        $is_fiscal = true;
        if ($this->request->has('is_fiscal'))
            $is_fiscal = (bool)$this->request->is_fiscal;

        $settingsArr = [];
        $shop = Shop::find($this->shop_id);
        $settings = Setting::all();
        $field = ['company_name', 'company_address'];
        $companyInfo = [];
        foreach ($settings as $setting) {
            $settingsArr[$setting->name] = $setting->value;
            if (in_array($setting->name, $field))
                $companyInfo[$setting->name] = $setting->value;
        }
        $companyInfo['shop_name'] = $shop->name;
        $companyInfo['shop_address'] = $shop->address;
        $companyInfo['shop_cashier'] = $order->user->name;
        $companyInfo['shop_consultant'] = '';
        $without_fiscal = (bool)$settingsArr['close_order_without_kkm'];
        $num_copy = (int)$settingsArr['print_copies'];

        $prepareData = [
            'Reason' => 'Order',
            'ID' => $order->id,
            'Copies' => $num_copy,
            'Body' => [],
            'Details' => [],
            'Payments' => [],
            'CompanyInfo' => $companyInfo
        ];

        $orderDetails = $order->details()->with(['serial.product', 'serial.details.metals', 'serial.details.stones', 'consultant'])->get();
        $details = [];
        $consultants = [];
        foreach ($orderDetails as $orderItem) {
            $metals = [];
            if ($orderItem->serial && $orderItem->serial->details->metals) {
                foreach ($orderItem->serial->details->metals as $metal) {
                    $metals[] = $metal->name . '/' . $metal->probe;
                }
            }
            $stones = [];
            if ($orderItem->serial && $orderItem->serial->details->stones) {
                foreach ($orderItem->serial->details->stones as $stone) {
                    $stones[] = $stone->name;
                }
            }
            $product_name = (($orderItem->serial) ? ($orderItem->serial->product->name) : $orderItem->product->name);
            if ($orderItem->certificate && substr($orderItem->certificate->number, 0, 2) == '22')
                $product_name = 'Arvuna p/u comanda';


            $item = [
                "At" => $orderItem->updated_at->toDateTimeString(),
                "ProductUUID" => (($orderItem->serial) ? $orderItem->serial->product->uuid : $orderItem->product->uuid),
                "Price" => $orderItem->price,
                "Counts" => $orderItem->quantity,
                "DiscountPercent" => $orderItem->discount_percent,
                "DiscountValue" => $orderItem->discount_amount,
                "Total" => $orderItem->total,
                "ProductName" => (($orderItem->serial) ? ($orderItem->serial->product->name) : $orderItem->product->name),
                "ItemDetails" => []
            ];
            if ($orderItem->consultant)
                $consultants[] = $orderItem->consultant->name;
            array_push($prepareData["Details"], $item);
        }
        $companyInfo['shop_consultant'] = implode(', ', $consultants);
        $prepareData['CompanyInfo'] = $companyInfo;

        $paymentTypes = $order->paymentTypes()->with(['paymentType', 'certificate'])->get();
        foreach ($paymentTypes as $payment) {
            if ($payment->payed) {
                $item = [
                    "PaymentCode" => 4,
                    "PaymentAmount" => $payment->amount,
                    "PaymentChange" => '0.00',
                    "PaymentName" => '',
                    "PaymentCertificateNumber" => '',
                ];
            } else {
                $item = [
                    "PaymentCode" => $payment->paymentType->code,
                    "PaymentAmount" => $payment->amount,
                    "PaymentChange" => $payment->change,
                    "PaymentName" => $payment->paymentType->name,
                    "PaymentCertificateNumber" => (($payment->certificate) ? $payment->certificate->number : null),
                ];
            }
            array_push($prepareData["Payments"], $item);
        }


        $printerReceipt = $shop->printers()->receipt()->first();
        $printerFiscal = $shop->printers()->fiscal()->first();

        if (!!$printerReceipt) {
            try {
                // return response()->json($prepareData);
                // $client = new Client();
                // $res = $client->request('POST', $printerReceipt->path, [
                //     'json' => $prepareData
                // ]);
            } catch (\Exception $e) {
                return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!<br>' . $e->getMessage()];
            }
            // return ['status' => 'error', 'message' => 'У Вас не настроен <strong>Receipt</strong> принтер!'];
        }

        if (!!$printerFiscal) {
            if ($is_fiscal && $order->amount >= 0) {
                try {
                    $client = new Client();
                    $res = $client->request('POST', $printerFiscal->path, [
                        'json' => $prepareData
                    ]);
                } catch (\Exception $e) {
                    if (!$without_fiscal)
                        return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerFiscal->path . ' недоступен!'];
                }
            }
            // return ['status' => 'error', 'message' => 'У Вас не настроен <strong>Fiscal</strong> принтер!'];
        }

        return ['status' => 'ok'];

        /*$resp = [];
        if ($res->getStatusCode() == 200) {
            //$resp = json_decode($res->getBody());
        }*/
    }

    public function findOrder(Request $request, $id)
    {

        $record = Order::where('id', $id)->get();
        //    if ($record){
            
        return response()->json($record);
        // }else
        // {return 'No Order';}
    }

    public function orderDetails(Request $request, $order_id)
    {
        $order = Order::find($order_id);
        if ($order) {
            $records = $order->details()->with(['product',])->orderby('created_at', 'DESC')->get();//'serial.product', 'serial.details.metals', 'serial.details.stones', 'serial.section'
        } else $records = [];

        return response()->json($records);

    }

    public function orderDetailsStore(Request $request)
    {
        $order = Order::find($request->order_id);

        $record = $order->details()->create(['order_id' => $request->order_id,
            'order_detail_uuid' => (string)Uuid::generate(4),
            "serial_id" => "0",
            'product_id' => $request->product_id,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'discount_percent' => $request->discount_percent,
            'discount_amount' => $request->discount_amount,
            'total' => $request->total,
            'sum' => $request->sum,
            'model' => $request->model,
            'consultant_id' => "0",
            'color' => $request->color,
            'size' => $request->size,
            'color_uuid' => $request->color_uuid,
            'size_uuid' => $request->size_uuid,
            'sum' => $request->sum,
            'barcode' => $request->barcode,
            'modelcode' => $request->modelcode
        ]);
        if ($request->has('updated_at')) {
            $record->updated_at = $request->updated_at;
            $record->save(['timestamps' => false]);
        }
        if ($request->filled('certificate_number')) {
            $record->certificate()->create(['order_id' => $request->order_id, 'number' => $request->certificate_number, 'amount' => $request->price, 'valid_at' => $request->certificate_date, 'name' => $request->certificate_name, 'phone' => $request->certificate_phone]);
        }

        return response()->json(['id' => $record->id]);
    }

    public function orderDetailsUpdate(Request $request, $id)
    {
        $record = OrderDetail::find($id);
        if ($record) {
            $record->update($request->all());
            if ($request->has('updated_at')) {
                $record->updated_at = $request->updated_at;
                $record->save(['timestamps' => false]);
            }
        }

        return response()->json(['status' => 'ok']);
    }

    public function orderDetailsRemove(Request $request, $id)
    {
        $record = OrderDetail::find($id);
        if ($record) {
            if ($record->certificate)
                $record->certificate->delete();
            $record->delete();
        }

        return response()->json(['status' => 'ok']);
    }

    public function orderPayments(Request $request, $order_id)
    {
        $order = Order::find($order_id);
        if ($order) {
            try {
                $records = $order->paymentTypes()->get();
            } catch (\Exception $e) {
            }
        } else $records = [];

        try {
            foreach ($records as $record) {
                $pt = PaymentType::find($record->payment_type_id);
                $record->payment_type = $pt->name;
            }
        } catch (\Exception $e) {
        }


        return response()->json($records);

    }

    public function orderPaymentsStore(Request $request)
    {
        $dataValue = $request;
        $response = OrderPaymentType::insert(
            [
                'order_id' => $dataValue->order_id,
                'payment_uuid' => (string)Uuid::generate(4),
                'payment_type_id' => $dataValue->payment_type_id,
                'user_uuid' => $dataValue->user_uuid,
                'amount' => $dataValue->amount,
                'comment' => $dataValue->comment,
                'payed' => $dataValue->payed,

            ]
        );

        return response()->json($response);
    }

    public function orderPaymentsPrintReciep(Request $request, $payment_id)
    {
        $record = OrderPaymentType::find($payment_id);
        $pt = PaymentType::find($record->payment_type_id);
        $payment_code = $pt->code;
        $payment_type = $pt->name;
        $order = Order::find($record->order_id);
        $settingsArr = [];
        $shop = Shop::find(1);
        $settings = Setting::all();
        $field = ['company_name', 'company_address'];
        $companyInfo = [];
        foreach ($settings as $setting) {
            $settingsArr[$setting->name] = $setting->value;
            if (in_array($setting->name, $field))
                $companyInfo[$setting->name] = $setting->value;
        }
        $companyInfo['shop_name'] = $shop->name;
        $companyInfo['shop_address'] = $shop->address;
        $companyInfo['shop_cashier'] = " ";//$order->user->name;
        $companyInfo['shop_consultant'] = '';
        $without_fiscal = (bool)$settingsArr['close_order_without_kkm'];
        $prepareData = [
            'Reason' => 'Order',
            'ID' => $record->id,
            'Copies' => '1',
            'Body' => [],
            'Details' => [],
            'Payments' => [],
            'CompanyInfo' => $companyInfo
        ];
        $item = [
            "At" => $record->created_at->toDateTimeString(),
            "ProductUUID" => $order->uuid,
            "Price" => $record->amount,
            "Counts" => '1',
            "DiscountPercent" => '0.00',
            "DiscountValue" => '0.00',
            "Total" => $record->amount,
            "ProductName" => 'Prepay',
            "ItemDetails" => []
        ];
        array_push($prepareData["Details"], $item);
        $item = [
            "PaymentCode" => $payment_code,
            "PaymentAmount" => $record->amount,
            "PaymentChange" => '',
            "PaymentName" => $payment_type,
            "PaymentCertificateNumber" => '',
        ];
        array_push($prepareData["Payments"], $item);
        $printerReceipt = $shop->printers()->receipt()->first();
        $printerFiscal = $shop->printers()->fiscal()->first();
        try {
            $client = new Client();
            $res = $client->request('POST', $printerReceipt->path, [
                'json' => $prepareData
            ]);
            // $record->payed=1;
            // $record->update();
            return 200;
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!<br>' . $e->getMessage()];
        }

    }

    public function orderPaymentsPrintFiscal(Request $request, $payment_id)
    {
        $record = OrderPaymentType::find($payment_id);
        $pt = PaymentType::find($record->payment_type_id);
        $payment_code = $pt->code;
        $payment_type = $pt->name;
        $order = Order::find($record->order_id);
        $settingsArr = [];
        $shop = Shop::find(1);
        $settings = Setting::all();
        $field = ['company_name', 'company_address'];
        $companyInfo = [];
        foreach ($settings as $setting) {
            $settingsArr[$setting->name] = $setting->value;
            if (in_array($setting->name, $field))
                $companyInfo[$setting->name] = $setting->value;
        }
        $companyInfo['shop_name'] = $shop->name;
        $companyInfo['shop_address'] = $shop->address;
        $companyInfo['shop_cashier'] = " ";//$order->user->name;
        $companyInfo['shop_consultant'] = '';
        $without_fiscal = (bool)$settingsArr['close_order_without_kkm'];
        $prepareData = [
            'Reason' => 'Order',
            'ID' => $record->id,
            'Copies' => '1',
            'Body' => [],
            'Details' => [],
            'Payments' => [],
            'CompanyInfo' => $companyInfo
        ];
        $item = [
            "At" => $record->created_at->toDateTimeString(),
            "ProductUUID" => $order->uuid,
            "Price" => $record->amount,
            "Counts" => '1',
            "DiscountPercent" => '0.00',
            "DiscountValue" => '0.00',
            "Total" => $record->amount,
            "ProductName" => 'Prepay',
            "ItemDetails" => []
        ];
        array_push($prepareData["Details"], $item);
        $item = [
            "PaymentCode" => $payment_code,
            "PaymentAmount" => $record->amount,
            "PaymentChange" => '',
            "PaymentName" => $payment_type,
            "PaymentCertificateNumber" => '',
        ];
        array_push($prepareData["Payments"], $item);
        $printerReceipt = $shop->printers()->receipt()->first();
        $printerFiscal = $shop->printers()->fiscal()->first();
        try {
            $client = new Client();
            $res = $client->request('POST', $printerFiscal->path, [
                'json' => $prepareData
            ]);
            $record->payed = 1;
            $record->update();
            return 200;
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => 'Принт-сервер: ' . $printerReceipt->path . ' недоступен!<br>' . $e->getMessage()];
        }

    }

    public function searchBarcode(Request $request)
    {
        $records = ['status' => 'ok'];
        $barcode = $request->barcode;

        /*$order_details = OrderDetail::with([
            'serial' => function($query) use ($barcode) {
                $query->where('barcode', $barcode);
            },
            'product' => function($query) use ($barcode) {
                $query->where('barcode', $barcode);
            },
        ])->get();*/
        if (!empty($barcode)) {
            $order_details = OrderDetail::with(['serial.product', 'serial.details.metals', 'serial.details.stones', 'serial.section', 'product', 'order.user', 'order.shop.section'])
                ->whereHas('order', function ($query) {
                    $query->where('closed', 1)->orWhere('closed', 71);
                })
                ->whereHas('serial', function ($query) use ($barcode) {
                    $query->where('barcode', $barcode)->orWhere('vendorcode', $barcode);
                })
                /*->whereHas('product', function($query) use ($barcode) {
                    $query->where('barcode', $barcode)->orWhere('vendorcode', $barcode);
                })*/
                ->where('quantity', '>', 0)
                ->latest()->first();
            if ($order_details && $order_details->price > 0) {
                $records = $order_details;
            } else {
                $product = Product::where('barcode', $barcode)->first();
                $serial = Serial::with(['product', 'details.metals', 'details.stones'])->where('barcode', $barcode)->where('stock', 0)->first();
                $records['product'] = $product;
                $records['serial'] = $serial;

                if (!$records) {
                    $records = ['message' => 'Такой продукт в продажах не найден!'];
                    return response()->json($records, 404);
                }
            }
        }

        return response()->json($records);
    }

}
