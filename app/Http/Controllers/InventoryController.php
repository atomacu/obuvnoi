<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use GuzzleHttp\Client;
use LSS\Array2XML;
use LSS\XML2Array;
use Webpatser\Uuid\Uuid;

use App\Shop;
use App\Section;
use App\User;
use App\Product;
use App\Serial;

use App\InvDocument;
use App\InvDocumentProduct;

class InventoryController extends Controller
{
    //
    protected $user;

    public function __construct(Request $request) {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('default_socket_timeout', '300');

        if ($request->hasCookie('user_id')) {
            $user_id = $request->cookie('user_id');
        } else
            $user_id = 1;

        $this->user = User::find($user_id);
    }

    public function documents(Request $request, $type, $section_id = null)
    {
        $returns = $this->getInvDocuments($request, $type);
        $return = json_decode($returns->getContent());
        if ($return->status == "error") {
            return response()->json($return);
        }

    	$documents = InvDocument::where('type', $type)->where('synced', 0)
                ->when($section_id, function ($query) use ($section_id) {
                    return $query->where('section_id', $section_id);
                })
                ->orderBy('date_at')->with(['user', 'receivedUser', 'section', 'products', 'products.serial', 'products.product'])->get();

    	return response()->json($documents);
    }

    public function storeDocument(Request $request, $type)
    {
        $data = [
            'uuid' => (string)Uuid::generate(4),
            'section_id' => $request->section_id,
            'from_section_id' => ($request->has('from_section_id')) ? $request->from_section_id : null,
            'user_id' => $this->user->id,
            'received_user_id' => null,
            'date_at' => Carbon::parse($request->date_at)->toDateTimeString(),
            'number' => $request->number,
            'notes' => $request->notes,
            'type' => $type,
            'local' => 1
        ];
        $result = InvDocument::create($data);

        return response()->json(['status' => 'ok', 'response' => $result]);
    }

    public function removeDocument(Request $request)
    {
        $result = InvDocument::find($request->id);
        $result->products()->delete();
        $result->delete();

        return response()->json(['status' => 'ok', 'response' => $result]);
    }

    public function confirmDocument(Request $request)
    {
        if ($request->hasCookie('shop_id')) {
            $shop = Shop::find($request->cookie('shop_id'));
        } else {
            $shop = Shop::first();
        }
        $dept_uuid = $shop->dept_uuid;

    	$result = '';

    	$record = InvDocument::find($request->id);

    	$items = [];
        $p_ids = [];
        $s_ids = [];
    	if ($record->products->count()) {
            $items['item'] = [];
	        foreach($record->products as $product) {
	            $items['item'][] = [
	                'CheckSerial' => (($product->serial_id) ? 1 : 0),
	                'Product' => (($product->serial_id) ? $product->serial->product->name . ' арт.' . $product->serial->vendorcode . ' (' . $product->serial->number . ')' : $product->product->name . ' арт.' . $product->product->vendorcode),
	                'ProductUUID' => (($product->serial_id) ? $product->serial->uuid : $product->product->uuid),
	                'PLU' => (($product->serial_id) ? $product->serial->barcode : $product->product->barcode),
	                'QuantOst' => $product->quantity_res,
	                'Quant' => $product->quantity,
                    'UUIDRow' => $product->uuid,
	                'ConfirmationDate' => ($product->confirm_at) ? $product->confirm_at->format('d.m.Y H:i:s') : '',
	            ];
                if ($product->scanned) {
                    if ($product->serial_id)
                        $s_ids[] =  $product->serial->id;
                    else
                        $p_ids[] =  $product->product->id;
                }
	        }
	    }

        $data = [
            'header' => [
                'opname' => 'post' . $record->type,
                'deptuuid' => $dept_uuid,
                'rescode' => 0,
                'resmsg' => '',
            ],
            'data' => [
                'Document' => [
                    'UUID' => $record->uuid,
                    'Number' => $record->number,
                    'Date' => $record->date_at->format('d.m.Y H:i:s'),
                    'SkladSenderUUID' => (($record->from_section) ? $record->from_section->uuid : ''),
                    'SkladSender' => (($record->from_section) ? $record->from_section->name : ''),
                    'SkladUUID' => (($record->section) ? $record->section->uuid : ''),
                    'Sklad' => (($record->section) ? $record->section->name : ''),
                    'Notes' => $record->notes,
                    'Author' => ($record->user) ? $record->user->name : '',
                    'AuthorUUID' => ($record->user) ? $record->user->uuid : '',
                    'Received' => $this->user->name,
                    'ReceivedUUID' => $this->user->uuid
                ],
                'items' => $items
            ]
        ];

        $xmlData = Array2XML::createXML('root', $data)->saveXML();
        try {
            $client = new Client();
            $url = env('1C_HOST_REST') . '?comand=postconfirm' . $record->type;
            $res = $client->request('POST', $url, [
                'auth' => [env('1C_USERNAME'), env('1C_PASSWORD')],
                'body' => $xmlData
            ]);
            $result = $res->getBody()->getContents();
            $responseArray = XML2Array::createArray($result);

            $data = [
                'received_user_id' => $this->user->id,
                'synced' => 1
            ];
            $record->update($data);
            if (count($p_ids) > 0)
                Product::whereIn('id', $p_ids)->update(['activated' => 1]);
            if (count($s_ids) > 0)
                Serial::whereIn('id', $s_ids)->update(['activated' => 1]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

    	return response()->json(['status' => 'ok', 'response' => $result]);
    }

    public function products(Request $request, $document_id)
    {
        $products = InvDocument::find($document_id)->products()->with(['serial', 'product', 'serial.product', 'serial.details', 'serial.details.metals', 'serial.details.stones'])->get();

        return response()->json($products);
    }

    public function storeRecord(Request $request)
    {
        $serial = null;
        $product = null;
        if ($request->has('serial'))
            $serial = json_decode($request->serial);
        else {
            $product = json_decode($request->product);
        }

        $data = [
            'uuid' => (string)Uuid::generate(4),
            'document_id' => $request->document_id,
            'serial_id' => ($serial) ? $serial->id : null,
            'product_id' => ($product) ? $product->id : null,
            'quantity_res' => $request->quantity_res,
            'quantity' => $request->quantity,
            'scanned' => $request->scanned,
            'confirm_at' => Carbon::now()->toDateTimeString()
        ];
        $record = InvDocumentProduct::create($data);

        return response()->json($record);
    }

    public function updateRecord(Request $request)
    {
    	$data = [
            'scanned' => $request->scanned,
            'quantity' => $request->quantity,
    		'confirm_at' => Carbon::now()->toDateTimeString()
    	];
    	InvDocumentProduct::find($request->id)->update($data);

    	return response()->json(['status' => 'ok']);
    }


    //$returns[] = $this->getInvDocuments($request, 'invent');
    //$returns[] = $this->getInvDocuments($request, 'coming');
    //$returns[] = $this->getInvDocuments($request, 'move');
    public function getInvDocuments(Request $request, $type)
    {
        if ($request->hasCookie('shop_id')) {
            $shop = Shop::find($request->cookie('shop_id'));
        } else {
            $shop = Shop::first();
        }
        $dept_uuid = $shop->dept_uuid;

        $last_sync_inv = Carbon::now()->subWeeks(1)->toDateTimeString();
        /*$last_rec = InvDocument::where('type', $type)->orderBy('created_at', 'DESC')->first();
        if ($last_rec)
            $last_sync_inv = $last_rec->created_at;

        if ($request->has('date')) {
            $dt = Carbon::parse($request->date);
            $last_sync_inv = $dt->toDateTimeString();
        }*/

        $command = "get$type";
        $data = [
            'header' => [
                'opname' => $command,
                'deptuuid' => $dept_uuid,
                'FromDate' => $last_sync_inv,
                'ToDate' => Carbon::now()->toDateTimeString()
            ],
            'data' => []
        ];
        $xmlData = Array2XML::createXML('root', $data)->saveXML();

        try {
            $client = new Client();
            $url = env('1C_HOST_REST') . '?comand=' . $command;
            $res = $client->request('POST', $url, [
                'auth' => [env('1C_USERNAME'), env('1C_PASSWORD')],
                'body' => $xmlData
            ]);
            $result = $res->getBody()->getContents();
            Storage::put('last_response_inv_documents_' . $command . '.xml', $result);

            $r = @simplexml_load_string($result);
            if ($r !== FALSE) {
                $responseArray = XML2Array::createArray($result);
                
                if ($type == 'invent')
                    $data = $responseArray['Inventarization'];
                if ($type == 'coming')
                    $data = $responseArray['Coming'];
                if ($type == 'move')
                    $data = $responseArray['Move'];

                $documents = $data['Document'];
                if (!isset($documents[0])) {
                    $s_doc = $documents;
                    $documents = [];
                    $documents[0] = $s_doc;
                }
                foreach ($documents as $document) {
                    $section_id = Section::where('uuid', $document['SkladUUID'])->first()->id;
                    $user_id = null;
                    if ($document['AuthorUUID'] != '00000000-0000-0000-0000-000000000000')
                        $user_id = User::where('uuid', $document['AuthorUUID'])->first()->id;
                    $received_user_id = null;
                    if ($document['ReceivedUUID'] != '00000000-0000-0000-0000-000000000000') {
                        $received_user = User::where('uuid', $document['ReceivedUUID'])->first();
                        if ($received_user)
                            $received_user_id = $received_user->id;
                    }

                    $data_rec = ['uuid' => $document['UUID'], 'section_id' => $section_id, 'user_id' => $user_id, 'received_user_id' => $received_user_id, 'date_at' => Carbon::parse($document['Date'])->toDateTimeString(), 'number' => $document['Number'], 'notes' => $document['Notes'], 'type' => $type];

                    $record = InvDocument::where('uuid', $document['UUID'])->first();
                    if (!$record) {
                        $record = InvDocument::create($data_rec);
                        $products = $document['items'];

                        if (isset($products['item'])) {
                            if (!isset($products['item'][0])) {
                                $s_item = $products['item'];
                                $products['item'] = [];
                                $products['item'][0] = $s_item;
                            }
                            foreach ($products['item'] as $product) {
                                $serial_id = null;
                                $product_id = null;

                                if ($product['CheckSerial'] == '1') {
                                    $q_serial = Serial::where('barcode', $product['PLU'])->first();
                                    if (!$q_serial) {
                                        throw new \Exception("Обновите номенклатуру!");
                                    }
                                    $q_serial->update(['uuid' => $product['ProductUUID']]);
                                    $serial_id = $q_serial->id;
                                } else {
                                    $q_product = Product::where('barcode', $product['PLU'])->first();
                                    if (!$q_product) {
                                        throw new \Exception("Обновите номенклатуру!");
                                    }
                                    $product_id = $q_product->id;
                                }

                                $quantity_res = (isset($product['QuantOst'])) ? $product['QuantOst'] : 0;

                                $data_rec = ['uuid' => $product['UUIDRow'], 'serial_id' => $serial_id, 'product_id' => $product_id, 'quantity_res' => $quantity_res, 'quantity' => $product['Quant']];
                                $record->products()->create($data_rec);
                            }
                        }
                    } else {
                        $record->update(["synced" => 0]);
                    }
                }
            }
        } catch (\Exception $e) {
            if (isset($record))
                $record->delete();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'ok', 'message' => "Documents synced"]);
    }

}
