<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Product;
use App\Serial;
use App\PaymentType;
use App\DiscountCard;
use App\Certificate;
use App\Section;
use App\Shop;
use App\User;
use App\Client;
use App\Recept;
use App\Color;
use App\Size;

class DataController extends Controller
{
    //
    public function __construct(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('default_socket_timeout', '300');
    }

    public function products(Request $request)
    {
    	$records = [];
      
    	$products = Product::where('parent_id', 0)->orderBy('product_type', 'DESC')->orderBy('check_serial', 'DESC')->orderBy('name')->get();

    	foreach ($products as $product) {
    		
            $data = $this->getChild($product->children);
            /*$data = [];
    		foreach ($product->children as $child) {
    			array_push($data, ['id' => $child->id, 'uuid'=> $child->uuid,'name' => $child->name, 'product_type' => $child->product_type, 'barcode' => $child->barcode, 'vendorcode' => $child->vendorcode, 'price' => $child->price, 'check_serial' => $child->check_serial, 'allow_discount' => $child->allow_discount]);
    		}*/
    		array_push($records, ['id' => $product->id, 'uuid'=> $product->uuid, 'name' => $product->name, 'product_type' => $product->product_type, 'barcode' => $product->barcode, 'vendorcode' => $product->vendorcode, 'price' => $product->price, 'check_serial' => $product->check_serial, 'allow_discount' => $product->allow_discount, 'data' => $data]);

        }

            //	$products = Product::where('parent_id', 0)->orderBy('product_type', 'DESC')->orderBy('check_serial', 'DESC')->orderBy('name')->get();
        // $id = $product['id'];
        // $parentID = $row['parent_id'];
        // $treeStructure[$parentID][] = $treeStructure[$id];    
        
  
        return response()->json($records);
    }

    public function getChild($childs) {
        $records = [];
        foreach ($childs as $child) {
            $data = $this->getChild($child->children);

            array_push($records, ['id' => $child->id, 'uuid'=> $child->uuid, 'name' => $child->name, 'product_type' => $child->product_type, 'barcode' => $child->barcode, 'vendorcode' => $child->vendorcode, 'price' => $child->price, 'check_serial' => $child->check_serial, 'allow_discount' => $child->allow_discount, 'data' => $data]);
        }

        return $records;
    }

    public function tree(Request $request){
        // $records = Product::join('serials', 'products.id', '=', 'serials.product_id')->select('products.name', 'serials.barcode')->where('serials.product_id','=','products.id')
        // ->get();
        $products = Product::orderBy('is_folder', 'DESC')->orderBy('check_serial', 'DESC')->orderBy('name')->get();//:where('parent_id',$parentID)->
        return response()->json($products);
    }
    public function treeElement(Request $request, $product_id){
        // $records = Product::join('serials', 'products.id', '=', 'serials.product_id')->select('products.name', 'serials.barcode')->where('serials.product_id','=','products.id')
        // ->get();
       // $products = Product::orderBy('product_type', 'DESC')->orderBy('check_serial', 'DESC')->orderBy('name')->get();//:where('parent_id',$parentID)->
       $product = Product::find($product_id);
        return response()->json($product);
    }
    public function serials(Request $request, $product_id)
    {
    	$product = Product::find($product_id);
    	$serials = $product->serials()->with(['details.metals','details.stones', 'section'])->where('stock', '>', 0)->where('activated', 1)->orderBy('vendorcode')->orderBy('barcode')->get();
    	return response()->json($serials);
    }

    public function paymentTypes(Request $request)
    {
        return response()->json(PaymentType::all());
    }

    public function getSections(Request $request)
    {
        return response()->json(Section::all());
    }

    public function getShops(Request $request)
    {
        return response()->json(Shop::all());
    }
 public function getbyBarcode(Request $request){
    $records = collect();
    $barcode= $request->barcode;
    $product_br=substr($barcode,6,6);
     $color_br=substr($barcode,0,3);
     $size_br=substr($barcode,3,3);
    
     $get_type = ($request->has('get_type')) ? $request->get_type : '';
     if ($request->has('barcode')) {
        
            $products = Product::where(function ($query) use ($request) {
                $query->where('barcode',substr($request->barcode,6,6));
            })
            ->where(function($query) use ($get_type)  {
                if ($get_type != "all") {
                    $query->where('activated', 1);
                }
            })->get();
            if ($products->count())
            {   $color=Color::where('barcodeId',substr($request->barcode,0,3))->get();
                if($color->count()){
                    $size=Size::where('barcodeId',substr($request->barcode,3,3))->get(); 
                    if($size->count()){
                        $products[0]->color=$color[0]->name;
                        $products[0]->color_uuid=$color[0]->uuid;
                        $products[0]->size=$size[0]->name;
                        $products[0]->size_uuid=$size[0]->uuid;
                        $products[0]->fullbarcode=$request->barcode;
                        $records = $products;
                    }else{
                        $records = ['message' => 'Такой баркод не найден!'];
                        return response()->json($records, 404);
                    }
                }else {
                    $records = ['message' => 'Такой баркод не найден!'];
                    return response()->json($records, 404);
                }
 
            }
        }
        if (!$records->count()) {
            $records = ['message' => 'Такой баркод не найден!'];
            return response()->json($records, 404);
        }
    return response()->json($records);

 }
    public function getSerial(Request $request)
    {
        $records = collect();
        $get_type = ($request->has('get_type')) ? $request->get_type : '';

        if ($request->has('barcode')) {
            $serials = Serial::where(function ($query) use ($request) {
                $query->where('barcode', $request->barcode)
                      ->orWhere('vendorcode', $request->barcode);
            })
            ->where('stock', '>', 0)
            ->where(function($query) use ($get_type)  {
                if ($get_type != "all") {
                    $query->where('activated', 1);
                }
            })
            ->with(['product', 'details.metals','details.stones', 'section'])->get();
            if ($serials->count()) {
                $records = $serials;
            } else {
                $products = Product::where(function ($query) use ($request) {
                    $query->where('barcode', $request->barcode)
                          ->orWhere('vendorcode', $request->barcode);
                })
                ->where(function($query) use ($get_type)  {
                    if ($get_type != "all") {
                        $query->where('activated', 1);
                    }
                })->get();
                if ($products->count())
                    $records = $products;
            }
            if (!$records->count()) {
                $records = ['message' => 'Такой баркод не найден!'];
                return response()->json($records, 404);
            }

        }
        return response()->json($records);
    }

    public function getDiscountCard(Request $request)
    {
        $records = [];
        // return response()->json($request->number);

        if ($request->has('number')) {
            $discount_card = DiscountCard::where('number', $request->number)->get();
            if ($discount_card->count()) {
                $records = $discount_card;
            } else {
                $records = ['message' => 'Такой номер карты не найден!'];
                return response()->json($records, 404);
            }
        }
        return response()->json($records);
    }

    public function getCertificate(Request $request)
    {
        $records = [];

        if ($request->has('number')) {
            if ($request->has('type') && $request->type == 'all')
                $certificate_card = Certificate::where('number', $request->number)->where('activated', 1)->get();
            else
                $certificate_card = Certificate::where('number', $request->number)->where('activated', 1)->where('valid_at', '>=', Carbon::now())->get();
            
            if ($certificate_card->count()) {
                $records = $certificate_card;
            } else {
                $records = ['message' => 'Такой номер сертификата не найден!'];
                return response()->json($records, 404);
            }
        }
        return response()->json($records);
    }

    public function checkStocks(Request $request)
    {
        $records = [];

        //dd($request->serial_list);
        if ($request->has('product_list')) {
            $list = json_decode($request->product_list);
            //if (count($list) > 0) $records = Product::whereIn('id', $list)->get();
        }
        if ($request->has('serial_list')) {
            $list = json_decode($request->serial_list);
            if (count($list) > 0) $records = Serial::whereIn('id', $list)->where('stock', '<=', 0)->get();
        }

        return response()->json($records);
    }

    public function getConsultants(Request $request)
    {
        return response()->json(User::consultants()->get());
    }

    public function clients(Request $request, $idno = '')
    {
        if (empty($idno))
            $clients = Client::all();
        else
            $clients = Client::where('idno', 'like', $idno.'%')->get();

        return response()->json($clients);
    }

    public function Recepts(Request $request)
    {
        $records=Recept::all();

        try{ 
            foreach ($records as $record){
            $pt=DiscountCard::find($record->dicount_card_id);
             $record->name=$pt->first_name." ".$pt->last_name;
            $record->phone=$pt->phone_mobile;
            $record->percent=$pt->percent;
            $record->birthday_at=$pt->birthday_at;}
        } catch (\Exception $e) {}
        return response()->json($records);
    	
    }
      public function ReceptsOrder(Request $request, $id)
    {
        $records= Recept::where('order_id',$id)->get();

        try{ 
            foreach ($records as $record){
            $pt=DiscountCard::find($record->dicount_card_id);
             $record->name=$pt->first_name." ".$pt->last_name;
            $record->phone=$pt->phone_mobile;
            $record->percent=$pt->percent;
            $record->birthday_at=$pt->birthday_at;}
        } catch (\Exception $e) {}
        return response()->json($records);
    	
    }
    public function ReceptsStore(Request $request)
    {
         $dataValue= json_decode($request->dataValue,true);
        
    	$response=Recept::insert(
            [
                'order_id'=>$dataValue['order_id'],
                'dicount_card_id'=>$dataValue['dicount_card_id'],
                'Dr_dioptria'=>$dataValue['Dr_dioptria'],
                'Dr_astig'=>$dataValue['Dr_astig'], 
                'Dr_gradus'=>$dataValue['Dr_gradus'],
                'Dl_dioptria'=>$dataValue['Dl_dioptria'],
                'Dl_astig'=>$dataValue['Dl_astig'],
                'Dl_gradus'=>$dataValue['Dl_gradus'],
                'Rr_dioptria'=>$dataValue['Rr_dioptria'],
                'Rr_astig'=>$dataValue['Rr_astig'],
                'Rr_gradus'=>$dataValue['Rr_gradus'],
                'Rl_dioptria'=>$dataValue['Rl_dioptria'],
                'Rl_astig'=>$dataValue['Rl_astig'],
                'Rl_gradus'=>$dataValue['Rl_gradus'],
                'PD_l'=>$dataValue['PD_l'],
                'PD_r'=>$dataValue['PD_r'],
                'PD_gradus'=>$dataValue['PD_gradus'],
                'comment'=>$dataValue['comment'],
             
           
            ]
        );
       
        return response()->json($response);
    }
    public function ReceptsUpdate(Request $request, $id)
    {
        $record = Recept::find($id);
        if ($record) {
        	$record->update($request->all());
            if ($request->has('updated_at')) {
                $record->updated_at = $request->updated_at;
                $record->save(['timestamps' => false]);
            }
    	}

    	return response()->json(['status' => 'ok']);
    }
    
    public function Colors(Request $request){
        $records=Color::all();
        return response()->json($records);
    }
    public function ProductColors(Request $request, $id){
        
    }
    public function Sizes(Request $request){
        $records=Size::all();
        return response()->json($records);
    }
    public function ProductSizes(Request $request, $id){
        
    }


}
