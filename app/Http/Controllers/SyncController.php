<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use GuzzleHttp\Client;
//use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\StreamDecoratorTrait;
use Tymon\JWTAuth\JWTAuth;
use JsonSerializable;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use DB;
use Validator;
use LSS\Array2XML;
use LSS\XML2Array;
use App\Order;
use App\Product;
use App\Serial;
use App\SerialDetails;
use App\User;
use App\Stone;
use App\Metal;
use App\Shift;
use App\Section;
use App\Setting;
use App\DiscountCard;
use App\Shop;
use App\InvDocument;
use App\Color;
use App\Size;
use App\Picture;
use Webpatser\Uuid\Uuid;

class SyncController extends Controller
{
    protected $soapWrapper;
    protected $dept_uuid = '';
    protected $last_sync;
    protected $is_full_sync;
    protected $shop_id;
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);
        ini_set('default_socket_timeout', '300');

        $this->jwt = $jwt;
    }
    // public function __construct(Request $request, SoapWrapper $soapWrapper)
    // {
    //     set_time_limit(0);
    //     ini_set('memory_limit', -1);
    //     ini_set('default_socket_timeout', '300');
    //     $this->soapWrapper = $soapWrapper;
    //     $this->soapWrapper->add('Giuvaier', function ($service) {
    //       $service
    //         ->wsdl(env('1C_HOST_SOAP'))
    //         ->trace(true)
    //         ->options(['login' => env('1C_USERNAME'), 'password' => env('1C_PASSWORD')]);
    //     });
    //     $settingsArr = [];
    //     $settings = Setting::all();
    //     foreach ($settings as $setting) {
    //         $settingsArr[$setting->name] = $setting->value;
    //     }
    //     $this->shop_id = $request->cookie('shop_id');
    //     if ($this->shop_id) {
    //         $shop = Shop::find($this->shop_id);
    //     } else {
    //         $shop = Shop::first();
    //     }
    //     $this->dept_uuid = $shop->dept_uuid;
    //     $this->last_sync = $settingsArr['last_sync'];
    //     $this->is_full_sync = ($this->last_sync === '' || $this->last_sync === null);
    // }

    function postProducts(Request $request)
    {
        $products = json_decode($request->products, true);

        $products_count = 0;
        $message = '';
        try {
            $product_ids = [];
            if (!empty($products)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    Product::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($products as $product) {
                    $product['PLU'] = preg_replace('/[^[:print:]]/', '', $product['PLU']);
                    $data_rec = ['uuid' => $product['UUID'], 'parent_uuid' => $product['ParentUUID'], 'product_type' => '1', 'name' => $product['Name'], 'barcode' => $product['PLU'], 'vendorcode' => ' ', 'modelcode' => $product['PLUCode'], 'price' => $product['Price'], 'check_serial' => '1', 'is_folder' => $product['IsFolder'], 'allow_discount' => '1', 'vat' => $product['NDS'], 'activated' => 1];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = Product::where('uuid', $product['UUID'])->first();
                    }
                    if (!empty($record)) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = Product::create($data_rec);
                    }
                    $product_ids[$record->uuid] = $record->id;
                    $products_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Products synced: $products_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function fillParent()
    {
        $products = Product::all();
        $parents_count = 0;
        foreach ($products as $product) {
            if ($product->childrenUUID()->count()) {
                $product->childrenUUID()->update(['parent_id' => $product->id]);
                $parents_count++;
            }
        }
        //echo "<p>Parent products filled: $parents_count</p>";
    }

    function postCards(Request $request)
    {
        $cards = json_decode($request->cards, true);

        $cards_count = 0;
        $message = '';
        try {
            $card_ids = [];
            if (!empty($cards)) {
                // if ($this->is_full_sync) {
                //     DB::statement('SET foreign_key_checks=0');
                //     DiscountCard::truncate();
                //     DB::statement('SET foreign_key_checks=1');
                // }
                foreach ($cards as $card) {
                    // $birthday_at = ((empty($card['Birthday'])) ? null : Carbon::create($card['Birthday'])->format('Y-m-d'));
                    // return $birthday_at ;
                    $data_rec = ['uuid' => $card['OwnerUUID'], 'name' => $card['Magnetic'], 'number' => $card['Magnetic'], 'first_name' => '', 'last_name' => '', 'owner_name' => $card['Owner'], 'birthday_at' => $card['Birthday'], 'phone_mobile' => $card['TelMobil'], 'percent' => $card['Percent'], 'sum_sales' => '0', 'comment' => '', 'activated' => 1,];
                    $record = null;

                    $record = DiscountCard::where('uuid', $card['OwnerUUID'])->first();

                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = DiscountCard::create($data_rec);

                    }
                    $card_ids[$record->uuid] = $record->id;
                    $cards_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Cards synced: $cards_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function getStocks(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'Stock','Department':'" . env('dep_UUID') . "','UUID':'46f2463f-8c58-4d6d-ae14-c1806a98862f'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();
        return $data;
    }

    function postStocks(Request $request)
    {
        $stocks = json_decode($request->stocks, true);

        $stocks_count = 0;
        $message = '';
        try {
            $stock_ids = [];
            if (!empty($stocks)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    Section::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($stocks as $stock) {

                    $data_rec = ['uuid' => $stock['UUID'], 'name' => $stock['Name'],];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = Section::where('uuid', $stock['UUID'])->first();
                    }
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = Section::create($data_rec);
                    }
                    $stock_ids[$record->uuid] = $record->id;
                    $stocks_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Stock synced: $stocks_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function postUsers(Request $request)
    {
        $users = json_decode($request->users, true);
        $users_count = 0;
        $message = '';
        try {
            $user_ids = [];
            if (!empty($users)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    User::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($users as $user) {

                    $data_rec = ['uuid' => $user['UUID'], 'name' => $user['Name'], 'password' => $user['Password'], 'permission' => $user['Enabled'], 'consultant' => '0', 'activated' => $user['Activated']];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = User::where('uuid', $user['UUID'])->first();
                    }
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = User::create($data_rec);
                    }
                    $user_ids[$record->uuid] = $record->id;
                    $users_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>user synced: $users_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function getColors(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'ProductColors','Department':'" . env('dep_UUID') . "','UUID':'19430e20-e71d-4a84-8932-e00719f35f7c'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');
        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();
        return $data;


    }

    function postColors(Request $request)
    {
        $colors = json_decode($request->colors, true);
        $colors_count = 0;
        $message = '';
        try {
            $stock_ids = [];
            if (!empty($colors)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    Color::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($colors as $color) {

                    $data_rec = ['uuid' => $color['UUID'], 'barcodeId' => $color['Code'], 'name' => $color['Name'],];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = Color::where('uuid', $color['UUID'])->first();
                    }
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = Color::create($data_rec);
                    }
                    $stock_ids[$record->uuid] = $record->id;
                    $colors_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Stock synced: $colors_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function getSizes(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'ProductSizes','Department':'" . env('dep_UUID') . "','UUID':'19430e20-e71d-4a84-8932-e00719f35f7c'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();

        return $data;
    }

    //################################################################################################COLORS

    function postSizes(Request $request)
    {
        $sizes = json_decode($request->sizes, true);

        $sizes_count = 0;
        $message = '';
        try {
            $stock_ids = [];
            if (!empty($sizes)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    Size::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($sizes as $size) {

                    $data_rec = ['uuid' => $size['UUID'], 'barcodeId' => $size['Code'], 'name' => $size['Name'], 'IntSize' => $size['IntSize'], 'EuroSize' => $size['EuroSize'], 'Growth' => $size['Growth']];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = Size::where('uuid', $size['UUID'])->first();
                    }
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = Size::create($data_rec);
                    }
                    $stock_ids[$record->uuid] = $record->id;
                    $sizes_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Stock synced: $sizes_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function getPictures(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'ProductPictures','Department':'" . env('dep_UUID') . "','UUID':'19430e20-e71d-4a84-8932-e00719f35f7c'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();
        return $data;


    }
    //################################################################################################COLORS
    //################################################################################################SIZES

    function postPictures(Request $request)
    {
        $pictures = json_decode($request->pictures, true);

        $pictures_count = 0;
        $message = '';
        try {
            $stock_ids = [];
            if (!empty($pictures)) {
                if ($this->is_full_sync) {
                    DB::statement('SET foreign_key_checks=0');
                    Picture::truncate();
                    DB::statement('SET foreign_key_checks=1');
                }
                foreach ($pictures as $picture) {

                    $data_rec = ['product_uuid' => $picture['UUID'], 'pic' => 'data:image/png;base64,' . $picture['Pict'],];
                    $record = null;

                    if (!$this->is_full_sync) {
                        $record = Picture::where('product_uuid', $picture['UUID'])->first();
                    }
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        // if ((int)$product['CheckSerial'] == 0)
                        //     $data_rec['activated'] = 0;
                        $record = Picture::create($data_rec);
                    }
                    $stock_ids[$record->uuid] = $record->id;
                    $pictures_count++;
                }
                $this->fillParent();
            }
            $message .= "<p>Stock synced: $pictures_count</p>";
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $setting = Setting::where('name', 'last_sync')->first();
        $setting->update(['value' => Carbon::now()]);

        return response()->json(['status' => 'ok', 'message' => $message]);
    }

    public function updateProducts(Request $request)
    {
        $this->last_sync = '2018-07-01 00:00:00';
        if ($request->has('date')) {
            $dt = Carbon::parse($request->date);
            $this->is_full_sync = false;
            $this->last_sync = $dt->toDateTimeString();
        }
        $data = [
            'header' => [
                'opname' => 'PRODUCTSXML',
                'deptuuid' => $this->dept_uuid,
                'FromDate' => $this->last_sync,
            ],
            'data' => []
        ];
        $xmlData = Array2XML::createXML('root', $data)->saveXML();

        //return response()->make($xmlData)->header('Content-Type', 'application/xml');

        $response = $this->soapWrapper->call('Giuvaier.getexchange', [['param1' => $xmlData]]);
        Storage::put('last_response_data.xml', $response->return);

        $sections_count = 0;
        $products_count = 0;
        $serials_count = 0;
        $message = '';
        $serials_list = [];
        try {
            $responseArray = XML2Array::createArray($response->return);
            $data = $responseArray['root']['data'];
            $sections = $data['Sklad'];
            $metals = $data['Metalls'];
            $probes = $data['Proby'];
            $stones = $data['Kamni'];
            $products = $data['Products'];
            $serials = $data['Serials'];

            $stones_arr = [];
            foreach ($stones['item'] as $stone) {
                $stones_arr[$stone['KameniUIID']] = $stone['KameniName'];
            }
            $metals_arr = [];
            foreach ($metals['item'] as $metal) {
                $metals_arr[$metal['MetallUIID']] = $metal['MetallName'];
            }
            $probes_arr = [];
            foreach ($probes['item'] as $proba) {
                $probes_arr[$proba['ProbaUIID']] = $proba['ProbaName'];
            }

            $section_ids = [];
            if (!empty($sections['item'])) {
                foreach ($sections['item'] as $section) {
                    $data_rec = ['uuid' => $section['SkladUIID'], 'name' => $section['SkladName']];

                    $record = Section::where('uuid', $section['SkladUIID'])->first();
                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        $record = Section::create($data_rec);
                    }
                    $section_ids[$section['SkladUIID']] = $record->id;
                    $sections_count++;
                }
            }

            $product_ids = [];
            if (!empty($products['item'])) {
                foreach ($products['item'] as $product) {
                    $data_rec = ['uuid' => $product['UUID'], 'parent_uuid' => $product['ParentID'], 'product_type' => $product['ProductType'], 'name' => $product['Name'], 'barcode' => $product['PLU'], 'vendorcode' => $product['Artikul'], 'price' => $product['Price'], 'check_serial' => $product['CheckSerial'], 'allow_discount' => $product['AlowDiscount'], 'vat' => $product['StavkaNDS'], 'activated' => 1];

                    $record = Product::where('uuid', $product['UUID'])->first();

                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        $record = Product::create($data_rec);
                    }
                    $product_ids[$record->uuid] = $record->id;
                    $products_count++;
                }
                $this->fillParent();
            }

            if (!empty($serials['item'])) {
                DB::statement('SET foreign_key_checks=0');
                if (!isset($serials['item'][0])) {
                    $s_item = $serials['item'];
                    $serials['item'] = [];
                    $serials['item'][0] = $s_item;
                }
                foreach ($serials['item'] as $serial) {
                    $product_id = $product_ids[$serial['ProductUUID']];
                    if (isset($serial['SkladUUID']))
                        $section_id = $section_ids[$serial['SkladUUID']];
                    else $section_id = null;

                    $data_rec = ['uuid' => '', 'product_id' => $product_id, 'barcode' => $serial['PLU'], 'vendorcode' => $serial['Artikul'], 'number' => $serial['Serial'], 'price' => $serial['Price'], 'stock' => $serial['Stock'], 'activated' => $serial['Activity']];
                    $data_details_rec = ['weight' => $serial['Massa'], 'price_gramm' => $serial['PriceGramm'], 'size' => $serial['Razmer'], 'color' => $serial['ColorMetall']];

                    if (!empty($section_id))
                        $data_rec['section_id'] = $section_id;

                    $record = Serial::where('number', $serial['Serial'])->first();

                    if ($record) {
                        $record->update($data_rec);
                    } else {
                        $record = Serial::create($data_rec);
                    }
                    $record->details()->delete();
                    $record->details()->create($data_details_rec);

                    if (isset($serial['Kamni']['item'])) {
                        $record->details->stones()->delete();
                        if (!isset($serial['Kamni']['item'][0])) {
                            $s_item = $serial['Kamni']['item'];
                            $serial['Kamni']['item'] = [];
                            $serial['Kamni']['item'][0] = $s_item;
                        }
                        foreach ($serial['Kamni']['item'] as $stone) {
                            if (isset($stones_arr[$stone['KameniUUID']]))
                                $record->details->stones()->create(['name' => $stones_arr[$stone['KameniUUID']], 'weight' => 0, 'clarity' => '']);
                        }
                    }
                    $data_metals = [];
                    if (isset($metals_arr[$serial['Metall']])) {
                        $record->details->metals()->delete();
                        $brobe = ((isset($probes_arr[$serial['Proba']])) ? $probes_arr[$serial['Proba']] : '');
                        $data_metals = ['name' => $metals_arr[$serial['Metall']], 'probe' => $brobe];
                        $record->details->metals()->create($data_metals);
                    }

                    /*foreach ($serial['Metalls']['item'] as $metal) {
                        $record->details()->metals()->create(['name' => $metals_arr[$metal['MetallUIID']], 'probe' => ''])
                    }*/
                    $data_rec['details'] = $data_details_rec;
                    $data_rec['metals'] = $data_metals;
                    $serials_count++;
                }
                DB::statement('SET foreign_key_checks=1');
            }

            $message .= "<p>Sections synced: $sections_count</p>";
            $message .= "<p>Products synced: $products_count</p>";
            $message .= "<p>Serials synced: $serials_count</p>";

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'ok', 'message' => $message]);
    }
//################################################################################################SIZES
//################################################################################################Prictures

    public function fullSync(Request $request)
    {
        $this->is_full_sync = true;
        $this->last_sync = '';
        $returns = [];

        $returns[] = $this->getProducts($request);
        $returns[] = $this->getCards($request);
        $returns[] = $this->getUsers($request);

        return response()->json($returns);
    }

    public function getProducts(Request $request)
    {

        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'Products','Department':'" . env('dep_UUID') . "','UUID':'f3aa6e30-74d9-4cb8-8fe6-cdd4304f0f33'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');
        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();

        return $data;
    }

//################################################################################################Prictures

    public function getCards(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'Cards','Department':'" . env('dep_UUID') . "','UUID':'f3aa6e30-74d9-4cb8-8fe6-cdd4304f0f33'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();

        return $data;
    }

    public function getUsers(Request $request)
    {
        $endpoint_url = env('1C_HOST');
        $string_json = "{'Reason':'Users','Department':'" . env('dep_UUID') . "','UUID':'e626ca43-c151-49df-b50a-e81592a8dc8e'} ";
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $string_json,

        );
        try {
            $response = $client->post($endpoint_url, $options);
        } catch (ClientException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }
        $data = $response->getBody();
        return $data;
    }

    public function partSync(Request $request)
    {
        $this->is_full_sync = false;
        $this->last_sync = '';
        $returns = [];

        $returns[] = $this->getProducts($request);
        $returns[] = $this->getCards($request);
        $returns[] = $this->getUsers($request);

        return response()->json($returns);
    }

    public function pushOrders(Request $request)
    {
        $unsyncedOrders = Order::whereRaw('(closed = 1 OR closed = 71 OR closed = 66)')->where('synced', 0);
        $unsyncedShifts = Shift::select('shifts.id', 'shifts.uuid', 'shifts.shop_id', 'shifts.user_id', DB::raw('NULL AS section_id, NULL AS discount_card_id, NULL AS client_id, NULL AS notes, NULL AS amount, NULL AS closed'), 'shifts.synced', 'shifts.created_at', 'shifts.updated_at')->where('synced', 0);

        $records = $unsyncedOrders->union($unsyncedShifts)->orderBy('created_at')->get();

        $num_orders = 0;
        foreach ($records as $record) {

            if ($record->closed) {

                $paymentTypes = [];
                if ($record->paymentTypes->count()) {
                    $paymentTypes['item'] = [];
                    foreach ($record->paymentTypes as $paymentType) {
                        $paymentTypes['item'][] = [
                            'payment_type' => $paymentType->paymentType->code,
                            'payment_amount' => $paymentType->amount,
                            'certificate_number' => (($paymentType->certificate) ? $paymentType->certificate->number : null),
                        ];
                    }
                }

                $orderDetails = [];
                if ($record->details->count()) {
                    $orderDetails['item'] = [];
                    foreach ($record->details as $order_details) {
                        $productType = (($order_details->serial_id) ? $order_details->serial->product->product_type : $order_details->product->product_type);
                        $orderDetails['item'][] = [
                            'at' => $order_details->created_at->toDateTimeString(),
                            'productType' => $productType,
                            'product' => (($order_details->serial_id) ? $order_details->serial->product->uuid : $order_details->product->uuid),
                            'serial' => (($order_details->serial_id) ? $order_details->serial->number : ''),
                            'certificate_number' => (($order_details->certificate) ? $order_details->certificate->number : null),
                            'certificate_date' => (($order_details->certificate) ? $order_details->certificate->valid_at->toDateTimeString() : null),
                            'certificate_name' => (($order_details->certificate) ? $order_details->certificate->name : null),
                            'certificate_phone' => (($order_details->certificate) ? $order_details->certificate->phone : null),
                            'price' => $order_details->price,
                            'quantity' => $order_details->quantity,
                            'sum' => ($order_details->price * $order_details->quantity),
                            'discount_percent' => $order_details->discount_percent,
                            'discount_sum' => $order_details->discount_amount,
                            'modelcode' => $order_details->modelcode,
                            'sum_final' => $order_details->total,
                            'ConsultantID' => (($order_details->consultant) ? $order_details->consultant->uuid : null),
                        ];
                    }
                }
                $data = [
                    'header' => [
                        'opname' => 'ORDER_JUVAIER',
                        'deptuuid' => $this->dept_uuid,
                        'rescode' => 0,
                        'resmsg' => 'ORDER de la juvaeri',
                    ],
                    'data' => [
                        'Order' => [
                            'uuid' => $record->uuid,
                            'at' => $record->created_at->toDateTimeString(),
                            'user' => $record->user->uuid,
                            'notes' => $record->notes,
                            'sum_final' => $record->amount,
                            'discount_card_uuid' => (($record->discountCard) ? $record->discountCard->uuid : null),
                            'discount_card_percent' => (($record->discountCard) ? $record->discountCard->percent : null),
                            'state' => $record->closed,
                            'paymentTypes' => $paymentTypes,
                            'sklad_uuid' => (($record->section) ? $record->section->uuid : ''),
                            'client_idno' => (($record->client) ? $record->client->idno : ''),
                            'client_name' => (($record->client) ? $record->client->name : ''),
                        ],
                        'OrderDetails' => $orderDetails
                    ]
                ];

            } else {

                $data = [
                    'header' => [
                        'opname' => 'SHIFT_JUVAIER',
                        'deptuuid' => $this->dept_uuid,
                        'rescode' => 0,
                        'resmsg' => 'SMENA de la juvaeri',
                    ],
                    'data' => [
                        'uuid' => $record->uuid,
                        'at' => $record->created_at->toDateTimeString(),
                        'user' => $record->user->uuid,
                    ]
                ];

            }

            $xmlData = Array2XML::createXML('root', $data)->saveXML();

            //return response()->make($xmlData)->header('Content-Type', 'application/xml');

            $response = $this->soapWrapper->call('Giuvaier.getexchange', [['param1' => $xmlData]]);
            try {
                $validator = Validator::make(['uuid' => $response->return], ['uuid' => 'uuid']);
                $is_valid = $validator->passes();
                if (!$is_valid) {
                    $responseArray = XML2Array::createArray($response->return);
                    $data = $responseArray['root']['header'];
                    //$validator = Validator::make(['uuid' => $data['resmsg']], ['uuid' => 'uuid']);
                    //$is_valid = $validator->passes();
                    $is_valid = ($data['rescode'] == 1);
                }

                if ($is_valid) {
                    if ($record->closed) {
                        $m_order = Order::find($record->id);
                        $m_order->timestamps = false;
                        $m_order->update(['synced' => 1]);
                    } else {
                        $m_shift = Shift::find($record->id);
                        $m_shift->timestamps = false;
                        $m_shift->update(['synced' => 1]);
                    }
                    $num_orders++;
                }
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
            }
        }

        return response()->json(['status' => 'ok', 'message' => "Orders sync: $num_orders"]);
    }

    public function pushOrders2(Request $request)
    {
        $unsyncedOrders = Order::whereRaw('(closed = 1 OR closed = 71 OR closed = 66)')->where('synced', 0);
        $unsyncedShifts = Shift::select('shifts.id', 'shifts.uuid', 'shifts.shop_id', 'shifts.user_id', DB::raw('NULL AS section_id, NULL AS discount_card_id, NULL AS client_id, NULL AS notes, NULL AS amount, NULL AS closed'), 'shifts.synced', 'shifts.created_at', 'shifts.updated_at')->where('synced', 0);

        $records = $unsyncedOrders->union($unsyncedShifts)->orderBy('created_at')->get();
        $rec = [
            "carduuid" => []
        ];
        $num_orders = 0;

        $endpointURL = env('1C_HOST');
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');
        $client = new Client();

        foreach ($records as $record) {
            if ($record->closed) {
                $this->syncOrder($record);
                $num_orders++;
            }

            if ($record->closed == null) {
                $this->syncShift($record);
            }
        }

        return response()->json(['status' => 'ok', 'message' => "Orders sync: $num_orders"]);
    }

    private function syncOrder(Order $order)
    {
       
        $endpointURL = env('1C_HOST');
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');
        $client = new Client();
        $paymentTypes = [];
      
        if ($order->paymentTypes->count()) {
            $paymentTypes['item'] = [];
            foreach ($order->paymentTypes as $paymentType) {
               $paymentTypes['item'][] = [
                    'PaymentType' => $paymentType->paymentType->code,
                    'PaymentAmount' => $paymentType->amount,
                    'CertificateNumber' => (($paymentType->certificate) ? $paymentType->certificate->number : null),
                    'createdAt' => $paymentType->created_at,
                    'userUuid' => $paymentType->user_uuid,
                    'paymentUuid' => $paymentType->payment_uuid,
                    

                ];
            }
        }
////            $section = Section::find($unsynchronizedOrder->section_id);
        $bodyData = [
            "UUID" => $order->uuid,
            "No" => $order->id,
            "CashType" => $order->paymentType ? (int)$order->paymentType->code : 0,
            "Dept" => "d42355c5-9e2c-11e9-8572-7085c27cf9e7",
//                "Dept" => $section->uuid,
            "At" => $order->created_at->format('d-m-Y H:i:s'),
            "CardNo" => (($order->discountCard) ? $order->discountCard->number : 0),
            "Operator" => $order->user->uuid,
            "State" => $order->closed,
            "TableNo" => 0,
            "CheckPrinted" => 1,
            "PredCheckPrinted" => 0,
            "ApartmentNo" => 0,
            "ClientCount" => 1,
            "NotExtSynced" => 1,
            "Discount" => (($order->discountCard) ? $order->discountCard->percent : 0),
            "ClosedAt" => $order->updated_at->format('d-m-Y H:i:s'),
            "PaymentTypes" => $paymentTypes,
        ];


        $prepareData = [
            'Reason' => 'Order',
            'Department' => env('dep_UUID'),
            'UUID' => (string)Uuid::generate(4),
            'Body' => $bodyData,
            'Details' => [],
            'Tickets' => [],
            'Coupons' => [],
            "PayNet" => []
        ];

        if ($order->details->count()) {
            foreach ($order->details as $orderDetail) {

                $orderDetail = [
                    "At" => $orderDetail->created_at->format('d-m-Y H:i:s'),
                    "ProductUUID" => (($orderDetail->serial_id) ? $orderDetail->serial->product->uuid : $orderDetail->product->uuid),
                    "BarCode" => $orderDetail->barcode,
                    "ModelCode" => $orderDetail->modelcode,
                    "Size" => $orderDetail->size_uuid,
                    "Color" => $orderDetail->color_uuid,
                    "OutOrder" => 1,
                    "Price" => $orderDetail->price,
                    "Counts" => $orderDetail->quantity,
                    "Printed" => 1,
                    "Discount" => $orderDetail->discount_percent,
                    "DiscountAmount" => $orderDetail->discount_amount,
                    "Totals" => ($orderDetail->price * $orderDetail->quantity),
                    "Total" => $orderDetail->total,
                    "UUID" => $orderDetail->order_detail_uuid,
                    "ProductName" => $orderDetail->product->name,
                    "SetInfo" => []
                ];
                array_push($prepareData["Details"], $orderDetail);
            }
        }

        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json'],
            'body' => json_encode($prepareData),

        );
        try {
            $response = $client->post($endpointURL, $options);
        } catch (ClientException $e) {
            return 500;
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }


        $data = json_decode($response->getBody()->getContents(), true);
        if ($data['ResCode'] == 0) {
            $order->update(['synced' => 1]);
//            $paymentTypes =
//                ["Reason" => "CashOps",
//                    "Department" => "468A726E",
//                    "UUID" => "a7299091-cd7e-44e8-8263-9c566fa63c37",
//                    "Items" => []
//                ];
//            if ($order->paymentTypes->count()) {
//                foreach ($order->paymentTypes as $paymentType) {
//                    $aux = [
//                        "UUID" => $paymentType->payment_uuid,
//                        "At" => $paymentType->created_at->format('d-m-Y H:i:s'),
//                        "Sums" => $paymentType->amount,
//                        "UserUUID" => $order->user->uuid,
//                        "OperationType" => 2,
//                        "Description" => "ккк",
//                        "PartnerType" => 600,
//                        "State" => 0,
//                        "No" => $paymentType->id,
//                        "PartnerID" => "079294106",
//                        "PartnerName" => "ввв",
//                        "ParentOrder" => $order->uuid
//                    ];
//                    array_push($paymentTypes["Items"], $aux);
//                }
//
//                $options = array(
//                    'auth' => [
//                        $username,
//                        $password
//                    ],
//                    'headers' => ['content-type' => 'application/json',],
//                    'body' => json_encode($paymentTypes),
//
//                );
//                try {
//                    $response = $client->post($endpointURL, $options);
//                } catch (ClientException $e) {
//                    return 500;
//                } catch (\Exception $e) {
//                    return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
//                }
//            }
        } else {
            return response()->json(['status' => 'ok', 'data' => $data['Message']], 400);
        }

        return response()->json(['status' => 'ok', 'data' => 'Ордер отправлен']);
    }

    private function syncShift($shift)
    {
        $endpointURL = env('1C_HOST');
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');
        $client = new Client();

        $user = User::where('id', $shift->user_id)->first();
        $prepareData = [
            'Reason' => 'Shifts',
            'Department' => env('dep_UUID'),
            'UUID' => (string)Uuid::generate(4),
            'Body' => [
                "UUID" => $shift->uuid,
                "ClosedAt" => $shift->updated_at->format('d-m-Y H:i:s'),
                "OperatorUUID" => $user->uuid,
                "No" => $shift->id
            ]
        ];
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => json_encode($prepareData),

        );
        try {
            $response = $client->post($endpointURL, $options);
        } catch (ClientException $e) {
            return 500;
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        $data = $response->getBody();
        if ($data) {
            $m_shift = Shift::find($shift->id);
            $m_shift->timestamps = false;
            $m_shift->update(['synced' => 1]);
        }
    }

    public function syncClosedOrder($orderId)
    {
        $order = Order::find($orderId);
        
      
        if ($order->closed === 0) {
            return response()->json(['status' => 'fail', 'data' => 'Ордер не закрыт'], 400);
        }
        return $this->syncOrder($order);
    }

    public function syncClosedShift($shiftId)
    {
        $shift = Shift::find($shiftId);

        $this->syncShift($shift);
        return response()->json(['status' => 'ok', 'data' => 'Смена отправлена']);
    }

    public function getUnsyncedOrdersAndShifts()
    {
        $unsyncedOrders = Order::whereRaw('(closed = 1 OR closed = 71 OR closed = 66)')->where('synced', 0);
        $unsyncedShifts = Shift::select('shifts.id', 'shifts.uuid', 'shifts.shop_id', 'shifts.user_id', DB::raw('NULL AS section_id, NULL AS discount_card_id, NULL AS client_id, NULL AS notes, NULL AS amount, NULL AS closed'), 'shifts.synced', 'shifts.created_at', 'shifts.updated_at')
            ->addSelect(\DB::raw('NULL AS paymentTypes'))
            ->where('synced', 0);

        $records = $unsyncedOrders->union($unsyncedShifts)->orderBy('created_at')->get();

        $results = [];

        foreach ($records as $record) {
            if ($record->closed) {
                $results[] = [
                    'id' => $record->id,
                    'type' => 'order',
                    'createdAt' => $record->created_at->toDateTimeString()
                ];
            }

            if ($record->closed == null) {
                $results[] = [
                    'id' => $record->id,
                    'type' => 'shift',
                    'createdAt' => $record->created_at->toDateTimeString()
                ];
            }
        }

        return response()->json(['status' => 'ok', 'data' => $results]);
    }

}
