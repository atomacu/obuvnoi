<?php

namespace App\Http\Controllers;

use App\Picture;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Client;

class PictureController extends Controller
{
    public function savePicture(Request $request)
    {
        // return $request;
        //    // $file = Input::file('pic');
        // $img = Image::make($request->img);
        // new Illuminate\Support\Facades\Response::make($img->encode('jpeg'));
        $picture = Picture::where('product_uuid', $request->product_uuid)->first();
        if ($picture) {
            $picture->product_uuid = $request->product_uuid;
            $picture->pic = $request->img;
            $picture->save();
        } else {
            $picture = new Picture;
            $picture->product_uuid = $request->product_uuid;
            $picture->pic = $request->img;
            $picture->save();
        }

        return 200;
    }

    public function deletePicture($id)
    {
        $picture = Picture::where('product_uuid', $id)->first();
        if (!empty($picture)) {
            $picture->delete();
            return 200;
        } else {
            return 500;
        }
    }

    /*
     * Extracts picture's data from DB and makes an image
     */
    public function showPicture($barcode)
    {
        $endpointURL = env('1C_HOST');
        $username = env('1C_USERNAME');
        $password = env('1C_PASSWORD');

        $stringJSON = '{"Reason":"PictureList","UUID": "19430e20-e71d-4a84-8932-e00719f35f7c","Department": "","Data": [' . intval($barcode) . ']}';
        $client = new Client();
        $options = array(
            'auth' => [
                $username,
                $password
            ],
            'headers' => ['content-type' => 'application/json',],
            'body' => $stringJSON,
        );

        try {
            $response = $client->post($endpointURL, $options);

            $data = $response->getBody();
            return $data;

        } catch (ClientException $e) {
            return "no_found";
        }
    }
}
