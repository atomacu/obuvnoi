<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
    //
    protected $fillable = [
        'shop_id', 'name', 'type', 'path', 'activated'
    ];

    public function scopeFiscal($query)
    {
        return $query->where('type', 'fiscal');
    }

    public function scopeReceipt($query)
    {
        return $query->where('type', 'receipt');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

}
