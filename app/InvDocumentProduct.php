<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvDocumentProduct extends Model
{
    //
    protected $fillable = [
        'uuid', 'document_id', 'serial_id', 'product_id', 'quantity_res', 'quantity', 'scanned', 'confirm_at',
    ];
    
    protected $dates = [
        'confirm_at',
        'created_at',
        'updated_at',
    ];

    public function document()
    {
        return $this->belongsTo(InvDocument::class, 'id', 'document_id');
    }

    public function serial()
    {
        return $this->belongsTo(Serial::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
