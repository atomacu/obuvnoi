<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metal extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'serial_details_id', 'name', 'probe'
    ];

}
