<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvDocument extends Model
{
    //
    protected $fillable = [
        'uuid', 'user_id', 'section_id', 'from_section_id', 'received_user_id', 'date_at', 'number', 'notes', 'type', 'local', 'synced',
    ];
    
    protected $dates = [
        'date_at',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function receivedUser()
    {
        return $this->belongsTo(User::class, 'received_user_id');
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }
    public function from_section()
    {
        return $this->belongsTo(Section::class, 'from_section_id');
    }

    public function products()
    {
        return $this->hasMany(InvDocumentProduct::class, 'document_id');
    }

}
