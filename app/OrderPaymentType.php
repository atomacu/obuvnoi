<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPaymentType extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'order_id','payment_uuid', 'payment_type_id', 'user_uuid',  'comment', 'payed', 'certificate_id', 'amount', 'change'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function certificate()
    {
        return $this->belongsTo(Certificate::class);
    }

}
