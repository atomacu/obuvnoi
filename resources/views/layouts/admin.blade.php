<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Trio Shop</title>

	<link rel="stylesheet" type="text/css" href="/codebase/webix.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/admin.css">

	<script type="text/javascript" src="/codebase/webix_debug.js"></script>
	<script type="text/javascript" src="/codebase/i18n/ru.js"></script>
	<script>
		webix.attachEvent('onBeforeAjax',
		    function(mode, url, params, xhr, headers, data, promise) {
		        if (webix.storage.session.get('user')) {
			        var token = webix.storage.session.get('user').token;
			        if (token) {
			        	headers['Authorization'] = 'Bearer ' + token;
			        }
		    	}
		    }
		);
		webix.attachEvent('onAjaxError',
		    function(xhr) {
		        if (xhr.status == 401) {
		        	if (location.pathname != '/')
		        		location.href = '/';
		        }
		    }
		);
setTimeout(() => {
	webix.ready(function () {
			getAdminAccount();
		});
}, 1000);
		

	</script>
	<script type="text/javascript" src="/js/lodash.min.js"></script>
	<script type="text/javascript" src="/js/moment.min.js"></script>
	<script type="text/javascript" src="/js/app.js"></script>
	<script type="text/javascript" src="/js/keypad.js"></script>
</head>
<body>

    <h1 class='admin-header'>
        <a href="/admin">
        	Admin Trio Shop
        </a>
        <a href="/cashbox" style="float: right; font-size: 16px; line-height: 26px;">В приложение</a>
    </h1>
    <div class='admin-main-area'>
        <ul class='menu'>
        	<li><span><strong>СИСТЕМА</strong></span>
        		<ul>
		            <li><a href="/admin/printers">Принтеры</a></li>
		            <li><a href="/admin/settings">Настройки</a></li>
	        	</ul>
        	</li>
        	<li><span><strong>СПРАВОЧНИКИ</strong></span>
        		<ul>
		            <li><a href="/admin/shops">Магазины</a></li>
		            <li><a href="/admin/users">Пользователи</a></li>
		            <li><a href="/admin/discount-cards">Дисконтные карты</a></li>
		            <li><a href="/admin/certificates">Сертификаты</a></li>
	        	</ul>
        	</li>
        </ul>
        <content class="admin-content">
        	@yield("content")
        </content>
    </div>

</body>
</html>