<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Trio Shop</title>

	<link rel="stylesheet" type="text/css" href="/codebase/webix.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">

	<script type="text/javascript" src="/codebase/webix.js"></script>
	<script type="text/javascript" src="/codebase/i18n/ru.js"></script>
	<script>
		webix.attachEvent('onBeforeAjax',
		    function(mode, url, params, xhr, headers, data, promise) {
		        if (webix.storage.session.get('user')) {
			        var token = webix.storage.session.get('user').token;
			        if (token) {
			        	headers['Authorization'] = 'Bearer ' + token;
			        }
		    	}
		    }
		);
		webix.attachEvent('onAjaxError',
		    function(xhr) {
		        if (xhr.status == 401) {
		        	if (location.pathname != '/' && location.pathname != '/reports')
		        		location.href = '/';
		        }
		    }
		);
	setTimeout(() => {
		webix.ready(function () {
			getAccount();
		});
	}, 1000);	
	

	</script>
	<script type="text/javascript" src="/js/lodash.min.js"></script>
	<script type="text/javascript" src="/js/moment.min.js"></script>
	<script type="text/javascript" src="/js/app.js?1234567890"></script>
	<script type="text/javascript" src="/js/keypad.js"></script>
</head>
<body oncontextmenu="return false" onselectstart="return false" ondragstart="return false">

	@yield("content")

</body>
</html>