@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>
		var grid = {
		    view: 'datatable',
		    id: 'dt_records',
		    select: true,
		    autoheight: true,
		    scroll: false,
		    columns: [
		        { id: 'number', header: 'Номер карты', fillspace: true },
		        { id: 'full_name', header: 'Имя', width: 200 },
		        { id: 'birthday_at', header: { text: 'Дата рождения', css: 'text-center'}, width: 130, css: 'text-center', format: webix.i18n.dateFormatStr },
		        { id: 'percent', header: { text: 'Процент скидки', css: 'text-center'}, width: 140, css: 'text-center' },
		        { id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, width: 100, css: 'text-center' },
		    ],
		    url: '/admin/discount-cards/data',
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [
		        grid
		    ]
		});

	</script>
@endsection