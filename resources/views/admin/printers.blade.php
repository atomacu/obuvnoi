@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>
		var printTypes = [
		    { id: 'receipt', value: 'Receipt' },
		    { id: 'fiscal', value: 'Fiscal' }
		];

		var shops = new webix.DataCollection({
		  scheme:{
		  	$init: function(obj) { obj.value = obj.name; }
		  },
		  url: '/admin/shops/data'
		});

		var toolbar = {
		    view: 'toolbar',
		    cols:[
		        { view: 'button', value: 'Добавить', width: 120, click: function() {
		            $$('dt_records').add({shop_id: null, name: '', type: '', path: '', activated: 1});
		            var lastId = $$('dt_records').getLastId();
		            $$('dt_records').select(lastId);
		            $$('dt_records').editCell(lastId, 'shop_id');
		        }},
		    ]
		};

		var remove = function(e, id, trg) {
		    webix.confirm({text: 'Удалить?', ok: 'Да', cancel: 'Нет', callback: function(result) {
		            if (result) {
		                $$('dt_records').remove(id);
		            }
		        }
		    });
		    return false;
		};

		var grid = {
		    view: 'datatable',
		    id: 'dt_records',
		    select: true,
		    autoheight: true,
		    scroll: false,
		    editable: true,
		    editaction: 'dblclick',
		    checkboxRefresh: true,
		    fixedRowHeight: false,
		    rowLineHeight: 23,
		    columns: [
		        { id: 'shop_id', header: 'Магазин', editor: 'richselect', collection: shops, width: 150 },
		        { id: 'name', header: 'Наименование', editor: 'text', fillspace: true },
		        { id: 'type', header: 'Тип принтера', editor: 'richselect', collection: printTypes, width: 150 },
		        { id: 'path', header: 'Путь к принтеру (принт-сервера)', editor: 'text', width: 300 },
		        { id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, width: 100, css: 'text-center' },
		        { id: 'trash', header: '&nbsp;', width: 35, template: '<span class="webix_icon fa-trash-o"></span>' },
		    ],
		    on: {
		        onAfterLoad: function() {
		            webix.delay(function() {
		                this.myAdjustRowHeight(['name'], true);
		                this.render();
		            }, this);
		        },
		        onDataUpdate: function(id, data) {
		            this.myAdjustRowHeight(['name'], true, id);
		            this.render();
		        }
		    },
		    onClick: {
		        'fa-trash-o': remove
		    },
		    url: '/admin/printers/data',
		    save: 'rest->/admin/printers/data'
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [
		        toolbar, grid
		    ]
		});

	</script>
@endsection