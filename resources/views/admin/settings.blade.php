@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>
		
		var form_elements = [];

		@foreach($settings as $setting)
			form_elements.push({ view: '{{ $setting->type }}', value: '{{ str_replace("\n", "\\n", $setting->value) }}', label: '{{ $setting->label }}:', labelAlign: 'right', labelWidth: 300, width: 600, name: '{{ $setting->name }}' });
		@endforeach
		form_elements.push({ view: 'button', value: 'Сохранить', align: 'center', width: 200, click: saveForm });

		var form_settings = {
			view: 'form',
			id: 'frm_settings',
			scroll: false,
			elements: form_elements
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [ form_settings ]
		});

		webix.extend($$('frm_settings'), webix.ProgressBar);

		function saveForm() {
			show_progress_icon('frm_settings');
			var form_data = $$('frm_settings').getValues();

			webix.ajax().post('/admin/settings', form_data)
				.then(function (result) {
					hide_progress_icon('frm_settings');
					webix.message({type: 'success', text: 'Сохранено!'});
				})
				.fail(function (xhr) {
					var response = JSON.parse(xhr.response);
					webix.message({type: 'error', text: response.message});
				});
		}

	</script>
@endsection