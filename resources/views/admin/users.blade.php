@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>
		var permissions = [
		    { id: 1, value: 'Администратор' },
		    { id: 2, value: 'Пользователь' },
		];

		var toolbar = {
		    view: 'toolbar',
		    cols:[
		        { view: 'button', value: 'Добавить', width: 120, click: function() {
		            $$('dt_records').add({name: 'user_name', password: '', permission: 0, activated: 1});
		            var lastId = $$('dt_records').getLastId();
		            $$('dt_records').select(lastId);
		            $$('dt_records').editCell(lastId, 'name');
		        }},
		    ]
		};

		var remove = function(e, id, trg) {
		    webix.confirm({text: 'Удалить?', ok: 'Да', cancel: 'Нет', callback: function(result) {
		            if (result) {
		                $$('dt_records').remove(id);
		            }
		        }
		    });
		    return false;
		};

		var grid = {
		    view: 'datatable',
		    id: 'dt_records',
		    select: true,
		    autoheight: true,
		    scroll: false,
		    editable: true,
		    editaction: 'dblclick',
		    checkboxRefresh: true,
		    fixedRowHeight: false,
		    rowLineHeight: 23,
		    columns: [
		        { id: 'name', header: 'Имя пользователя', editor: 'text', fillspace: true },
		        { id: 'password', header: 'Пинкод', editor: 'text', width: 150 },
		        { id: 'permission', header: 'Права', editor: 'richselect', collection: permissions, width: 150 },
		        { id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, width: 100, css: 'text-center' },
		        { id: 'trash', header: '&nbsp;', width: 35, template: '<span class="webix_icon fa-trash-o"></span>' },
		    ],
		    on: {
		        onAfterLoad: function() {
		            webix.delay(function() {
		                this.myAdjustRowHeight(['name'], true);
		                this.render();
		            }, this);
		        },
		        onDataUpdate: function(id, data) {
		            this.myAdjustRowHeight(['name'], true, id);
		            this.render();
		        }
		    },
		    onClick: {
		        'fa-trash-o': remove
		    },
		    url: '/admin/users/data',
		    save: 'rest->/admin/users/data'
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [
		        toolbar, grid
		    ]
		});

	</script>
@endsection