@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>

		var toolbar = {
		    view: 'toolbar',
		    cols:[
		        { view: 'button', value: 'Добавить', width: 120, click: function() {
		            $$('dt_records').add({name: 'shop_name', dept_uuid: '', address: '', activated: 1});
		            var lastId = $$('dt_records').getLastId();
		            $$('dt_records').select(lastId);
		            $$('dt_records').editCell(lastId, 'name');
		        }},
		    ]
		};

		var remove = function(e, id, trg) {
		    webix.confirm({text: 'Удалить?', ok: 'Да', cancel: 'Нет', callback: function(result) {
		            if (result) {
		                $$('dt_records').remove(id);
		            }
		        }
		    });
		    return false;
		};

		var sections = new webix.DataCollection({
		  scheme:{
		  	$init: function(obj) { obj.value = obj.name; }
		  },
		  url: '/data/sections'
		});

		var grid = {
		    view: 'datatable',
		    id: 'dt_records',
		    select: true,
		    autoheight: true,
		    scroll: false,
		    editable: true,
		    editaction: 'dblclick',
		    checkboxRefresh: true,
		    fixedRowHeight: false,
		    rowLineHeight: 23,
		    columns: [
		        { id: 'name', header: 'Наименование магазина', editor: 'text', fillspace: true },
		        { id: 'dept_uuid', header: 'Dept UUID', editor: 'text', width: 100 },
		        { id: 'section_id', header: 'Секция', editor: 'richselect', collection: sections, width: 150 },
		        { id: 'address', header: 'Адрес', editor: 'popup', width: 300 },
		        { id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, width: 100, css: 'text-center' },
		        { id: 'trash', header: '&nbsp;', width: 35, template: '<span class="webix_icon fa-trash-o"></span>' },
		    ],
		    on: {
		        onAfterLoad: function() {
		            webix.delay(function() {
		                this.myAdjustRowHeight(['name', 'address'], true);
		                this.render();
		            }, this);
		        },
		        onDataUpdate: function(id, data) {
		            this.myAdjustRowHeight(['name', 'address'], true, id);
		            this.render();
		        }
		    },
		    onClick: {
		        'fa-trash-o': remove
		    },
		    url: '/admin/shops/data',
		    save: 'rest->/admin/shops/data'
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [
		        toolbar, grid
		    ]
		});

	</script>
@endsection