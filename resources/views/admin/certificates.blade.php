@extends('layouts.admin')

@section('content')
	<div id='app-container'></div>
	<script>
		webix.i18n.parseFormat = "%Y-%m-%d %H:%i:%s";
		webix.i18n.setLocale();

		var toolbar = {
		    view: 'toolbar',
		    cols:[
		        { view: 'button', value: 'Добавить', width: 120, click: function() {
		            $$('dt_records').add({number: '', name: '', phone: '', amount: '', valid_at: '', activated: 1 });
		            var lastId = $$('dt_records').getLastId();
		            $$('dt_records').select(lastId);
		            $$('dt_records').editCell(lastId, 'number');
		        }},
		    ]
		};
		var remove = function(e, id, trg) {
		        webix.confirm({text: 'Удалить?', ok: 'Да', cancel: 'Нет', callback: function(result) {
		                if (result) {
		                    $$('dt_records').remove(id);
		                }
		            }
		        });
		        return false;
		    };

		webix.editors.$popup = {
		    date: {
		        view: 'popup',
		        body: { view: 'calendar', timepicker: true, icons: true }
		    }
		};
		
		var grid = {
		    view: 'datatable',
		    id: 'dt_records',
		    select: true,
		    autoheight: true,
		    scroll: false,
		    editable: true,
		    editaction: 'dblclick',
		    checkboxRefresh: true,
		    fixedRowHeight: false,
		    rowLineHeight: 23,
		    columns: [
		        { id: 'number', header: ['Номер сертификата', { content: 'textFilter' }], editor: 'text', width: 180 },
		        { id: 'name', header: ['Имя клиента', { content: 'textFilter' }], editor: 'text', fillspace: true },
		        { id: 'phone', header: ['Телефон', { content: 'textFilter' }], editor: 'text', width: 180 },
		        { id: 'amount', header: { text: 'Цена', css: 'text-right'}, editor: 'text', width: 100, css: 'text-right', format: webix.i18n.priceFormat },
		        { id: 'valid_at', header: { text: 'Действителен до', css: 'text-center'}, editor: 'date', width: 150, css: 'text-center', format: webix.i18n.fullDateFormatStr },
		        { id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, width: 100, css: 'text-center' },
		        { id: 'created_at', header: { text: 'Дата продажи', css: 'text-center'}, editor: 'date', width: 150, css: 'text-center', format: webix.i18n.fullDateFormatStr },
		        { id: 'trash', header: '&nbsp;', width: 35, template: '<span class="webix_icon fa-trash-o"></span>' },
		    ],
		    onClick: {
		        'fa-trash-o': remove
		    },
		    scheme: {
		        $change: function(item) {
		            var dt = moment();
		            var dt_item = moment(item.valid_at);
		            var diff = dt_item.diff(dt, 'days');
		            if (diff <= 0) {
		            	item.$css = 'row-warning';
		            }
		            if (item.activated == 0) {
		                item.$css = 'row-disable';
		            }
		        }
		    },
		    url: '/admin/certificates/data',
		    save: 'rest->/admin/certificates/data'
		};

		webix.ui({
		    type: 'space',
		    container: 'app-container',
		    rows: [
		    	toolbar,
		        grid
		    ]
		});

	</script>
@endsection