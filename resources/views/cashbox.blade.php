@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="codebase/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="/js/moment.min.js"></script>
    <style>
        .trioViewGroupLIst > div > div,
        .trioViewDataView > div > div > div {
            margin: 2px;
            border: 1px solid #aaa;
            border-radius: 10px;
            float: left;
        }

        .trioSize1 > div > div > .trio_title {
            width: 100px !important;
        }

        .trioSize2 > div > div,
        .trioSize2 > div > div > div {
            /*height: unset !important;*/
            border: none;
            margin: 0;
            line-height: 20px;
        }

        .picture_selector {
            background-size: contain;
            border: 1px solid #A4BED4;
            background-position: center;
        }

        .trioViewDataView .webix_scroll_cont, .trioViewDataView .webix_scroll_cont > div {
            overflow: unset !important;
        }

        .trio_title {
            /*width: 150px;*/
            position: relative;
            height: 43px;
            overflow: hidden;
            line-height: 14px;
            font-size: 11px; /* marimea la font,  meniu  */
        }

        .recept_size {
            font-size: 80px;
            margin-bottom: 10px;
        }
    </style>
    <script>
        webix.ready(function () {
            if (window.performance.navigation.type === 1) {
                onLogout();
            }
        });

        const TEXTS = {
            TOOLBAR: {
                OPERATION: {
                    CLOSE_SHIFT: 'Закрытие смены',
                    CASH_REGISTER: 'Кассовый аппарат',
                    CLOSE_OPEN_CHECK: 'Закрыть открытый чек',
                    REPORT_X: 'Отчёт X',
                    REPORT_Z: 'Отчёт Z',
                    REPORTS: 'Отчёты...',
                    ACCOUNT_LOG: 'Журнал счетов...',
                    EXCHANGE_WITH_BACK_OFFICE: 'Обмен с бэк-офисом',
                    UPDATE_ITEM: 'Обновить номенклатуру',
                    UPDATE_USERS: 'Обновить пользователей',
                    UPDATE_IMAGES: 'Обновить картины',
                    DISCOUNT_CARDS: 'Дисконтные карты',
                    WAREHOUSES: 'Склады',
                    PRODUCT_COLORS: 'Цвета Продуктов',
                    PRODUCT_SIZES: 'Размеры Продуктов',
                    CHECKS_UNLOADING: 'Выгрузка чеков',
                },
                TOP: {
                    BARCODE: 'Баркод',
                    GOODS: 'Товары',
                    NEW_ORDER: 'Новый',
                    CLOSE: 'Закрыть',
                    DISCOUNT: 'Скидка',
                    DELETE: 'Удалить',
                    REFUND: 'Возврат',
                    CLIENT: 'Клиент',
                    PRODUCT: 'Остатки',
                    PRINT_Z_REPORT: 'Распечатать Z отчёт?',
                    PRINT_Z_REPORT_CONFIRM: 'Да',
                    PRINT_Z_REPORT_CANCEL: 'Нет',
                    SELECTED_SECTION: 'Выбрана секция',
                    SELECTED_SECTION_OK: 'Да',
                    SELECTED_SECTION_CANCEL: 'Отмена',

                }
            },
            COMMON: {
                INSERT_BARCODE: 'Введите баркод',
                LOADING: 'Загрузка...',
                CANCEL_ORDER_SYNC: 'Не синхронизированные заказы'
            },
            MAIN_TABLE: {
                PRODUCTS: 'Товар',
                BARCODE: 'Баркод',
                MODEL: 'Код',
                MODELCODE: 'Модель',
                COLOR: 'Цвет',
                SIZE: 'Размер',
                PRICE: 'Цена',
                QUANTITY: 'Кол.',
                INSERT_QUANTITY: 'Введите количество',
                SUM: 'Сумма',
                INSERT_DISCOUNT_PERCENTAGE: 'Введите процент скидки',
                DISCOUNT: 'Скидка',
                INSERT_DISCOUNT_AMOUNT: 'Введите сумму скидки',
                TOTAL: 'Итого',
                VALIDATION_MESSAGES: {
                    MAX_PERCENTAGE_DISCOUNT_ERROR: 'Процент скидки не должен быть больше 100%.',
                    MAX_AMOUNT_DISCOUNT_ERROR: 'Сумма скидки не должна быть больше суммы.',
                    DISCOUNT_ONLY_ADMINS_ERROR: 'Данная скидка доступна только для администратора!',
                }
            },
            RETURN: {
                OPEN: 'Открытый',
                CLOSED: 'Закрытый',
                MISTAKEN: 'Ошибочный',
                RETURN_OPEN: 'Возврат открытый',
                RETURN_CLOSED: 'Возврат закрытый',

            }
        };
        var order_selected, order_item_selected, report_selected;
        var lastShift_at = moment().format('YYYY-MM-DD 00:00');
        var printerReceipt, printerFiscal;
        var is_process = false;
        var certificate_type_code = 11;
        var payment_types = new webix.DataCollection({
            url: '/data/payment-types',
        });
        var payment_types2 = new webix.DataCollection({
            url: '/data/payment-types',
            scheme: {
                $init: function (obj) {
                    obj.value = obj.name;
                }
            }
        });
        var sections = new webix.DataCollection({
            url: '/data/sections',
            scheme: {
                $init: function (obj) {
                    obj.value = obj.name;
                }
            }
        });
        var shops = new webix.DataCollection({
            url: '/data/shops',
            scheme: {
                $init: function (obj) {
                    obj.value = obj.name;
                }
            }
        });
        var consultants = new webix.DataCollection({
            url: '/data/consultants',
            scheme: {
                $init: function (obj) {
                    let name = (obj.name).split(' ');
                    obj.value = name[0];
                }
            }
        });

        var smOperations = [
            {id: 1, value: TEXTS.TOOLBAR.OPERATION.CLOSE_SHIFT},
            // { id: 18, value: 'Удвлить Ордер' },
            {$template: 'Separator'},
            // {
            //     id: 100, value: TEXTS.TOOLBAR.OPERATION.CASH_REGISTER,
            //     submenu: [
            //         {id: 2, value: TEXTS.TOOLBAR.OPERATION.CLOSE_OPEN_CHECK},
            //         {id: 3, value: TEXTS.TOOLBAR.OPERATION.REPORT_X},
            //         {id: 4, value: TEXTS.TOOLBAR.OPERATION.REPORT_Z},
            //         //{ id: 5, value: 'Служебное внесение/изьятие' },
            //     ]
            // },
            // {$template: 'Separator'},
            {id: 6, value: TEXTS.TOOLBAR.OPERATION.REPORTS},
            // { id: 17, value: 'Рицэпты' },
            // {id:16, value:'Рицэпты Ордера'},
            {id: 7, value: TEXTS.TOOLBAR.OPERATION.ACCOUNT_LOG},
            {$template: 'Separator'},
            {
                id: 101, value: TEXTS.TOOLBAR.OPERATION.EXCHANGE_WITH_BACK_OFFICE,
                submenu: [
                    {id: 8, value: TEXTS.TOOLBAR.OPERATION.UPDATE_ITEM},
                    {id: 9, value: TEXTS.TOOLBAR.OPERATION.UPDATE_USERS},
                    // {id: 20, value: TEXTS.TOOLBAR.OPERATION.UPDATE_IMAGES},
                    // {id: 11, value: TEXTS.TOOLBAR.OPERATION.DISCOUNT_CARDS},
                    {id: 13, value: TEXTS.TOOLBAR.OPERATION.WAREHOUSES},
                    {id: 16, value: TEXTS.TOOLBAR.OPERATION.PRODUCT_COLORS},
                    {id: 17, value: TEXTS.TOOLBAR.OPERATION.PRODUCT_SIZES},
                    {$template: 'Separator'},
                    {id: 10, value: TEXTS.TOOLBAR.OPERATION.CHECKS_UNLOADING},
                    {$template: 'Separator'},
                    // { id: 12, value: 'Инвентаризация' },
                ],
                config: {
                    width: 220
                }
            },
            //  {id:15, value:'Возврат'},
        ];

        var toolbar_top = {
            view: 'toolbar',
            height: 80,
            cols: [
                {
                    view: 'search', css: 'font-size:20px', placeholder: TEXTS.TOOLBAR.TOP.BARCODE, width: 220, id: 'search',
                    on: {
                        // onSearchIconClick: winProducts2,
                        onEnter: function (ev) {
                            var $that = this;
                            var value = this.getValue();
                            webix.delay(function () {
                                $that.setValue('');
                            });
                            if (value != '')
                                getByBarcode2(value);
                        }
                    }
                },
                // {view: 'button', type: 'iconButtonTop', icon: 'fas fa-cube', label: TEXTS.TOOLBAR.TOP.GOODS, width: 80, click: winProducts},
                {},
                {view: 'button', type: 'iconButtonTop', icon: 'cart-plus', label: TEXTS.TOOLBAR.TOP.NEW_ORDER, width: 80, click: addOrder},
                {view: 'button', type: 'iconButtonTop', icon: 'money', label: TEXTS.TOOLBAR.TOP.CLOSE, width: 80, click: payOrder},
                // {view: 'button', type: 'iconButtonTop', icon: 'fas fa-percent', label: TEXTS.TOOLBAR.TOP.DISCOUNT, width: 80, click: openDiscount},
                {view: 'button', type: 'iconButtonTop', icon: 'trash-o', width: 80, label: TEXTS.TOOLBAR.TOP.DELETE, click: removeOrder},
                {view: 'button', type: 'iconButtonTop', icon: 'reply', width: 80, label: TEXTS.TOOLBAR.TOP.REFUND, click: returnProduct},
                // {
                //     view: 'button', type: 'iconButtonTop', icon: 'fas fa-address-card', label: TEXTS.TOOLBAR.TOP.CLIENT, width: 80, click: function () {
                //         winReports();
                //         keyboard('client_uuid', 'Карта Клиента');
                //         $$('report_list').select(6);
                //     }
                // },
                {
                    view: 'button', type: 'iconButtonTop', icon: 'fas fa-pie-chart', label: TEXTS.TOOLBAR.TOP.PRODUCT, width: 80, click: function () {
                        winReports();
                        $$('report_list').select(3);
                        productBarcode()
                    }
                },
                // {view:'button', type: 'iconButtonTop',icon: "fas fa-dollar" ,label:"PrePay",width:80,	click:prePayOrder},
                // {view:'button', type: 'iconButtonTop', icon:"fas fa-pencil", label:"Recept",width:80,	click:newRecept},
                {},
                {
                    view: 'menu', width: 80, height: 80,
                    data: [{icon: 'tasks', type: 'iconButtonTop', value: '', submenu: smOperations}],
                    type: {subsign: true}, openAction: 'click',
                    submenuConfig: {
                        width: 200, on: {
                            onItemClick: function (id) {
                                id = parseInt(id);
                                switch (id) {
                                    case 1:
                                        closeShift();
                                        break;
                                    case 2:
                                        printFiscal('CloseReceipt');
                                        break;
                                    case 3:
                                        printFiscal('report', 'X;');
                                        break;
                                    case 4:
                                        webix.confirm({
                                            text: TEXTS.TOOLBAR.TOP.PRINT_Z_REPORT,
                                            ok: TEXTS.TOOLBAR.TOP.PRINT_Z_REPORT_CONFIRM,
                                            cancel: TEXTS.TOOLBAR.TOP.PRINT_Z_REPORT_CANCEL,
                                            type: 'confirm-warning',
                                            callback: function (result) {
                                                if (result) {
                                                    printFiscal('report', 'Z;');
                                                }
                                            }
                                        });
                                        break;
                                    case 5:
                                        printFiscal();
                                        break;
                                    case 6:
                                        winReports();
                                        break;
                                    case 7:
                                        accountsJournal();
                                        break;
                                    case 8:
                                        syncProducts();
                                        break;
                                    case 20:
                                        syncPictures();
                                        break;
                                    case 9:
                                        winUsers()
                                        break;
                                    case 10:
                                        pushOrders();
                                        break;
                                    case 11:
                                        winDiscountCards();
                                        break;
                                    case 12:
                                        location.href = '/inventory';
                                        break;
                                    case 13:
                                        winSections();
                                        break;
                                    case 15:
                                        returnProduct()
                                        break;
                                    case 16:
                                        winColors()
                                        break;
                                    case 17:
                                        winSizes()
                                        break;
                                    case 18:
                                        removeOrder()
                                        break;
                                }
                            }
                        }
                    },
                    css: 'menu-button'
                },
                {
                    view: 'button', type: 'iconButtonTop', icon: 'cogs', width: 80, id: 'btnSettings', hidden: true, href: '/admin', click: function () {
                        location.href = this.config.href;
                    }
                }
            ]
        };

        webix.ui({
            view: 'popup',
            id: 'ddNewOrder',
            body: {
                view: 'list',
                autoheight: true,
                select: false,
                css: 'list-hover',
                data: [],
                template: '#name#',
                on: {
                    onItemClick: function (id, e, node) {
                        var item = this.getItem(id);
                        var $that = this;
                        webix.confirm({
                            text: `${TEXTS.TOOLBAR.TOP.SELECTED_SECTION}: <br><span class="text-danger">${item.name}</span>`,
                            ok: TEXTS.TOOLBAR.TOP.SELECTED_SECTION_OK,
                            cancel: TEXTS.TOOLBAR.TOP.SELECTED_SECTION_CANCEL,
                            type: 'confirm-warning',
                            callback: function (result) {
                                if (result) {
                                    addOrder(id);
                                    $that.getTopParentView().hide();
                                }
                            }
                        });
                        //addOrder(id);
                        //this.getTopParentView().hide();
                    }
                },
                url: '/data/sections'
            }
        });

        function barcodeKeypad() {
            titleKeyPad = TEXTS.COMMON.INSERT_BARCODE;
            objValue = $$('search');
            keyPadOptions.callback = getByBarcode;
            popupKeyPad.show();
        }

        function barcodeKeypad2() {
            titleKeyPad = TEXTS.COMMON.INSERT_BARCODE;
            objValue = $$('search2');
            keyPadOptions.callback = getByBarcode;
            popupKeyPad.show();
        }

        var toolbar_bottom = {
            view: 'toolbar',
            height: 50,
            cols: [
                {view: 'icon', id: 'unsynced_icon', icon: 'hourglass-half', width: 50, badge: '', tooltip: TEXTS.COMMON.CANCEL_ORDER_SYNC},
                {view: 'label', id: 'lblShop', label: '', align: 'center'},
                {view: 'button', id: 'btnLogout', type: 'icon', icon: 'sign-out', label: '', autowidth: true, click: onLogout},
            ]
        };

        var dtSerials = {
            view: 'datatable',
            id: 'order_details',
            autoConfig: true,
            select: 'row',
            scroll: false,
            editable: true,
            //math: true,
            footer: true,
            columns: [
                {
                    id: 'product_name',
                    header: TEXTS.MAIN_TABLE.PRODUCTS,
                    fillspace: true,
                    footer: {
                        text: 'Всего:',
                        colspan: 3,
                        css: 'text-right text-footer'
                    }
                },
                {
                    id: 'modelcode',
                    header: TEXTS.MAIN_TABLE.MODELCODE,
                    adjust: 'data',
                    minWidth: 100,
                },
                {
                    id: 'picture',
                    header: '&nbsp;',
                    width: 35,
                    template: '<span class="webix_icon fa-eye"></span>'
                },
                // { id: 'section_name', header: 'Секция', adjust: 'data' },
                {
                    id: 'barcode',
                    header: TEXTS.MAIN_TABLE.BARCODE,
                    adjust: true,
                    minWidth:140,
                    css: 'text-center'
                },
                {
                    id: 'model',
                    header: TEXTS.MAIN_TABLE.MODEL,
                    adjust: 'data',
                    width: 100,
                },
                //	{ id: 'vendorcode', header: 'Арт.', adjust: 'data' },
                // { id: 'consultant_id', header: { text: 'Конс.'}, css: 'edit-flag', adjust: 'header', editor: 'richselect', suggest: {fitMaster: false, width: 70}, collection: consultants },
                {
                    id: 'color',
                    header: {
                        text: TEXTS.MAIN_TABLE.COLOR,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                },
                {
                    id: 'size',
                    header: {
                        text: TEXTS.MAIN_TABLE.SIZE,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                },
                {
                    id: 'color_uuid',
                    header: {
                        text: TEXTS.MAIN_TABLE.COLOR,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                    hidden: true
                },
                {
                    id: 'size_uuid',
                    header: {
                        text: TEXTS.MAIN_TABLE.SIZE,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                    hidden: true
                },
                {
                    id: 'price',
                    header: {
                        text: TEXTS.MAIN_TABLE.PRICE,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                    format: webix.i18n.priceFormat
                },
                {
                    id: 'quantity',
                    header: {
                        text: TEXTS.MAIN_TABLE.QUANTITY,
                        css: 'text-center'
                    },
                    css: 'text-center edit-flag',
                    width: 50,
                    editor: 'keypadPopupEditor',
                    editorTitle: TEXTS.MAIN_TABLE.INSERT_QUANTITY,
                    footer: {
                        content: 'summColumn',
                        css: 'text-center text-footer'
                    }
                },
                {
                    id: 'sum',
                    header: {
                        text: TEXTS.MAIN_TABLE.SUM,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80,
                    /*math: '[$r, price] * [$r, quantity]',*/
                    format: webix.i18n.priceFormat,
                    footer: {
                        content: 'summColumn',
                        css: 'text-right text-footer'
                    }
                },
                {
                    id: 'discount_percent',
                    header: {
                        text: '%',
                        css: 'text-center cell-warning'
                    },
                    css: 'text-center edit-flag cell-warning',
                    width: 60,
                    editor: 'keypadPopupEditor',
                    editorTitle: TEXTS.MAIN_TABLE.INSERT_DISCOUNT_PERCENTAGE,
                    format: webix.i18n.priceFormat
                },
                {
                    id: 'discount_amount',
                    header: {
                        text: TEXTS.MAIN_TABLE.DISCOUNT,
                        css: 'text-right cell-warning'
                    },
                    css: 'text-right edit-flag cell-warning',
                    width: 80,
                    editor: 'keypadPopupEditor',
                    editorTitle: TEXTS.MAIN_TABLE.INSERT_DISCOUNT_AMOUNT,
                    footer: {
                        content: 'summColumn',
                        css: 'text-right text-footer'
                    }
                },
                {
                    id: 'total',
                    header: {
                        text: TEXTS.MAIN_TABLE.TOTAL,
                        css: 'text-right'
                    },
                    css: 'text-right',
                    width: 80, /*math: '[$r, sum] - [$r, discount_amount]',*/
                    format: webix.i18n.priceFormat,
                    footer: {content: 'summColumn', css: 'text-right text-footer'}
                },

                {
                    id: 'trash',
                    header: '&nbsp;',
                    width: 35,
                    template: '<span class="webix_icon fa-trash-o"></span>'
                },
            ],
            scheme: {
                $init: function (obj) {
                    if (obj.serial) {
                        obj.product_name = (obj.serial.product.name);
                        if (obj.serial.section) obj.section_name = (obj.serial.section.name);
                        obj.barcode = (obj.serial.barcode) + '&nbsp;';
                        obj.vendorcode = (obj.serial.vendorcode) + '&nbsp;';
                        //obj.product_id = 'null';
                    }
                    if (obj.product) {
                        obj.product_name = (obj.product.name);
                        obj.barcode = obj.barcode;
                        obj.model = obj.product.barcode;
                        obj.vendorcode = (obj.product.vendorcode) + '&nbsp;';
                        //obj.serial_id = 'null';
                    }
                }
            },
            height: 460,
            minHeight: 50,
            on: {
                onBeforeLoad: function () {
                    this.showOverlay(TEXTS.COMMON.LOADING);
                },
                onAfterLoad: function () {
                    this.hideOverlay();
                    calculateOrder(this);
                },
                onAfterAdd: function () {
                    calculateOrder(this);
                },
                onAfterDelete: function (id) {
                    //	$$('serial-details').clearAll();
                    calculateOrder(this);
                },
                onBeforeEditStart: function (id) {
                    var item = this.getSelectedItem();
                    if (id.column == 'quantity' && (item.serial)) {
                        return false;
                    }
                    if (item.product && item.product.product_type == 6) {
                        return false;
                    }
                    if ((id.column == 'discount_percent' || id.column == 'discount_amount') && ((item.product && !item.product.allow_discount) || (item.serial && !item.serial.product.allow_discount))) {
                        return false;
                    }
                    return true;
                },
                onAfterEditStop: function (state, editor, ignoreUpdate) {
                    //math: '([$r, sum] * [$r, discount_percent]) / 100';
                    var that = this;
                    var serialId = that.getSelectedId();
                    if (serialId) {
                        var serialItem = that.getSelectedItem();
                        if (editor.column === 'discount_percent') {
                            if (parseFloat(state.value) > 100) {
                                serialItem.discount_percent = state.old;
                                webix.message({type: 'error', text: TEXTS.MAIN_TABLE.VALIDATION_MESSAGES.MAX_PERCENTAGE_DISCOUNT_ERROR});
                                return false;
                            }
                            serialItem.discount_amount = (serialItem.sum * state.value / 100).toFixed(2);
                        }
                        if (editor.column === 'discount_amount') {
                            if (parseFloat(state.value) > parseFloat(serialItem.price)) {
                                serialItem.discount_amount = state.old;
                                webix.message({type: 'error', text: TEXTS.MAIN_TABLE.VALIDATION_MESSAGES.MAX_AMOUNT_DISCOUNT_ERROR});
                                return false;
                            }
                            serialItem.discount_percent = (state.value * 100 / serialItem.sum).toFixed(2);
                        }
                        if (editor.column === 'quantity') {
                            var sum = serialItem.price * serialItem.quantity;
                            if (serialItem.discount_percent > 0) {
                                serialItem.discount_amount = (sum * serialItem.discount_percent / 100).toFixed(2);
                            }
                        }
                        if (serialItem.discount_percent >= 100 && accountInfo.permission != 2) {
                            serialItem.discount_percent = state.old;
                            webix.message({type: 'error', text: TEXTS.MAIN_TABLE.VALIDATION_MESSAGES.DISCOUNT_ONLY_ADMINS_ERROR});
                            return false;
                        }

                        webix.delay(function() {
                            that.updateItem(serialId, serialItem);
                        }, null, null, 500);

                        calculateOrder(that);
                    }
                    this.refresh();
                },
                onAfterSelect: function (obj) {
                    var serialItem = this.getSelectedItem();
                    // show_details_pictures();
                }
            },
            onClick: {
                'fa-trash-o': function (e, id, trg) {
                    remove(id, this, function (res) {
                    })
                },
                'fa-eye': function (e, id, trg) {
                    showPicture(id)
                },
            },
            pager: 'serialPager',
            url: '/order-details/' + order_selected,
            save: {
                updateFromResponse: true,
                url: 'rest->/order-details',
                on: {
                    onBeforeDataSend: function () {
                        show_progress_icon('order_details');
                    },
                    onAfterSave: function () {
                        hide_progress_icon('order_details');
                    }
                }
            }
        };

        function show_details_pictures() {
            webix.extend($$('datails_imageview'), webix.ProgressBar);
            var item = $$('order_details').getSelectedItem();
            if (item) {
                show_progress_icon('datails_imageview');
                $$("datails_imageview").setValues({pic: `data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=`});
                webix.ajax().get('/pic/' + item.product.barcode).then(function (response) {
                        if (response.text() === 'no_found') {
                            $$('datails_imageview').setValues({pic: '/images/noPic.png'});
                        } else {
                            const imageBase64 = ((((response.json()) || {}).Items || [])[0] || {}).Pict;
                            if (!!imageBase64) {
                                $$('datails_imageview').setValues({pic: `data:image/png;base64, ${imageBase64}`});
                            } else {
                                $$('datails_imageview').setValues({pic: '/images/noPic.png'});
                            }
                        }
                        hide_progress_icon('datails_imageview');
                    },
                );
            } else {
                show_progress_icon('datails_imageview');
                $$('datails_imageview').setValues({pic: '/images/noPic.png'});
                hide_progress_icon('datails_imageview');
            }
        }

        var serialPager = {
            view: 'pager',
            id: 'serialPager',
            template: '{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}',
            size: 10,
            group: 3
        };

        var svOrders = {
            view: 'layout',
            type: 'clean',
            rows: [
                {type: 'header', template: '<span class="order_details"></span>'},
                {
                    cols: [
                        {
                            view: 'button',
                            type: 'icon',
                            icon: 'arrow-left',
                            width: 30,
                            click: function () {
                                $$('pager_orders').select('prev');
                            }
                        },
                        {
                            view: 'list',
                            id: 'order_list',
                            layout: 'x',
                            scroll: false,
                            select: true,
                            type: {
                                width: 150,
                                height: 100
                            },
                            template: function (obj) {
                                let icon_status = '';
                                if (obj.closed == 0)
                                    icon_status = `<span class="icon-status" title="${TEXTS.RETURN.OPEN}"><i class="webix_icon fa-list-alt"></i></span>`;
                                else if (obj.closed == 1)
                                    icon_status = `<span class="icon-status" title="${TEXTS.RETURN.CLOSED}"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-check fa-stack-2x text-success"></i></span>`;
                                else if (obj.closed == 66)
                                    icon_status = `<span class="icon-status" title="${TEXTS.RETURN.MISTAKEN}"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-times fa-stack-2x text-danger"></i></span>`;
                                else if (obj.closed == 70)
                                    icon_status = `<span class="icon-status" title="${TEXTS.RETURN.RETURN_OPEN}"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-reply fa-stack-2x"></i></span>`;
                                else if (obj.closed == 71)
                                    icon_status = `<span class="icon-status" title="${TEXTS.RETURN.RETURN_CLOSED}"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-reply fa-stack-2x text-success"></i></span>`;

                                return '<div item_id="' + obj.id + '" class="order_item text-center">'
                                    + icon_status + '№ <strong>' + obj.id + '</strong><br>' +
                                    '<strong>' + obj.created_at + '</strong><br>' +
                                    'Сумма: <strong>' + obj.amount + '</strong>'
                                    + ((obj.totalDiscountAmount !== 0) ? `<br>Скидка: <strong>${webix.i18n.priceFormat(obj.totalDiscountAmount)}</strong>` : '') + '</div>';
                            },
                            scheme: {
                                $init: function (obj) {
                                    obj.created_at = moment(obj.created_at).format('DD.MM.YY HH:mm');
                                    obj.amount = webix.i18n.priceFormat(obj.amount);
                                    obj.totalDiscountAmount = 0;
                                    if (obj.details) {
                                        obj.totalDiscountAmount = obj.details.reduce((total, detail) => total + parseFloat(detail.discount_amount), 0)
                                    }
                                }
                            },
                            on: {
                                onAfterSelect: function (id) {
                                    order_item_selected = this.getItem(id);
                                    order_selected = order_item_selected.id;

                                    orderDetails(order_item_selected);
                                },
                                onAfterLoad: function () {
                                    this.select(this.getFirstId());
                                }
                            },
                            size: 10,
                            pager: {
                                view: 'pager',
                                id: 'pager_orders',
                                apiOnly: true,
                                size: 10
                            },
                            url: '/orders',
                            save: {
                                updateFromResponse: true,
                                url: 'rest->/orders',
                                on: {
                                    onAfterInsert: function (state, id, data) {
                                        order_item_selected = state;
                                        order_selected = state.id;
                                        $$('order_list').select(order_selected);
                                    }
                                }
                            }
                        },
                        {
                            view: 'button',
                            type: 'icon',
                            icon: 'arrow-right',
                            width: 30,
                            click: function () {
                                $$('pager_orders').select('next');
                            }
                        }
                    ]
                }
            ]
        };

        function displayOrderDetails(order_item) {
            // show_details_pictures()
            var sum_payments = 0;
            // var dtOrderPayments = $$('order-payments');
            // dtOrderPayments .eachRow(
            //     function (row) {
            //     	var item = dtOrderPayments.getItem(row);
            //         if (item.payed ==1)sum_payments+=parseFloat(item.amount);
            // 		//console.log(item.amount)
            //     }
            // )
            const elements = [];
            if (order_item) {
                elements.push(`Заказ ${order_item.id}: ${order_item.created_at}`);
                elements.push(`<strong>${order_item.user.name}</strong>`);

                if (order_item.totalDiscountAmount !== 0) {
                    elements.push(`Скидка: ${webix.i18n.priceFormat(order_item.totalDiscountAmount)}`)
                }
            }
            let order_details = document.querySelector('.order_details');
            order_details.innerHTML = elements.join(' | ');
        }

        function orderDetails(order_item) {
            displayOrderDetails(order_item);
            $$('order_details').clearAll();
            $$('order_details').load('/order-details/' + order_item.id);

            // $$('recepts_view').clearAll()
            // $$('recepts_view').load('/data/receptsOrder/'+ order_item.id);
            // $$('order-payments').clearAll();
            // // setTimeout(() => {
            //  	$$('order-payments').load('/order-payments/' + order_item.id)

            // }, 600);
        }

        function addOrder() {
            $$('order_list').add({id: 0, closed: 0}, 0);
            $$('order_list').refresh();
        }

        function checkItemsStock() {
            show_progress_icon('order_details');
            var dtOrderDetails = $$('order_details');
            var result = true;
            var serial_items = [];
            var order_details = {};
            dtOrderDetails.eachRow(
                function (row) {
                    var item = dtOrderDetails.getItem(row);
                    if (item.serial_id) {
                        serial_items.push(item.serial_id);
                        order_details[item.serial_id] = row;
                    }
                }
            );
            var xhr = webix.ajax().sync().post('/data/check-stocks', {serial_list: serial_items});
            var data = JSON.parse(xhr.responseText);
            if (data.length) {
                data.forEach(function (item, i) {
                    var item = dtOrderDetails.getItem(order_details[item.id]);
                    item.$css = 'background-danger';
                    dtOrderDetails.refresh();
                });
                result = false;
            }

            hide_progress_icon('order_details');
            return result;
        }

        var changePay = function (newVal, oldVal) {
            newVal = parseFloat(newVal) || 0;
            oldVal = parseFloat(oldVal) || 0;

            var newPaidValue = 0;

            var frmPayValues = $$('frmPayOrder').getValues();
            frmPayValues = _.pickBy(frmPayValues, function (value, key) {
                return _.startsWith(key, 'payment_type-');
            });
            _.forEach(frmPayValues, function (value, key) {
                let val = parseFloat(value);
                if (val) newPaidValue += val;
            });

            var paidFor = $$('paidFor');
            var sChange = $$('sChange');
            var btnPaid = $$('btnPaid');
            var certObj = $$('payment_type-cert');
            var cashObj = $$('payment_type-cash');

            var valCert = 0;
            if (certObj) {
                valCert = parseFloat(certObj.getValue()) || 0;
            }
            //$$('certificate_value').setValue(valCert);

            var totalSumma = parseFloat($$('totalSumma').getValue());
            var oldPaidValue = parseFloat(paidFor.getValue());

            if (valCert >= totalSumma) {
                newPaidValue = totalSumma;
                //$$('certificate_value').setValue(newPaidValue);
            }
            if (valCert > 0) {
                var dtCertList = $$('certificate-list');
                var idList = '';
                dtCertList.eachRow(
                    row => {
                        var item = dtCertList.getItem(row);
                        idList += ((idList != '') ? ',' : '') + item.certificate_id;
                    }
                );
                $$('certificate_id').setValue(idList);
            }
            paidFor.setValue(newPaidValue.toFixed(2));

            if (cashObj) {
                webix.html.removeCss(cashObj.$view, 'background-danger');
            }
            if (paidFor) {
                const isFormValid = $$('payment_type-cash').getParentView().validate();
                webix.html.removeCss(paidFor.$view, 'background-danger');
                webix.html.removeCss(paidFor.$view, 'background-success');
                if (newPaidValue < totalSumma || !isFormValid) {
                    webix.html.addCss(paidFor.$view, 'background-danger');
                    btnPaid.disable();
                } else {
                    webix.html.addCss(paidFor.$view, 'background-success');
                    btnPaid.enable();
                }
            }

            let balance = (newPaidValue - totalSumma).toFixed(2);

            sChange.setValue(0);
            if (valCert < totalSumma) {
                sChange.setValue(balance);
                let cash = parseFloat(cashObj.getValue()) || 0;
                if (cash < balance) {
                    webix.html.addCss(cashObj.$view, 'background-danger');
                    webix.message({type: 'error', text: 'Баланс не может быть больше наличных!'});
                    btnPaid.disable();
                }
            }
            $$('change_value').setValue(sChange.getValue());
        };

        function payOrder() {
            if (!order_selected) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Выберите заказ для оплаты!'
                });
                return false;
            }

            if (order_item_selected.closed != 70 && !checkItemsStock()) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Некоторые добавленные продукты уже проданы!'
                });
                return false;
            }

            var sum_items = 0, sum_return = 0;
            var sum_payments = 0;
            var dtOrderDetails = $$('order_details');
            dtOrderDetails.eachRow(
                function (row) {
                    var item = dtOrderDetails.getItem(row);
                    if (item.quantity < 0)
                        sum_return += item.total;
                    else sum_items += item.total;
                }
            );
            // var dtOrderPayments = $$('order-payments');
            // dtOrderPayments .eachRow(
            //     function (row) {
            //     	var item = dtOrderPayments.getItem(row);
            //         if (item.payed ==1)sum_payments+=parseFloat(item.amount);
            // 		//console.log(item.amount)
            //     }
            // )
            sum_return *= -1;
            if (sum_items < sum_return) {
                /*webix.alert({
		        type: 'alert-error',
		        text: 'Сумма новых продукты должна быть равно или больше суммы возврата!'
		    });
			return false;*/
            }

            var winPayOrder = webix.ui({
                view: 'window',
                id: 'winPayOrder',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {
                            margin: 4,
                            rows: [
                                {view: 'label', label: 'Оплата заказа: ' + order_selected, height: -1},
                                // { view: 'label', label: 'Аванс: ' + (sum_payments).toString(), height: -1 },
                            ]
                        },
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winPayOrder").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    width: 300,
                    view: 'form',
                    id: 'frmPayOrder',
                    scroll: false,
                    padding: 15,
                    elements: []
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });

            if (sum_items < 0 && order_item_selected.closed != 70) {
                let pt = payment_types.serialize();
                let pt_id = _.find(pt, {'code': 0}).id;
                $$('frmPayOrder').addView({view: 'text', name: 'payment_type-' + pt_id, value: 0, hidden: true});
                $$('frmPayOrder').addView({
                    view: 'text', label: 'Фиск. код', labelWidth: 110, name: 'client_idno', id: 'client_idno', required: true, css: 'input_progress',
                    suggest: {
                        body: {
                            url: '/data/clients',
                            template: '#name# (#idno#)',
                            yCount: 7,
                            dataFeed: function (filtervalue, filter) {
                                if (filtervalue.length < 3) return;
                                this.load(this.config.url + '/' + encodeURIComponent(filtervalue), this.config.datatype);
                            },
                            on: {
                                onBeforeLoad: function () {
                                    var master = this.getParentView().config.master;
                                    if (master)
                                        $$(master).showProgress();
                                },
                                onAfterLoad: function () {
                                    webix.delay(function () {
                                        var master = this.getParentView().config.master;
                                        if (master)
                                            $$(master).hideProgress();
                                    }, this, null, 2000);
                                }
                            }
                        },
                        on: {
                            onValueSuggest: function (node) {
                                $$('client_id').setValue(node.id);
                                $$('client_idno').setValue(node.idno);
                                $$('client_name').setValue(node.name);
                            },

                        }
                    }
                });

                $$('frmPayOrder').addView({view: 'text', label: 'Имя клиента', labelWidth: 110, name: 'client_name', id: 'client_name', required: true});
                $$('frmPayOrder').addView({view: 'text', name: 'client_id', id: 'client_id', value: 0, hidden: true});
                webix.extend($$('frmPayOrder').elements['client_idno'], webix.ProgressBar);
            } else {
                payment_types.data.each(function (obj) {
                    $$('frmPayOrder').addView({
                        view: 'text',
                        value: ((obj.code == 0) ? order_item_selected.amount : ''),
                        label: obj.name,
                        labelWidth: 135,
                        validate: value => ((parseFloat(value) || 0) >= 0),
                        hidden: (obj.code === certificate_type_code || obj.code === 2),
                        inputAlign: 'right',
                        name: 'payment_type-' + obj.id,
                        id: ((obj.code == certificate_type_code) ? 'payment_type-cert' : ((obj.code == 0) ? 'payment_type-cash' : 'payment_type-' + obj.id)),
                        readonly: ((obj.code == certificate_type_code) ? true : false),
                        popup: 'keypadPopup',
                        on: {
                            onItemClick: function (id, e) {
                                objValue = $$(id);

                                if (globalSettings.settings.multiply_payment && globalSettings.settings.multiply_payment == '0') {
                                    $$('payment_type-cash').setValue('');
                                    payment_types.data.each(function (obj) {
                                        if ($$('payment_type-' + obj.id)) {
                                            $$('payment_type-' + obj.id).setValue('');
                                        }
                                    });
                                }

                                if (obj.code == certificate_type_code) {
                                    //objValue.setValue('');

                                    /*if ($$('btnPaid').isEnabled()) {
									webix.message({type: 'error', text: 'Введённой суммы достаточно для оплаты!'});
									$$('keypadPopup').hide();
									return;
								}*/
                                    titleKeyPad = 'Номер ' + objValue.data.label;
                                    keyPadOptions.callback = getCertificate;
                                    popupKeyPad.show();
                                } else {
                                    titleKeyPad = 'Сумма ' + objValue.data.label;
                                    keyPadOptions.callback = null;
                                }
                            },
                            onChange: changePay
                        }
                    });
                    // if (obj.code == certificate_type_code) {
                    //     $$('frmPayOrder').addView({
                    //         view: 'datatable',
                    //         header: false,
                    //         scrollX: false,
                    //         height: 54,
                    //         rowHeight: 18,
                    //         id: 'certificate-list',
                    //         columns: [
                    //             {id: 'certificate_number', fillspace: true},
                    //             {id: 'certificate_amount', width: 80, css: 'text-right'},
                    //             {id: 'trash', width: 35, template: '<span class="webix_icon fa-trash-o"></span>'},
                    //         ],
                    //         onClick: {
                    //             'fa-trash-o': function (e, id, trg) {
                    //                 this.remove(id);
                    //                 this.refreshColumns();
                    //                 calculateCert();
                    //             }
                    //         },
                    //     });
                    //     $$('frmPayOrder').addView({view: 'text', name: 'certificate_id', id: 'certificate_id', hidden: true});
                    // }
                });
            }

            $$('frmPayOrder').addView({view: 'text', name: 'change_value', id: 'change_value', hidden: true});
            $$('frmPayOrder').addView({});
            $$('frmPayOrder').addView({view: 'text', id: 'paidFor', value: '0', label: 'Оплачено:', labelWidth: 155, inputAlign: 'right', readonly: true, css: 'alternative-rows'});
            $$('frmPayOrder').addView({view: 'text', id: 'sChange', value: '0', label: 'Баланс:', labelWidth: 155, inputAlign: 'right', readonly: true, css: 'alternative-rows'});

            $$('frmPayOrder').addView({view: 'text', id: 'totalSumma', value: order_item_selected.amount, hidden: true});
            //$$('frmPayOrder').addView({ view: 'text', name: 'order_id', value: order_item_selected.id, hidden: true });
            $$('frmPayOrder').addView({
                view: 'button', id: 'btnPaid', type: 'form', value: 'Оплачено', disabled: true,
                click: function () {
                    var data = this.getParentView().getValues();
                    closeOrder(data);
                    winPayOrder.close();
                }
            });
            changePay(order_item_selected.amount);

            winPayOrder.show();
            $$('frmPayOrder').focus();
        }

        function removeOrder() {
            webix.confirm({
                text: 'Удалить чек #' + order_item_selected.id + ' от ' + order_item_selected.created_at + ' ?', type: 'confirm-warning', ok: 'Да', cancel: 'Нет', callback: function (result) {
                    if (result) {
                        webix.ajax().put('/orders/' + order_item_selected.id, {closed: 66})
                            .then(function (result) {
                                $$('order_details').clearAll();
                                // $$('serial-details').clearAll();
                                $$('order_list').clearAll();
                                $$('order_list').load('/orders');
                                displayOrderDetails();
                                order_selected = null;
                                order_item_selected = null;
                            })
                            .fail(function (xhr) {
                                var response = JSON.parse(xhr.response);
                                webix.message({type: 'error', text: response.message});
                            });
                    }
                }
            });
            return false;
        }

        function openDiscount() {
            if (!order_selected) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Выберите заказ для оплаты!'
                });
                return false;
            }

            var winSetDiscount = webix.ui({
                view: 'window',
                id: 'winSetDiscount',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {
                            margin: 4,
                            rows: [
                                {view: 'label', label: 'Скидочная карта', height: -1},
                                {view: 'label', label: 'для заказа: ' + order_selected, height: -1},
                            ]
                        },
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winSetDiscount").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    width: 300,
                    view: 'form',
                    id: 'frmSetDiscount',
                    scroll: false,
                    padding: 15,
                    elements: [
                        {
                            view: 'text', type: 'password', id: 'cardNumber', name: 'cardNumber', value: '',
                            click: function (id) {
                                keyboard(id, "Выидите номер")
                            },
                            on: {
                                onEnter: function (ev) {
                                    let value = this.getValue();
                                    this.setValue('');
                                    setDiscount(value);
                                },
                            }
                        },
                        {
                            view: 'button', id: 'btnDiscountApply', type: 'form', value: 'Применить',
                            click: function () {
                                let values = this.getParentView().getValues();
                                setDiscount(values.cardNumber);
                            }
                        },
                        {
                            view: 'button', id: 'btnDiscountReset', type: 'danger', value: 'Сброс',
                            click: function () {
                                order_item_selected.discount_card_id = null;
                                order_item_selected.discount_card = null;
                                var dtOrderDetails = $$('order_details');
                                dtOrderDetails.eachRow(
                                    function (row) {
                                        var item = dtOrderDetails.getItem(row);
                                        item.discount_percent = 0;
                                        item.discount_amount = 0;
                                        dtOrderDetails.updateItem(row, item);
                                    }
                                )
                                calculateOrder(dtOrderDetails);
                                $$('winSetDiscount').close();
                            }
                        },
                        {
                            view: 'button', id: 'btnDiscountAdd', type: 'form', value: 'Добавить',
                            click: function () {
                                addDiscount()
                            }
                        },
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });
            winSetDiscount.show();

            $$('frmSetDiscount').focus();
        }

        function addDiscount() {

            var winAddDiscount = webix.ui({
                view: 'window',
                id: 'winAddDiscount',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, height: 50, align: 'right', click: '$$("winAddDiscount").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    rows: [
                        {
                            cols: [
                                {
                                    view: "text", id: "discount_firstname", width: 200, placeholder: "FirstName", click: function (id) {
                                        keyboard(id, 'Имя')
                                    }
                                },
                                {width: 20},
                                {
                                    view: "text", id: "discount_secondname", width: 200, placeholder: "SecondName", labelAlign: "left", click: function (id) {
                                        keyboard(id, 'Фамилия')
                                    }
                                },
                                {width: 20},
                                {
                                    view: "text", id: "discount_middlename", width: 200, placeholder: "Middlename", click: function (id) {
                                        keyboard(id, 'Отчество')
                                    }
                                }
                            ]
                        },
                        {height: 20,},
                        {
                            cols: [
                                {
                                    view: "text", id: "discount_tel", width: 200, placeholder: "Tel", click: function (id) {
                                        keyboard(id, 'Телефон')
                                    }
                                },
                                {width: 20},
                                {
                                    view: "text", id: "discount_email", width: 200, placeholder: "Email", click: function (id) {
                                        keyboard(id, 'Емэил')
                                    }
                                },
                            ]
                        },
                        {height: 20,},
                        {
                            view: "text", id: "discount_adrress", width: 400, placeholder: "Adrress", click: function (id) {
                                keyboard(id, 'Адресс')
                            }
                        },
                        {height: 20,},
                        {
                            cols: [
                                {label: "Sex:", view: "label", width: 200},
                                {width: 20},
                                {options: ["M", "F"], view: "tabbar", id: "dicount_sex", width: 200, value: "M"},
                            ]
                        },
                        {height: 20,},
                        {
                            cols: [
                                {label: "Language: ", view: "label", width: 200},
                                {width: 20},
                                {options: ["RU", " RO"], view: "tabbar", id: "dicount_language", width: 200, value: " RO"},
                            ]
                        },
                        {height: 20,},
                        {
                            cols: [
                                {
                                    view: "text", id: "dicount_cardnumber", width: 200, placeholder: "Card Number", click: function (id) {
                                        keyboard(id, 'Номер карточки')
                                    }
                                },
                                {width: 20},
                                {
                                    view: "text", id: "dicount_percent", width: 200, placeholder: "CardProcent", click: function (id) {
                                        keyboard(id, 'Процэнт Скидки')
                                    }
                                },
                            ]
                        },
                        {height: 20,},
                        {view: "checkbox", id: "dicount_enabled", label: "Enabled", value: 1},
                        // {view: "text",id:"dicount_cardtype" , width: 200,placeholder: "CardType",click:function(id){keyboard(id)}},
                        {height: 20,},
                        {
                            cols: [{width: 200},
                                {
                                    type: "form", view: "button", width: 200, icon: "wxi-search", label: "Добавить", height: 50, click: function () {
                                        show_progress_icon('winAddDiscount');


                                        var dataDiscount = {};
                                        dataDiscount.FirstName = $$('discount_firstname').getValue();
                                        dataDiscount.SecondName = $$('discount_secondname').getValue();
                                        dataDiscount.Middlename = $$('discount_middlename').getValue();
                                        dataDiscount.Tel = $$('discount_tel').getValue();
                                        dataDiscount.Adress = $$('discount_adrress').getValue();
                                        dataDiscount.Email = $$('discount_email').getValue();
                                        dataDiscount.Sex = $$('dicount_sex').getValue();
                                        dataDiscount.Lang = $$('dicount_language').getValue();
                                        dataDiscount.CardNr = $$('dicount_cardnumber').getValue();
                                        dataDiscount.CardProcent = $$('dicount_percent').getValue();
                                        dataDiscount.CardType = '1';
                                        dataDiscount.Enabled = $$('dicount_enabled').getValue();
                                        webix.ajax().post('/admin/discount-cards/', {dataDiscount}).then(function (result) {
                                            hide_progress_icon('winAddDiscount')
                                            webix.confirm({
                                                text: 'Синхранизировать данные с бэком', ok: 'Да', cancel: 'Нет', type: 'confirm-warning', callback: function (result) {
                                                    if (result) {
                                                        syncDicount();
                                                    }
                                                }
                                            });
                                        })
                                    }
                                },
                            ]
                        }

                    ],
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });
            winAddDiscount.show();
            webix.extend($$('winAddDiscount'), webix.ProgressBar);
        }

        function productBarcode() {
            var winProductBarcode = webix.ui({
                view: 'window',
                id: 'winProductBarcode',
                css: 'keypad',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {},
                        {width: 30,},
                        {view: 'button', type: 'icon', icon: 'times', width: 60, height: 50, align: 'right', click: '$$("winProductBarcode").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    rows: [
                        {view: "text", id: "keyinput2", width: 500, height: 100,},
                        {
                            view: "button", label: "submit", hotkey: "enter", height: 1, click: function () {
                                var text = $$('keyinput2').getValue()
                                if (text.length == 12 || text.length == 13 || text.length == 0) {
                                    $$('product_code').setValue($$('keyinput2').getValue())
                                    winProductBarcode.close();
                                } else {
                                    webix.message({type: 'error', text: 'Не Валидный Баркод'});
                                }
                            }
                        },
                    ]
                }
            });
            winProductBarcode.show();
            $$("keyinput2").focus();
        }

        function keyboard(id, param) {
            var letter = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m",]
            var text = $$(id.toString()).getValue();

            var winKeyBoard = webix.ui({
                view: 'window',
                id: 'winKeyBoard',
                autowidth: true,
                css: 'keypad',
                head: {
                    view: 'toolbar',
                    cols: [
                        {label: param, view: "label", width: 500},
                        {},
                        {
                            view: 'button', css: "keypadbutton", type: 'icon', icon: 'check', width: 60, height: 50, align: 'right', css: 'text-center', click: function () {
                                $$(id.toString()).setValue($$('keyinput').getValue())
                                winKeyBoard.close();
                            }
                        },
                        {width: 30,},
                        {view: 'button', type: 'icon', icon: 'times', width: 60, height: 50, align: 'right', click: '$$("winKeyBoard").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    rows: [
                        {
                            cols: [
                                {
                                    view: "button", id: 'l_shift', css: "keypadbutton", value: 'Shift', width: 100, height: 100,
                                    click: function () {
                                        if (letter[0] == "q") {
                                            try {
                                                webix.html.removeCss($$("l_shift").$view, "shift1");
                                            } catch (err) {
                                            }
                                            $$("l_shift").define("css", "shift2");
                                            letter = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M",]
                                            $$("l_q").define("value", letter[0]);
                                            $$("l_q").refresh();
                                            $$("l_w").define("value", letter[1]);
                                            $$("l_w").refresh();
                                            $$("l_e").define("value", letter[2]);
                                            $$("l_e").refresh();
                                            $$("l_r").define("value", letter[3]);
                                            $$("l_r").refresh();
                                            $$("l_t").define("value", letter[4]);
                                            $$("l_t").refresh();
                                            $$("l_y").define("value", letter[5]);
                                            $$("l_y").refresh();
                                            $$("l_u").define("value", letter[6]);
                                            $$("l_u").refresh();
                                            $$("l_i").define("value", letter[7]);
                                            $$("l_i").refresh();
                                            $$("l_o").define("value", letter[8]);
                                            $$("l_o").refresh();
                                            $$("l_p").define("value", letter[9]);
                                            $$("l_p").refresh();
                                            $$("l_a").define("value", letter[10]);
                                            $$("l_a").refresh();
                                            $$("l_s").define("value", letter[11]);
                                            $$("l_s").refresh();
                                            $$("l_d").define("value", letter[12]);
                                            $$("l_d").refresh();
                                            $$("l_f").define("value", letter[13]);
                                            $$("l_f").refresh();
                                            $$("l_g").define("value", letter[14]);
                                            $$("l_g").refresh();
                                            $$("l_h").define("value", letter[15]);
                                            $$("l_h").refresh();
                                            $$("l_j").define("value", letter[16]);
                                            $$("l_j").refresh();
                                            $$("l_k").define("value", letter[17]);
                                            $$("l_k").refresh();
                                            $$("l_l").define("value", letter[18]);
                                            $$("l_l").refresh();
                                            $$("l_z").define("value", letter[19]);
                                            $$("l_z").refresh();
                                            $$("l_x").define("value", letter[20]);
                                            $$("l_x").refresh();
                                            $$("l_c").define("value", letter[21]);
                                            $$("l_c").refresh();
                                            $$("l_v").define("value", letter[22]);
                                            $$("l_v").refresh();
                                            $$("l_b").define("value", letter[23]);
                                            $$("l_b").refresh();
                                            $$("l_n").define("value", letter[24]);
                                            $$("l_n").refresh();
                                            $$("l_m").define("value", letter[25]);
                                            $$("l_m").refresh();
                                        } else {
                                            try {
                                                webix.html.removeCss($$("l_shift").$view, "shift2");
                                            } catch (err) {
                                            }
                                            $$("l_shift").define("css", "shift1");
                                            letter = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m",]
                                            $$("l_q").define("value", letter[0]);
                                            $$("l_q").refresh();
                                            $$("l_w").define("value", letter[1]);
                                            $$("l_w").refresh();
                                            $$("l_e").define("value", letter[2]);
                                            $$("l_e").refresh();
                                            $$("l_r").define("value", letter[3]);
                                            $$("l_r").refresh();
                                            $$("l_t").define("value", letter[4]);
                                            $$("l_t").refresh();
                                            $$("l_y").define("value", letter[5]);
                                            $$("l_y").refresh();
                                            $$("l_u").define("value", letter[6]);
                                            $$("l_u").refresh();
                                            $$("l_i").define("value", letter[7]);
                                            $$("l_i").refresh();
                                            $$("l_o").define("value", letter[8]);
                                            $$("l_o").refresh();
                                            $$("l_p").define("value", letter[9]);
                                            $$("l_p").refresh();
                                            $$("l_a").define("value", letter[10]);
                                            $$("l_a").refresh();
                                            $$("l_s").define("value", letter[11]);
                                            $$("l_s").refresh();
                                            $$("l_d").define("value", letter[12]);
                                            $$("l_d").refresh();
                                            $$("l_f").define("value", letter[13]);
                                            $$("l_f").refresh();
                                            $$("l_g").define("value", letter[14]);
                                            $$("l_g").refresh();
                                            $$("l_h").define("value", letter[15]);
                                            $$("l_h").refresh();
                                            $$("l_j").define("value", letter[16]);
                                            $$("l_j").refresh();
                                            $$("l_k").define("value", letter[17]);
                                            $$("l_k").refresh();
                                            $$("l_l").define("value", letter[18]);
                                            $$("l_l").refresh();
                                            $$("l_z").define("value", letter[19]);
                                            $$("l_z").refresh();
                                            $$("l_x").define("value", letter[20]);
                                            $$("l_x").refresh();
                                            $$("l_c").define("value", letter[21]);
                                            $$("l_c").refresh();
                                            $$("l_v").define("value", letter[22]);
                                            $$("l_v").refresh();
                                            $$("l_b").define("value", letter[23]);
                                            $$("l_b").refresh();
                                            $$("l_n").define("value", letter[24]);
                                            $$("l_n").refresh();
                                            $$("l_m").define("value", letter[25]);
                                            $$("l_m").refresh();
                                        }

                                    }

                                },
                                {view: "text", id: "keyinput", width: 793, height: 100,},
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "C", click: function () {
                                        $$('keyinput').setValue(" ")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "1", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "1")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "2", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "2")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "3", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "3")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "4", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "4")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "5", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "5")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "6", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "6")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "7", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "7")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "8", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "8")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "9", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "9")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "0", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "0")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "!", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "!")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "@", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "@")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "#", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "#")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "%", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "%")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "&", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "&")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "(", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "(")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: ")", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + ")")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "-", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "-")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "+", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "+")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: ".", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + ".")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", id: 'l_q', css: "keypadbutton", width: 100, height: 100, value: letter[0], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[0])
                                    }
                                },
                                {
                                    view: "button", id: 'l_w', css: "keypadbutton", width: 100, height: 100, value: letter[1], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[1])
                                    }
                                },
                                {
                                    view: "button", id: 'l_e', css: "keypadbutton", width: 100, height: 100, value: letter[2], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[2])
                                    }
                                },
                                {
                                    view: "button", id: 'l_r', css: "keypadbutton", width: 100, height: 100, value: letter[3], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[3])
                                    }
                                },
                                {
                                    view: "button", id: 'l_t', css: "keypadbutton", width: 100, height: 100, value: letter[4], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[4])
                                    }
                                },
                                {
                                    view: "button", id: 'l_y', css: "keypadbutton", width: 100, height: 100, value: letter[5], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[5])
                                    }
                                },
                                {
                                    view: "button", id: 'l_u', css: "keypadbutton", width: 100, height: 100, value: letter[6], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[6])
                                    }
                                },
                                {
                                    view: "button", id: 'l_i', css: "keypadbutton", width: 100, height: 100, value: letter[7], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[7])
                                    }
                                },
                                {
                                    view: "button", id: 'l_o', css: "keypadbutton", width: 100, height: 100, value: letter[8], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[8])
                                    }
                                },
                                {
                                    view: "button", id: 'l_p', css: "keypadbutton", width: 100, height: 100, value: letter[9], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[9])
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", id: 'l_a', css: "keypadbutton", width: 100, height: 100, value: letter[10], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[10])
                                    }
                                },
                                {
                                    view: "button", id: 'l_s', css: "keypadbutton", width: 100, height: 100, value: letter[11], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[11])
                                    }
                                },
                                {
                                    view: "button", id: 'l_d', css: "keypadbutton", width: 100, height: 100, value: letter[12], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[12])
                                    }
                                },
                                {
                                    view: "button", id: 'l_f', css: "keypadbutton", width: 100, height: 100, value: letter[13], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[13])
                                    }
                                },
                                {
                                    view: "button", id: 'l_g', css: "keypadbutton", width: 100, height: 100, value: letter[14], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[14])
                                    }
                                },
                                {
                                    view: "button", id: 'l_h', css: "keypadbutton", width: 100, height: 100, value: letter[15], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[15])
                                    }
                                },
                                {
                                    view: "button", id: 'l_j', css: "keypadbutton", width: 100, height: 100, value: letter[16], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[16])
                                    }
                                },
                                {
                                    view: "button", id: 'l_k', css: "keypadbutton", width: 100, height: 100, value: letter[17], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[17])
                                    }
                                },
                                {
                                    view: "button", id: 'l_l', css: "keypadbutton", width: 100, height: 100, value: letter[18], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[18])
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: ":", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + ":")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", id: 'l_z', css: "keypadbutton", width: 100, height: 100, value: letter[19], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[19])
                                    }
                                },
                                {
                                    view: "button", id: 'l_x', css: "keypadbutton", width: 100, height: 100, value: letter[20], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[20])
                                    }
                                },
                                {
                                    view: "button", id: 'l_c', css: "keypadbutton", width: 100, height: 100, value: letter[21], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[21])
                                    }
                                },
                                {
                                    view: "button", id: 'l_v', css: "keypadbutton", width: 100, height: 100, value: letter[22], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[22])
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 200, height: 100, value: " ", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + " ")
                                    }
                                },
                                {
                                    view: "button", id: 'l_b', css: "keypadbutton", width: 100, height: 100, value: letter[23], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[23])
                                    }
                                },
                                {
                                    view: "button", id: 'l_n', css: "keypadbutton", width: 100, height: 100, value: letter[24], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[24])
                                    }
                                },
                                {
                                    view: "button", id: 'l_m', css: "keypadbutton", width: 100, height: 100, value: letter[25], click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + letter[25])
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, value: "?", click: function () {
                                        $$('keyinput').setValue($$('keyinput').getValue() + "?")
                                    }
                                },

                            ]
                        },

                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });
            winKeyBoard.show();
            $$('keyinput').setValue(text);
        }

        function keyboard2(id, param) {

            var text = $$(id.toString()).getValue();
            var winKeyBoard2 = webix.ui({
                view: 'window',
                id: 'winKeyBoard2',
                css: 'keypad',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {view: 'label', label: param},

                        {
                            view: 'button', type: 'icon', icon: 'check', width: 50, align: 'right', css: 'text-center', click: function () {
                                $$(id.toString()).setValue($$('keyinput2').getValue())
                                winKeyBoard2.close();
                            }
                        },
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winKeyBoard2").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    rows: [
                        {view: "text", id: "keyinput2", width: 300, height: 100,},
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "7", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "7")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "8", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "8")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "9", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "9")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "4", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "4")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "5", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "5")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "6", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "6")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "1", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "1")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "2", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "2")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: "3", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "3")
                                    }
                                },
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "button", css: "keypadbutton", width: 200, height: 100, label: "0", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + "0")
                                    }
                                },
                                {
                                    view: "button", css: "keypadbutton", width: 100, height: 100, label: ".", click: function () {
                                        $$('keyinput2').setValue($$('keyinput2').getValue() + ".")
                                    }
                                },
                            ]
                        },
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });
            winKeyBoard2.show();
            $$('keyinput2').setValue(text);
        }

        function setDiscount(value) {
            $$('winSetDiscount').hide();
            $$('winSetDiscount').close();
            if (value != '') {
                webix.ajax().post('/data/discount-card', {number: value})
                    .then(function (result) {
                        let resData = result.json()[0];
                        webix.message({type: 'success', text: 'Скидочная карта: ' + resData.full_name + ' (' + resData.percent + '%)'});
                        order_item_selected.discount_card_id = resData.id;
                        order_item_selected.discount_card = resData;
                        var dtOrderDetails = $$('order_details');
                        dtOrderDetails.eachRow(
                            function (row) {
                                var item = dtOrderDetails.getItem(row);
                                if ((item.product && item.product.allow_discount) || (item.serial && item.serial.product.allow_discount)) {
                                    item.discount_percent = resData.percent;
                                    item.discount_amount = (item.sum * resData.percent / 100).toFixed(2);
                                    //item.total = (item.sum - item.discount_amount).toFixed(2);
                                }
                            }
                        )
                        calculateOrder(dtOrderDetails);
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });

            }
        }

        function closeOrder(data) {
            webix.ajax().post('/orders/' + order_item_selected.id, data)
                .then(function (response) {
                    var result = response.json();
                    if (result.status == 'ok') {

                        var dtOrderDetails = $$('order_details');
                        dtOrderDetails.eachRow(
                            function (row) {
                                var item = dtOrderDetails.getItem(row);
                                if (item.serial_id) {
                                    _.remove(serials_data[item.serial.product_id], function (el) {
                                        return el.id == item.serial_id;
                                    })
                                }
                            }
                        )

                        $$('order_details').clearAll();
                        //	$$('order-payments').clearAll();
                        $$('order_list').clearAll();
                        $$('order_list').load('/orders');
                        displayOrderDetails();
                        order_selected = null;
                        order_item_selected = null;
                    } else if (result.message) {
                        webix.message({type: 'error', text: result.message});
                    }
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                });
        }

        function calculateOrder(obj) {
            obj.eachRow(
                function (row) {
                    var item = obj.getItem(row);
                    item.sum = (item.price * item.quantity).toFixed(2);
                    item.total = (item.sum - item.discount_amount);
                    obj.updateItem(row, item);
                }
            )
            obj.refreshColumns();
            webix.delay(function () {
                var idItem = $$('order_list').getSelectedId();
                var orderItem = $$('order_list').getSelectedItem();
                try {
                    orderItem.amount = obj.getColumnConfig('total').footer[0].value;
                    $$('order_list').updateItem(idItem, orderItem);
                } catch (err) {
                }
                displayOrderDetails(order_item_selected);

            }, null, null, 300);
        }

        // function serialDetails(item) {
        // 	var serialDtl = $$('serial-details');
        // 	var details_data = [];
        // 	serialDtl.clearAll();

        // 	if (item.serial) {
        // 		var details = item.serial.details;
        // 		var metals = details.metals;
        // 		var stones = details.stones;

        // 		details_data.push({name: 'Кол.', value: item.serial.stock});
        // 		details_data.push({name: 'Вес', value: details.weight});
        // 		details_data.push({name: 'Размер', value: details.size});
        // 		details_data.push({name: 'Цена за грамм', value: details.price_gramm});
        // 		if (details.color != '')
        // 			details_data.push({name: 'Цвет', value: details.color});

        // 		_.forEach(metals, function(metal, key) {
        // 			let data = {name: metal.name, value: metal.probe};
        // 			details_data.push(data);
        // 		});
        // 		_.forEach(stones, function(stone, key) {
        // 			let value = stone.weight + ((stone.clarity != '') ? ' : ' + stone.clarity : '');
        // 			let data = {name: stone.name, value: value};
        // 			details_data.push(data);
        // 		});
        // 	}
        // 	serialDtl.parse(details_data);
        // }

        image_view = {
            view: "template",
            width: 200,
            height: 200,
            id: 'datails_imageview',
            template: "<img style='object-fit: contain; width: 100%; height: 100%;' src='#pic#' width='200' />",
            on: {
                onShow: function () {
                    windowOpened = true;
                },
                onDestruct: function () {
                    windowOpened = false;
                }
            }

        };

        webix.ui({
            id: 'mainview',
            rows: [
                toolbar_top,
                {
                    view: 'scrollview',
                    body: {
                        type: 'wide',
                        rows: [dtSerials, serialPager]
                        // rows: [dtSerials, serialPager,image_view ]//dtlSerial, serialPager2
                    }
                },
                svOrders,
                toolbar_bottom
            ]
        });

        webix.extend($$('order_details'), webix.ProgressBar);
        webix.extend($$('mainview'), webix.ProgressBar);
        // webix.extend($$('order-payments'), webix.ProgressBar);

        // getUnsyncedOrders();
        setInterval(getUnsyncedOrders(), 60 * 3000);

        function selectByBarcode() {
            if (is_process)
                return;

            if (!order_selected) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Выберите или создайте новый заказ для продукта!'
                });
                return false;
            }

            is_process = true;

            var dtOrderDetails = $$('order_details');
            var exists = false;
            var num_records = 0;
            dtOrderDetails.eachRow(
                function (row) {
                    var item = dtOrderDetails.getItem(row);
                    // if ((serial_selected && item.serial_id == serial_selected.id) || (product_selected && item.product_id == product_selected.id)) {
                    // 	webix.message({type: 'error', text: 'Такой продукт уже добавлен!'});
                    // 	exists = true;
                    // 	return;
                    // }
                    num_records++;
                }
            )
            if (exists) {
                is_process = false;
                return;
            }

            if (num_records > 0 && order_item_selected.closed == 70) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Этот заказ для возврата!<br>Выберите или создайте новый заказ для продукта!'
                });
                return false;
            }

            var data;

            if (product_selected.check_serial && serial_selected) {
                var qty = ((serial_selected.quantity) ? serial_selected.quantity : 1);
                var discount_percent = ((serial_selected.discount_percent) ? serial_selected.discount_percent : 0);
                var discount_amount = ((serial_selected.discount_amount) ? serial_selected.discount_amount : 0);
                data = {
                    order_id: order_selected,
                    serial_id: serial_selected.id,
                    price: serial_selected.price,
                    quantity: qty,
                    sum: serial_selected.price * qty,
                    discount_percent: discount_percent,
                    discount_amount: discount_amount,
                    total: serial_selected.price * qty,
                    serial: serial_selected
                };
                if (serial_selected.updated_at)
                    data.updated_at = serial_selected.updated_at;
            } else if (product_selected) {
                var qty = ((product_selected.quantity) ? product_selected.quantity : 1);
                var discount_percent = ((product_selected.discount_percent) ? product_selected.discount_percent : 0);
                var discount_amount = ((product_selected.discount_amount) ? product_selected.discount_amount : 0);
                data = {
                    barcode: product_selected.fullbarcode,
                    modelcode: product_selected.modelcode,
                    order_id: order_selected,
                    product_id: product_selected.id,
                    price: product_selected.price,
                    quantity: qty, sum: product_selected.price * qty,
                    discount_percent: discount_percent,
                    discount_amount: discount_amount,
                    total: product_selected.price * qty,
                    product: product_selected,
                    size: product_selected.size,
                    color: product_selected.color,
                    color_uuid: product_selected.color_uuid,
                    size_uuid: product_selected.size_uuid
                };
                if (product_selected.updated_at)
                    data.updated_at = product_selected.updated_at;
            }

            if ((order_item_selected && order_item_selected.discount_card) && ((data.product && data.product.allow_discount) || (data.serial && data.serial.product.allow_discount))) {
                data.discount_percent = order_item_selected.discount_card.percent;
                data.discount_amount = Math.round(data.price * data.discount_percent / 100).toFixed(2);
                data.total = data.sum - data.discount_amount;
            }

            if (data) {
                dtOrderDetails.add(data, 0);
                dtOrderDetails.refreshColumns();
                serial_selected = null;
                product_selected = null;
                dtOrderDetails.getPager().select('last');
                dtOrderDetails.select(dtOrderDetails.getLastId());
            }
            is_process = false;
            if ($$('winProducts')) $$('winProducts').close();

        }

        function getByBarcode2(barcode) {
            if (barcode == '')
                return false;

            if (!order_selected) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Выберите заказ для продукта!'
                });
                return false;
            }
            webix.ajax().post('/data/getbybarcode', {barcode: barcode})
                .then(function (result) {
                    var resData = result.json();
                    console.log(resData)
                    if (resData[0].product) {
                        product_selected = resData[0].product;
                        serial_selected = resData[0];
                    } else {
                        product_selected = resData[0];
                    }
                    selectByBarcode();

                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                    objValue.setValue('');
                });
        }

        function getByBarcode(barcode) {
            if (barcode == '')
                return false;

            if (!order_selected) {
                webix.alert({
                    type: 'alert-error',
                    text: 'Выберите заказ для продукта!'
                });
                return false;
            }

            webix.ajax().post('/data/serials', {barcode: barcode})
                .then(function (result) {
                    var resData = result.json();
                    if (resData[0].product) {
                        product_selected = resData[0].product;
                        serial_selected = resData[0];
                    } else {
                        product_selected = resData[0];
                    }

                    selectSerial();
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                    objValue.setValue('');
                });
        }

        function getCertificate(number) {
            if (number == '') {
                calculateCert();
                return false;
            }
            objValue.setValue('');
            webix.ajax().post('/data/certificate', {number: number})
                .then(function (result) {
                    let resData = result.json()[0];

                    var dtCertList = $$('certificate-list');
                    var exists = false;
                    dtCertList.eachRow(
                        function (row) {
                            var item = dtCertList.getItem(row);
                            if (item.certificate_id == resData.id) {
                                webix.message({type: 'error', text: 'Такой сертификат уже добавлен!'});
                                exists = true;
                                return;
                            }
                        }
                    )
                    if (!exists) {
                        $$('certificate-list').add({certificate_id: resData.id, certificate_number: resData.number, certificate_amount: resData.amount});
                        $$('certificate-list').refresh();
                    }

                    calculateCert();
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                    calculateCert();
                });
        }

        function calculateCert() {
            var dtCertList = $$('certificate-list');
            var total = 0;
            dtCertList.eachRow(
                function (row) {
                    var item = dtCertList.getItem(row);
                    total += parseFloat(item.certificate_amount);
                }
            )
            objValue.setValue(total);
        }

        // Window of Products
        var products_data = [];
        var serials_data = [];
        var product_selected, serial_selected;

        var product_type = {
            folder: function (obj) {
                if (obj.$count)
                    return '<span class="webix_tree_folder_open"></span>';
                if (obj.check_serial)
                    return '<span class="webix_icon fa-file"></span>';
                else
                    return '<span class="webix_icon fa-file-text-o"></span>';
            }
        }

        function winProducts() {
            webix.ui({
                view: 'window',
                id: 'winProducts',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Продукты', width: 200},

                        {
                            view: 'search', align: 'left', placeholder: 'Баркод ', width: 200, id: 'q_search',
                            on: {
                                onSearchIconClick: function () {
                                    let value = this.getValue();
                                    search_product(value);
                                    this.setValue('');
                                },
                                onEnter: function (ev) {
                                    let value = this.getValue();
                                    search_product(value);
                                    this.setValue('');
                                }
                            }
                        },
                        {},
                        //{ view: 'button', type: 'icon', icon: 'check', width: 50, align: 'right', css: 'text-center', click: selectSerial },
                        {
                            view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', css: 'text-center', click: function () {
                                $$('setting_logo').disable();
                                $$('delete_picture').disable();
                                $$("winProducts").close();
                            }
                        }
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    cols: [
                        {
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: 'button', type: 'icon', icon: 'arrow-up', width: 50, height: 50, css: 'text-center', click: function () {
                                                $$('pager_products').select('prev');
                                            }
                                        },
                                        {
                                            view: 'pager',
                                            id: 'pager_products',
                                            template: '<div class="text-center">{common.page()} из #limit#</div>',
                                            apiOnly: true,
                                            size: 11,
                                            animate: {direction: 'top'}
                                        },
                                        {
                                            view: 'button', type: 'icon', icon: 'arrow-down', width: 50, height: 50, css: 'text-center', click: function () {
                                                $$('pager_products').select('next');
                                            }
                                        }
                                    ]
                                },
                                {
                                    view: 'treetable',
                                    id: 'product_list',
                                    type: product_type,
                                    width: 550,
                                    header: false,
                                    select: true,
                                    columns: [
                                        {id: 'name', template: '{common.treetable()} #name#', fillspace: true},
                                        // { id: 'vendorcode', template: '#vendorcode#', width: 100 },
                                        {id: 'uuid', template: '#uuid#', width: 200, hidden: true},
                                        {id: 'barcode', template: '#barcode#', width: 200},
                                    ],
                                    on: {
                                        onAfterSelect: function (id) {
                                            let item = this.getSelectedItem();
                                            if (item) {
                                                let product = _.find(products_data, {'id': item.id});
                                                product_selected = product;
                                                // if (item.check_serial)
                                                // 	getSerials(item.id);
                                                // else
                                                getProductDetails(item);
                                            }
                                        },
                                        onAfterLoad: function () {
                                            if (products_data.length == 0) {
                                                products_data = this.data.pull;
                                            }
                                            //console.log(this.data.pull);
                                            try {
                                                hide_progress_icon('product_list');
                                            } catch (e) {
                                            }
                                        }
                                    },
                                    yCount: 11,
                                    pager: 'pager_products',
                                    url: '/data/products'
                                }
                            ]
                        },
                        // {
                        // 	rows: [
                        // 		{
                        // 			cols: [
                        // 				{ view: 'button', type: 'icon', icon: 'arrow-up', width: 50, height: 50, css: 'text-center', click: function() { $$('pager_serials').select('prev'); } },
                        // 				{
                        // 					view: 'pager',
                        // 					id: 'pager_serials',
                        // 					template: '<div class="text-center">{common.page()} из #limit#</div>',
                        // 					apiOnly: true,
                        // 					size: 10,
                        // 					animate: { direction: 'top' }
                        // 				},
                        // 				{ view: 'button', type: 'icon', icon: 'arrow-down', width: 50, height: 50, css: 'text-center', click: function() { $$('pager_serials').select('next'); } }
                        // 			]
                        // 		},
                        // 		{
                        // 			view: 'list',
                        // 			id: 'serial_list',
                        // 			width: 220,
                        // 			select: true,
                        // 			data: [],
                        // 			template: '<div class="badge">#price# </div> #barcode#',
                        // 			yCount: 10,
                        // 			pager: 'pager_serials',
                        // 			on: {
                        // 				onAfterLoad: function() {
                        // 					try { hide_progress_icon('serial_list'); } catch(e) {}
                        // 				},
                        // 	        	onAfterSelect: function(id) {
                        // 	        		let serial_id = this.getItem(id).id;
                        // 	        		let product_id = this.getItem(id).product_id;
                        // 	        		getSerialDetails(serial_id, product_id);
                        // 	        	},
                        // 			}
                        // 		}
                        // 	]
                        // },
                        {
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: 'button', type: 'icon', icon: 'arrow-up', width: 50, height: 50, css: 'text-center', click: function () {
                                                $$('pager_details').select('prev');
                                            }
                                        },
                                        {
                                            view: 'pager',
                                            id: 'pager_details',
                                            template: '<div class="text-center">{common.page()} из #limit#</div>',
                                            apiOnly: true,
                                            size: 10,
                                            animate: {direction: 'top'}
                                        },
                                        {
                                            view: 'button', type: 'icon', icon: 'arrow-down', width: 50, height: 50, css: 'text-center', click: function () {
                                                $$('pager_details').select('next');
                                            }
                                        }
                                    ]
                                },
                                {
                                    view: 'treetable',
                                    id: 'details_list',
                                    width: 400,

                                    header: false,
                                    columns: [
                                        {id: 'name', template: '{common.treetable()} #name#', width: 150},
                                        {id: 'value', adjust: true}
                                    ],
                                    data: [],
                                    yCount: 3,
                                    pager: 'pager_details'
                                },
                                {
                                    view: "template",
                                    width: 400,
                                    //css: "picture_selector",
                                    height: 250,
                                    id: 'imageview2',
                                    template: "<img style='object-fit: contain; width: 100%; height: 100%;' src='#pic#'  />"
                                },
                                {
                                    view: 'uploader',
                                    id: 'setting_logo', inputName: 'logo',
                                    autosend: false,
                                    multiple: false,
                                    disabled: true,
                                    //hidden:true,
                                    //	type: 'imageTop',
                                    label: 'Добавить картинку',
                                    datatype: 'json',
                                    hidden: true,
                                    height: 40,
                                    width: 400,
                                    image: '',
                                    on: {
                                        onAfterFileAdd: function (item) {
                                            if (/\.(jpe?g|png|gif)$/i.test(item.name)) {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    //console.log(reader.result);
                                                    $$("imageview2").setValues({pic: reader.result});// reader.result;
                                                    console.log($$('product_list').getSelectedItem());
                                                    var item = $$('product_list').getSelectedItem().uuid;
                                                    var data = {
                                                        img: reader.result,
                                                        product_uuid: item
                                                    };

                                                    webix.ajax().post('/addpic', data).then(function (result) {
                                                    });
                                                }
                                                reader.readAsDataURL(item.file);
                                            }


                                        },

                                    }
                                },
                                {
                                    view: 'button',
                                    id: 'delete_picture',
                                    label: 'Удалить Картину',
                                    disabled: true,
                                    width: 400,
                                    height: 40,
                                    hidden: true,
                                    css: 'text-center',
                                    click: function () {
                                        var item = $$('product_list').getSelectedItem().uuid;
                                        console.log(item)
                                        webix.ajax().put('/delpic/' + item).then(function (response) {
                                            $$("imageview2").setValues({pic: '/images/noPic.png'});

                                        })

                                    }
                                },
                                {}

                            ]
                        }
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            }).show();
            webix.extend($$('product_list'), webix.ProgressBar);
            webix.extend($$('imageview2'), webix.ProgressBar);
            //	webix.extend($$('serial_list'), webix.ProgressBar);
            $$("imageview2").setValues({pic: '/images/noPic.png'});
            show_progress_icon('product_list');
        }

        function showPicture(id, obj) {
            webix.ui({
                view: 'window',
                id: 'showPicture',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {},
                        {
                            view: 'button',
                            type: 'icon',
                            icon: 'times',
                            width: 50,
                            align: 'right',
                            css: 'text-center',
                            click: function () {
                                $$('showPicture').close();
                            },
                        },
                    ],
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'template',
                    width: 600,
                    height: 600,
                    id: 'imageview',
                    template: '<img style=\'object-fit: contain; width: 100%; height: 100%;\' src=\'#pic#\' width=\'600\' />',
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                },
            }).show();
            webix.extend($$('imageview'), webix.ProgressBar);

            show_progress_icon('imageview');

            setTimeout(() => {
                var item = $$('order_details').getSelectedItem();

                $$("imageview").setValues({pic: `data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=`});
                webix.ajax().get('/pic/' + item.product.barcode).then(function (response) {
                    if (response.text() === 'no_found') {
                        $$('imageview').setValues({pic: '/images/noPic.png'});
                    } else {
                        const imageBase64 = ((((response.json()) || {}).Items || [])[0] || {}).Pict;
                        if (!!imageBase64) {
                            $$('imageview').setValues({pic: `data:image/png;base64, ${imageBase64}`});
                        } else {
                            $$('imageview').setValues({pic: '/images/noPic.png'});
                        }
                    }
                    hide_progress_icon('imageview');
                });
            }, 500);
        }

        //var menuList=webix.ajax().get('/data/tree/'+)
        function getmenu() {
            var nodes = [];
            var toplevelNodes = [];
            var lookupList = {};
            var flat;
            var r = webix.ajax().get('/data/tree/').then(function (data) {
                flat = data.json()
                for (var i = 0; i < flat.length; i++) {
                    var n = {
                        id: flat[i].id,
                        type: "folder",
                        name: flat[i].name,
                        price: flat[i].price,
                        barcode: flat[i].barcode,
                        data: [],
                        parent_id: ((flat[i].parent_id == 0) ? null : flat[i].parent_id),
                    };
                    lookupList[n.id] = n;
                    nodes.push(n);
                    if (n.parent_id == null) {
                        toplevelNodes.push(n);
                    }
                }
                for (var i = 0; i < nodes.length; i++) {
                    var n = nodes[i];
                    if (!(n.parent_id == null)) {
                        lookupList[n.parent_id].data = lookupList[n.parent_id].data.concat([n]);
                    }
                }
                return toplevelNodes;
            })
            return r
        }

        function winProducts2() {
            webix.ui({
                view: "window",
                id: 'popWindowAddProduct',
                // move: false,
                modal: true,
                width: 900,
                height: 700,
                css: "zindex",
                position: 'center',
                head: {
                    rows: [
                        {
                            view: 'toolbar',
                            height: 50,
                            cols: [
                                {view: 'label', label: 'Продукты', width: 200},

                                {
                                    view: "text", align: 'left', width: 200, placeholder: 'Баркод', id: 'search2',
                                    on: {
                                        onTimedKeyPress: function () {
                                            var value = this.getValue()
                                            $$("meniuList").filter(function (obj) {
                                                return obj.barcode.toLowerCase().indexOf(value) == 0;
                                            })
                                        }

                                    }
                                },
                                {view: 'button', type: 'icon', icon: 'calculator', width: 50, click: barcodeKeypad2},
                                {},
                                {
                                    view: 'button', type: 'icon', icon: 'check', width: 50, align: 'right', css: 'text-center', click: function () {
                                        var item = $$("meniuList").getSelectedItem()
                                        if (!item) {
                                            webix.message({type: 'error', text: 'Продукт небыл выбран!'});
                                        } else {
                                            webix.ajax().get('/data/tree/' + item.id).then(function (data) {
                                                var element = data.json()
                                                if (is_process)
                                                    return;

                                                if (!order_selected) {
                                                    webix.alert({
                                                        type: 'alert-error',
                                                        text: 'Выберите или создайте новый заказ для продукта!'
                                                    });
                                                    return false;
                                                }

                                                is_process = true;

                                                var dtOrderDetails = $$('order_details');
                                                var exists = false;
                                                var num_records = 0;
                                                dtOrderDetails.eachRow(
                                                    function (row) {
                                                        var item = dtOrderDetails.getItem(row);
                                                        if ((element.id && item.product_id == element.id)) {
                                                            webix.message({type: 'error', text: 'Такой продукт уже добавлен!'});
                                                            exists = true;
                                                            return;
                                                        }
                                                        num_records++;
                                                    }
                                                )
                                                if (exists) {
                                                    is_process = false;
                                                    return;
                                                }

                                                if (num_records > 0 && order_item_selected.closed == 70) {
                                                    webix.alert({
                                                        type: 'alert-error',
                                                        text: 'Этот заказ для возврата!<br>Выберите или создайте новый заказ для продукта!'
                                                    });
                                                    return false;
                                                }
                                                if (element) {
                                                    var qty = ((element.quantity) ? element.quantity : 1);
                                                    var discount_percent = ((element.discount_percent) ? element.discount_percent : 0);
                                                    var discount_amount = ((element.discount_amount) ? element.discount_amount : 0);
                                                    data = {
                                                        order_id: order_selected,
                                                        product_id: element.id,
                                                        price: element.price,
                                                        quantity: qty,
                                                        sum: element.price * qty,
                                                        discount_percent: discount_percent,
                                                        discount_amount: discount_amount,
                                                        total: element.price * qty,
                                                        product: element
                                                    };
                                                    if (element.updated_at)
                                                        data.updated_at = element.updated_at;
                                                }

                                                if ((order_item_selected && order_item_selected.discount_card) && ((data.product && data.product.allow_discount) || (data.serial && data.serial.product.allow_discount))) {
                                                    data.discount_percent = order_item_selected.discount_card.percent;
                                                    data.discount_amount = Math.round(data.price * data.discount_percent / 100).toFixed(2);
                                                    data.total = data.sum - data.discount_amount;
                                                }

                                                if (data) {
                                                    dtOrderDetails.add(data, 0);
                                                    dtOrderDetails.refreshColumns();
                                                    dtOrderDetails.getPager().select('last');
                                                    dtOrderDetails.select(dtOrderDetails.getLastId());
                                                }
                                                is_process = false;
                                                if ($$('popWindowAddProduct')) $$('popWindowAddProduct').close();

                                            })
                                        }
                                        // console.log(item.id)
                                    }
                                },
                                {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', css: 'text-center', click: '$$("popWindowAddProduct").hide()'}
                            ]
                        },
                    ]
                },

                body: {
                    rows: [
                        {

                            view: "toolbar", margin: -4, cols: [
                                {
                                    rows: [
                                        {
                                            height: 40, cols: [
                                                {
                                                    view: "icon", width: 80, icon: "home", css: "bigIcon", click: function () {
                                                        if ($$('meniuList').getOpenState().parents.length)
                                                            $('[webix_l_id="' + $$('meniuList').getFirstId() + '"]').click()
                                                    }
                                                },
                                                {
                                                    view: "icon", width: 80, icon: "level-up", css: "bigIcon", click: function () {
                                                        var el = $('.webix_list_item.webix_group_back').last()[0]
                                                        if (el) {
                                                            el.click()
                                                        }
                                                    }
                                                },


                                                {
                                                    view: 'pager',
                                                    id: 'pager_menu',
                                                    template: '<div class="text-center" style="color:white;">{common.page()} из #limit#</div>',
                                                    apiOnly: true,
                                                    size: 30,
                                                    animate: {direction: 'top'}
                                                },
                                                {
                                                    view: 'button', type: 'icon', icon: 'arrow-up', width: 50, height: 50, css: 'text-center', click: function () {
                                                        $$('pager_menu').select('prev');
                                                    }
                                                },
                                                {
                                                    view: 'button', type: 'icon', icon: 'arrow-down', width: 50, height: 50, css: 'text-center', click: function () {
                                                        $$('pager_menu').select('next');
                                                    }
                                                }

                                            ]
                                        },


                                    ]
                                },
                            ]


                        },
                        {
                            view: "scrollview",
                            id: "scrollview",
                            // scroll:"y",
                            // height: 160,
                            // autoheight:true,
                            body:
                                {
                                    view: "grouplist",
                                    id: 'meniuList',
                                    css: "trioViewGroupLIst trioSize1",
                                    // scroll:true,
                                    select: true,
                                    animate: false,
                                    type: {
                                        // templateItem:'<div class="trio_title">#Name#</div><div class="priceMenu"><b>#Price#</b></div> ',
                                        templateItem: '<div class="trio_title">#name#</div><div class="barcode">#barcode#</div><div class="priceMenu"><b>#price#</b></div>',
                                        templateGroup: '<div class="trio_title" style="width:120px;">#name#</div>',
                                        templateBack: '<div class="trio_title" style="width:120px;">#name#</div>',
                                    },
                                    on: {

                                        onItemClick: function (id) {

                                        },
                                        onAfterSelect: function (id) {

                                        }

                                    },

                                    borderless: true,
                                    data: getmenu(),
                                    yCount: 30,
                                    pager: 'pager_menu',
                                },

                        },

                    ]
                }
            }).show();

        }

        function search_product(value) {
            show_progress_icon('product_list');
            var $product_list = $$('product_list');
            //var $serial_list = $$('serial_list');
            var filtered_product_list = [];
            var filtered_serial_list = [];

            if (value != '') {
                webix.ajax().post('/data/serials', {barcode: value})
                    .then(function (result) {
                        var resData = result.json();
                        resData = _.filter(resData, function (obj) {
                            return (obj.product_id && obj.product_id > 0);
                        });
                        _.forEach(resData, function (val) {
                            if (!serials_data[val.product_id])
                                serials_data[val.product_id] = [];
                            serials_data[val.product_id].push(val);
                        });
                        // $serial_list.clearAll();
                        // $serial_list.parse(resData);
                    })

                filtered_product_list = _.filter(products_data, function (obj) {
                    return (obj.barcode == value || obj.vendorcode == value);
                });
            } else {
                filtered_product_list = products_data;
            }
            $product_list.clearAll();
            $product_list.parse(filtered_product_list);
            if ($product_list.count())
                $product_list.select($product_list.getFirstId());

            //console.log(filtered_product_list);

            //try { hide_progress_icon('product_list'); } catch(e) {}

        }

        function getProductDetails(product) {
            //	$$('serial_list').clearAll();
            $$('details_list').clearAll();
            var details_data = [];
            if (product && product.product_type == 1) {
                details_data.push({id: 0, name: 'Цена', value: product.price});
                if (product.barcode) details_data.push({id: 0, name: 'Модель', value: product.barcode});
                // if (product.vendorcode) details_data.push({id: 0, name: 'Артикул', value: product.vendorcode});
            }

            $$('setting_logo').enable();
            $$('delete_picture').enable();
            show_progress_icon('imageview2');
            $$("imageview2").setValues({pic: `data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=`});
            webix.ajax().get('/pic/' + product.barcode).then(function (response) {
                if (response.text() === 'no_found') {
                    $$("imageview2").setValues({pic: '/images/noPic.png'});
                } else {
                    const imageBase64 = ((((response.json()) || {}).Items || [])[0] || {}).Pict;
                    if (!!imageBase64) {
                        $$("imageview2").setValues({pic: `data:image/png;base64, ${imageBase64}`});
                    } else {
                        $$("imageview2").setValues({pic: '/images/noPic.png'});
                    }
                }
                hide_progress_icon('imageview2');
            });

            $$('details_list').parse(details_data);
        }


        // function getSerials(product_id)	{
        // //	$$('serial_list').clearAll();
        // 	$$('details_list').clearAll();
        // 	show_progress_icon('serial_list');
        // 	if (serials_data[product_id]) {
        // 		var data_out = serials_data[product_id];
        // 		if (order_item_selected) {
        // 			//data_out = _.filter(data_out, {section_id: order_item_selected.section.id});
        // 		}
        // 		$$('serial_list').parse(data_out);
        // 	} else {
        // 		webix.ajax().get('/data/serials/' + product_id).then(function (data) {
        //         	serials_data[product_id] = data.json();
        //         	var data_out = serials_data[product_id];
        //     		if (order_item_selected) {
        //     			//data_out = _.filter(data_out, {section_id: order_item_selected.section.id});
        //     		}
        //     		$$('serial_list').parse(data_out);
        //       	})
        // 	}
        // }
        // function getSerialDetails(serial_id, product_id) {
        // 	var product_id = product_id || product_selected.id;
        // 	var serial = _.find(serials_data[product_id], { 'id': serial_id });
        // 	serial_selected = serial;
        // 	if (!product_selected)
        // 		product_selected = _.find(products_data, { id: product_id});
        // 	serial_selected.product = product_selected;
        // 	var details_data = [];
        // 	$$('details_list').clearAll();
        // 	if (serial) {
        // 		var details = serial.details;
        // 		var metals = details.metals;
        // 		var stones = details.stones;
        // 		var metal_data = [];
        // 		var stone_data = [];
        // 		var metal_id = 1;
        // 		var stone_id = 2;
        // 		let dtl_data;
        // 		details_data.push({id: 0, name: 'Секция', value: serial.section.name});
        // 		details_data.push({id: 0, name: 'Артикул', value: serial.vendorcode});
        // 		details_data.push({id: 0, name: 'Кол.', value: serial.stock});
        // 		details_data.push({id: 0, name: 'Вес', value: details.weight});
        // 		details_data.push({id: 0, name: 'Размер', value: details.size});
        // 		details_data.push({id: 0, name: 'Цена за грамм', value: details.price_gramm});
        // 		if (details.color != '')
        // 			details_data.push({id: 0, name: 'Цвет', value: details.color});
        // 		_.forEach(metals, function(metal, key) {
        // 			let data = {id: metal_id + '.' + key, name: metal.name, value: metal.probe};
        // 			metal_data.push(data);
        // 		});
        // 		if (metal_data.length) {
        // 			dtl_data = {id: metal_id, name: 'Метал', open: true, data: metal_data};
        // 			details_data.push(dtl_data);
        // 		}
        // 		_.forEach(stones, function(stone, key) {
        // 			let data = {id: stone_id + '.' + key, name: stone.name, value: stone.weight/* + ' : ' + stone.clarity*/};
        // 			stone_data.push(data);
        // 		});
        // 		if (stone_data.length) {
        // 			dtl_data = {id: stone_id, name: 'Камень', open: true, data: stone_data};
        // 			details_data.push(dtl_data);
        // 		}
        // 	}
        // 	$$('details_list').parse(details_data);
        // }
        // function selectSerial() {
        // 	if (is_process)
        // 		return;
        // 	if (!order_selected) {
        // 	    webix.alert({
        // 	        type: 'alert-error',
        // 	        text: 'Выберите или создайте новый заказ для продукта!'
        // 	    });
        // 		return false;
        // 	}
        // 	is_process = true;
        // 	var dtOrderDetails = $$('order_details');
        // 	var exists = false;
        // 	var num_records = 0;
        // 	dtOrderDetails.eachRow(
        // 	    function (row) {
        // 	    	var item = dtOrderDetails.getItem(row);
        // 	        if ((serial_selected && item.serial_id == serial_selected.id) || (product_selected && item.product_id == product_selected.id)) {
        // 	        	webix.message({type: 'error', text: 'Такой продукт уже добавлен!'});
        // 	        	exists = true;
        // 	        	return;
        // 	        }
        // 	        num_records++;
        // 	    }
        // 	)
        // 	if (exists) {
        // 		is_process = false;
        // 		return;
        // 	}
        // 	if (num_records > 0 && order_item_selected.closed == 70) {
        // 	    webix.alert({
        // 	        type: 'alert-error',
        // 	        text: 'Этот заказ для возврата!<br>Выберите или создайте новый заказ для продукта!'
        // 	    });
        // 		return false;
        // 	}
        // 	var data;
        // 	if (product_selected.product_type == 6) {
        //         webix.ui({
        //             view: 'window',
        //             id: 'winCertificate',
        //             autowidth: true,
        //             position: 'center',
        //             modal: true,
        //             head: 'Сертификат',
        //             body: {
        // 			    view: 'form',
        // 			    id: 'frm_certificate',
        // 			    elements: [
        // 			        {
        // 			        	view: 'text', label: 'Номер сертификата:', name: 'certificate_number', labelWidth: 160, required: true, width: 400,
        // 			        	popup: 'keypadPopup',
        // 						on: {
        // 							onItemClick: function(id, e) {
        // 								objValue = $$(id);
        // 								titleKeyPad = 'Введите номер сертификата';
        // 						        keyPadOptions.callback = null;
        // 							}
        // 						}
        // 			    	},
        // 			    	{ view: 'text', value: '', label: 'На сумму:', labelWidth: 160, inputAlign: 'right', required: true, name: 'certificate_amount', popup: 'keypadPopup',
        // 						on: {
        // 							onItemClick: function(id, e) {
        // 								objValue = $$(id);
        // 								titleKeyPad = 'Введите сумму сертификата';
        // 						        keyPadOptions.callback = null;
        // 							}
        // 						}
        // 			    	},
        // 			    	{ view: 'text', value: '', label: 'Имя клиента:', labelWidth: 160, name: 'certificate_name' },
        // 			    	{ view: 'text', value: '', label: 'Телефон:', labelWidth: 160, name: 'certificate_phone' },
        // 			        { view: 'datepicker', label: 'Действителен до:', name: 'certificate_date', labelWidth: 160, format: webix.Date.dateToStr("%d.%m.%Y"), required: true, stringResult: true },
        // 			        { margin: 5,
        // 				        cols: [
        // 							{ view: 'button', value: 'OK', type: 'form', click: function() {
        // 								if ($$('frm_certificate').validate()) {
        // 						  			let form_data = $$('frm_certificate').getValues();
        // 									webix.ajax().post('/data/certificate', {number: form_data.certificate_number, type: 'all'})
        // 										.then(function (result) {
        // 											let resData = result.json()[0];
        // 											if (resData)
        // 												webix.message({type: 'error', text: 'Сертификат с таким номером уже существует!'});
        // 										})
        // 										.fail(function (xhr) {
        // 											if (xhr.status == 404) {
        // 												data = {order_id: order_selected, product_id: product_selected.id, price: form_data.certificate_amount, quantity: 1, sum: form_data.certificate_amount, discount_percent: 0, discount_amount: 0, total: form_data.certificate_amount, product: product_selected, certificate_number: form_data.certificate_number, certificate_date: form_data.certificate_date + ':00', certificate_name: form_data.certificate_name, certificate_phone: form_data.certificate_phone};
        // 										        dtOrderDetails.add(data, 0);
        // 										        //dtOrderDetails.refresh();
        // 										        dtOrderDetails.refreshColumns();
        // 										        is_process = false;
        // 						                        $$('winCertificate').close();
        // 						                        if ($$('winProducts')) $$('winProducts').close();
        // 											}
        // 										});
        // 			                    }
        // 								else
        // 									webix.message({ type: 'error', text: 'Заполните необходимые поля!' });
        // 							}},
        // 							{ view: 'button', value: 'Отмена', click: function() {
        // 									is_process = false;
        // 			                        this.getTopParentView().close();
        // 							}},
        // 						]
        // 					}
        // 			    ]
        //             },
        // 			on: {
        // 				onShow: function() {
        // 					windowOpened = true;
        // 				},
        // 				onDestruct: function() {
        // 					windowOpened = false;
        // 				}
        // 			}
        //         }).show();
        //         $$('frm_certificate').focus();
        // 	} else {
        // 		if (product_selected.check_serial && serial_selected) {
        // 			var qty = ((serial_selected.quantity) ? serial_selected.quantity : 1);
        // 			var discount_percent = ((serial_selected.discount_percent) ? serial_selected.discount_percent : 0);
        // 			var discount_amount = ((serial_selected.discount_amount) ? serial_selected.discount_amount : 0);
        // 			data = {order_id: order_selected, serial_id: serial_selected.id, price: serial_selected.price, quantity: qty, sum: serial_selected.price * qty, discount_percent: discount_percent, discount_amount: discount_amount, total: serial_selected.price * qty, serial: serial_selected};
        // 			if (serial_selected.updated_at)
        // 				data.updated_at = serial_selected.updated_at;
        // 		} else if (product_selected) {
        // 			var qty = ((product_selected.quantity) ? product_selected.quantity : 1);
        // 			var discount_percent = ((product_selected.discount_percent) ? product_selected.discount_percent : 0);
        // 			var discount_amount = ((product_selected.discount_amount) ? product_selected.discount_amount : 0);
        // 			data = {order_id: order_selected, product_id: product_selected.id, price: product_selected.price, quantity: qty, sum: product_selected.price * qty, discount_percent: discount_percent, discount_amount: discount_amount, total: product_selected.price * qty, product: product_selected};
        // 			if (product_selected.updated_at)
        // 				data.updated_at = product_selected.updated_at;
        // 		}
        // 		if ((order_item_selected && order_item_selected.discount_card) && ((data.product && data.product.allow_discount) || (data.serial && data.serial.product.allow_discount))) {
        // 			data.discount_percent = order_item_selected.discount_card.percent;
        // 			data.discount_amount = Math.round(data.price * data.discount_percent / 100).toFixed(2);
        // 			data.total = data.sum - data.discount_amount;
        // 		}
        //     	if (data) {
        // 	        dtOrderDetails.add(data, 0);
        // 	        dtOrderDetails.refreshColumns();
        // 	        serial_selected = null;
        // 	        product_selected = null;
        // 	        dtOrderDetails.getPager().select('last');
        // 	        dtOrderDetails.select(dtOrderDetails.getLastId());
        //     	}
        //     	is_process = false;
        //         if ($$('winProducts')) $$('winProducts').close();
        // 	}
        // }

        function printFiscal(reason, command) {
            if (reason) {
                webix.ajax().post('/print-fiscal', {reason: reason, command: command})
                    .then(function (result) {
                        //webix.message({type: 'success', text: 'Распечатано!'});
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            }
        }

        function closeShift() {
            webix.confirm({
                text: 'Подтверждаете закрытие смены?',
                ok: 'Да',
                cancel: 'Нет',
                type: 'confirm-warning',
                callback: function (result) {
                    if (result) {
                        if (lastShift_at && moment(lastShift_at).hour() != 0 && moment().isSame(lastShift_at, 'day')) {
                            webix.alert({
                                type: 'alert-error',
                                text: 'Внимание!<br>Текущий день уже закрыт:<br><strong>' + lastShift_at + '</strong>'
                            });
                            return false;
                        }

                        var orders_closed = [];
                        var orderObj = $$('order_list');
                        orderObj.data.each(
                            function (obj) {
                                if (!obj.closed) {
                                    orders_closed.push(obj.id);
                                }
                            }
                        );
                        if (orders_closed.length) {
                            webix.alert({
                                type: 'alert-error',
                                text: 'Внимание!<br>У Вас открытые чеки: <strong>' + orders_closed.join(', ') + '</strong>'
                            });
                            return false;
                        }
                        webix.ajax().post('/close-shift')
                            .then(function (result) {
                                webix.message({type: 'success', text: 'Смена закрыта!'});
                                var dataValues = {
                                    dateFrom: lastShift_at,
                                    dateTo: moment().format('YYYY-MM-DD 23:59'),
                                    report: 2
                                }

                                show_progress_icon('mainview');
                                var print_message = webix.message({type: 'info', text: 'Идёт печать!', expire: -1});
                                webix.ajax().post('/report/print', dataValues)
                                    .then(function (result) {
                                        webix.message.hide(print_message);
                                        hide_progress_icon('mainview');
                                    })
                                    .fail(function (xhr) {
                                        var response = JSON.parse(xhr.response);
                                        webix.message({type: 'error', text: response.message});
                                        hide_progress_icon('mainview');
                                    });

                                if (globalSettings.settings.shift_z_report == '1') {
                                    printFiscal('report', 'Z;');
                                }
                                getSettings();
                            })
                            .fail(function (xhr) {
                                var response = JSON.parse(xhr.response);
                                webix.message({type: 'error', text: response.message});
                            });

                    }
                }
            });
        }

        function winReports() {
            webix.ui({
                view: 'window',
                id: 'winReports',
                autowidth: true,
                // fullscreen: true,
                width: window.screen.width * 0.9,
                height: window.screen.height * 0.9,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Отчёты'},
                        {},

                        // {view: 'button', type: 'icon', icon: 'print', width: 80, label: 'Печать', align: 'right', click: printReport, css: 'background-primary'},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winReports").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    cols: [
                        {
                            rows: [
                                {
                                    view: 'list',
                                    width: 150,
                                    select: true,
                                    id: 'report_list',
                                    data: [{id: 1, title: 'Анализ продаж', selected: true,}, {id: 2, title: 'Сменный отчёт'}, {id: 3, title: 'Product Balance'}, {id: 6, title: 'Клиент'}],
                                    template: '#title#',
                                    on: {
                                        onAfterLoad: function () {
                                            try {
                                                hide_progress_icon('report_view');
                                            } catch (e) {
                                            }
                                        },
                                        onAfterSelect: function (id) {
                                            report_selected = id;
                                            generateReport();
                                        },
                                    }
                                }
                            ]
                        },
                        {
                            rows: [
                                {
                                    view: 'form',
                                    id: 'frmReport',
                                    elements: [
                                        {
                                            rows: [
                                                {
                                                    cols: [
                                                        {
                                                            view: 'datepicker',
                                                            id: 'dateFrom',
                                                            name: 'dateFrom',
                                                            width: 200,
                                                            value: lastShift_at,
                                                            label: 'От:',
                                                            labelWidth: 40,
                                                            timepicker: true,
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    generateReport();
                                                                }
                                                            }
                                                        },
                                                        {
                                                            view: 'datepicker',
                                                            id: 'dateTo',
                                                            name: 'dateTo',
                                                            width: 200,
                                                            value: moment().format('YYYY-MM-DD 21:49:42'),
                                                            label: 'До:',
                                                            labelWidth: 40,
                                                            timepicker: true,
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    generateReport();
                                                                }
                                                            }
                                                        },
                                                        {
                                                            view: 'richselect',
                                                            id: 'selSection',
                                                            name: 'section_id',
                                                            options: [],
                                                            hidden: !(accountInfo.permission == 2),
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    generateReport();
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    cols: [
                                                        {
                                                            view: "text",
                                                            id: "client_uuid",
                                                            name: 'client_uuid',
                                                            value: "",
                                                            label: "Client",
                                                            click: function () {
                                                                keyboard('client_uuid', 'Карта Клиента')
                                                            },
                                                            on: {

                                                                onChange: function (newv, oldv) {
                                                                    generateReport();
                                                                }
                                                            }
                                                        },
                                                        {
                                                            view: 'checkbox',
                                                            id: 'showEmpty',
                                                            name: 'showEmpty',
                                                            label: 'ShowEmpty',
                                                            value: true,
                                                            width: 200,
                                                            labelWidth: 100,
                                                            on: {
                                                                onChange: function (newv, oldv) {
                                                                    generateReport();
                                                                }
                                                            }

                                                        }
                                                    ]
                                                },
                                                {

                                                    view: "text",
                                                    id: "product_code",
                                                    name: 'product_code',
                                                    value: "",
                                                    label: "Product",
                                                    click: function () {
                                                        productBarcode()
                                                    },
                                                    on: {

                                                        onChange: function (newv, oldv) {
                                                            generateReport();
                                                        }
                                                    }
                                                },


                                            ]

                                        }
                                    ]
                                },
                                {view: 'template', id: 'report_view', width: 600, scroll: true}
                            ]
                        }
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            }).show();
            webix.extend($$('report_view'), webix.ProgressBar);

            $$('selSection').getPopup().getList().sync(sections);
            webix.delay(function () {
                $$('selSection').setValue(7);
            });
            $$('report_list').select(1);
        }

        function generateReport() {
            show_progress_icon('report_view');
            var dataValues = $$('frmReport').getValues();
            dataValues.shop_id = 1;
            dataValues.report = report_selected;
            if (dataValues.report == 1) {
                $$("product_code").disable();
                $$("client_uuid").disable();
                $$("showEmpty").disable();
                $$("selSection").enable();
                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        $$('report_view').setHTML(result.text());
                        hide_progress_icon('report_view');
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            } else if (dataValues.report == 2) {
                $$("product_code").disable();
                $$("client_uuid").disable();
                $$("showEmpty").disable();
                $$("selSection").disable();
                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        $$('report_view').setHTML(result.text());
                        hide_progress_icon('report_view');
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            } else if (dataValues.report == 3) {
                $$("product_code").enable();
                $$("client_uuid").disable();
                $$("showEmpty").enable();
                $$("selSection").disable();
                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        dataValues.report = 4;
                        var res = result.json()
                        dataValues.products = res['Items'];
                        webix.ajax().post('/report', dataValues)
                            .then(function (result) {
                                $$('report_view').setHTML(result.text());
                                hide_progress_icon('report_view');
                            })
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            } else if (dataValues.report == 6) {	//console.log(dataValues)
                // dataValues.client_uuid=$$('discount_cards_view').getSelectedItem()['uuid'];
                $$("product_code").disable();
                $$("client_uuid").enable();
                $$("showEmpty").enable();
                $$("selSection").disable();
                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        if (result.text() != "No_Found") {
                            dataValues.report = 7;
                            var res = result.json()
                            dataValues.clients = res['Items'];
                            webix.ajax().post('/report', dataValues)
                                .then(function (result) {
                                    $$('report_view').setHTML(result.text());
                                    hide_progress_icon('report_view');
                                })
                        } else {
                            $$('report_view').setHTML(result.text());
                            hide_progress_icon('report_view');
                        }
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            }
        }

        function printReport() {
            var dataValues = $$('frmReport').getValues();
            dataValues.report = report_selected;
            show_progress_icon('report_view');
            if (dataValues.report == 1 || dataValues.report == 2) {
                webix.ajax().post('/report/print', dataValues)
                    .then(function (result) {
                        hide_progress_icon('report_view');
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                        hide_progress_icon('report_view');
                    });
            } else if (dataValues.report == 3) {
                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        dataValues.report = 5;
                        var res = result.json()
                        dataValues.products = res['Items'];
                        webix.ajax().post('/report', dataValues)
                            .then(function (result) {

                                hide_progress_icon('report_view');
                            })
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            } else if (dataValues.report == 6) {	//console.log(dataValues)
                // dataValues.client_uuid=$$('discount_cards_view').getSelectedItem()['uuid'];

                webix.ajax().post('/report', dataValues)
                    .then(function (result) {
                        if (result.text() != "No_Found") {
                            dataValues.report = 8;
                            var res = result.json()
                            dataValues.clients = res['Items'];
                            webix.ajax().post('/report', dataValues)
                                .then(function (result) {

                                    hide_progress_icon('report_view');
                                })
                        } else {

                            hide_progress_icon('report_view');
                        }
                    })
                    .fail(function (xhr) {
                        var response = JSON.parse(xhr.response);
                        webix.message({type: 'error', text: response.message});
                    });
            }
        }

        function accountsJournal() {
            var selected_order;
            var list_items = [
                {id: '0', name: 'Открытый'},
                {id: '1', name: 'Закрытый'},
                {id: '66', name: 'Ошибочный'},
                {id: '70', name: 'Возврат открытый'},
                {id: '71', name: 'Возврат закрытый'},
            ];

            webix.ui({
                view: 'popup',
                id: 'popupStatus',
                head: 'Статус',
                width: 200,
                body: {
                    view: 'list',
                    id: 'listStatuses',
                    data: [],
                    datatype: 'json',
                    template: '#name#',
                    autoheight: true,
                    select: true,
                    on: {
                        onItemClick: function (id, e, node) {
                            var item = $$('record_list').getItem(selected_order);
                            if ((item.closed).toString() != id) {
                                webix.confirm({
                                    text: 'Изменить статус заказа?', ok: 'Да', cancel: 'Нет', type: 'confirm-warning', callback: function (result) {
                                        if (result) {
                                            webix.ajax().put('/orders/' + item.r_id, {closed: id, synced: 0})
                                                .then(function (result) {
                                                    generateAccountsJournal();
                                                    $$('order_list').clearAll();
                                                    $$('order_list').load('/orders');
                                                })
                                                .fail(function (xhr) {
                                                    var response = JSON.parse(xhr.response);
                                                    webix.message({type: 'error', text: response.message});
                                                });
                                        }
                                    }
                                });
                            }
                            this.getTopParentView().hide();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        var item = $$('record_list').getItem(selected_order);
                        $$('listStatuses').parse(list_items);
                        if (item.closed == 70 || item.closed == 71) {
                            $$('listStatuses').remove([0, 1]);
                        } else {
                            $$('listStatuses').remove([70, 71]);
                        }
                        $$('listStatuses').select((item.closed).toString());
                    }
                }
            });

            webix.ui({
                view: 'window',
                id: 'accountsJournal',
                autowidth: true,
                autoheight: true,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Журнал счетов'},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("popupStatus").close(); $$("accountsJournal").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    rows: [
                        {
                            view: 'form',
                            id: 'frmAccountsJournal',
                            padding: 0,
                            elements: [
                                {
                                    cols: [
                                        {
                                            view: 'datepicker',
                                            id: 'dateFrom',
                                            name: 'dateFrom',
                                            width: 200,
                                            value: lastShift_at,
                                            label: 'От:',
                                            labelWidth: 40,
                                            timepicker: true,
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    generateAccountsJournal();
                                                }
                                            }
                                        },
                                        {
                                            view: 'datepicker',
                                            id: 'dateTo',
                                            name: 'dateTo',
                                            width: 200,
                                            value: moment().format('YYYY-MM-DD 23:59'),
                                            label: 'До:',
                                            labelWidth: 40,
                                            timepicker: true,
                                            on: {
                                                onChange: function (newv, oldv) {
                                                    generateAccountsJournal();
                                                }
                                            }
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            cols: [
                                {
                                    rows: [
                                        {
                                            cols: [
                                                {
                                                    view: 'button', type: 'icon', icon: 'arrow-up', width: 30, height: 30, click: function () {
                                                        $$('pager_records').select('prev');
                                                    }
                                                },
                                                {
                                                    view: 'pager',
                                                    id: 'pager_records',
                                                    template: '<div class="text-center">{common.page()} из #limit#</div>',
                                                    apiOnly: true,
                                                    size: 8,
                                                    animate: {direction: 'top'}
                                                },
                                                {
                                                    view: 'button', type: 'icon', icon: 'arrow-down', width: 30, height: 30, click: function () {
                                                        $$('pager_records').select('next');
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            view: 'datatable',
                                            id: 'record_list',
                                            autowidth: true,
                                            autoConfig: true,
                                            select: 'row',
                                            scroll: false,
                                            columns: [
                                                {id: 'np', header: 'Смена/Чек', width: 100},
                                                //{ id: 'section_name', header: 'Секция', adjust: 'data' },
                                                {id: 'created_at', header: {text: 'Дата', css: 'text-center'}, adjust: 'data', format: webix.Date.dateToStr("%d.%m.%Y %H:%i"), css: 'text-center'},
                                                {id: 'updated_at', header: {text: 'Закрыт', css: 'text-center'}, adjust: 'data', format: webix.Date.dateToStr("%d.%m.%Y %H:%i"), css: 'text-center'},
                                                {id: 'user_name', header: 'Оператор', adjust: 'data'},
                                                {id: 'quantity_sum', header: {text: 'Кол.', css: 'text-center'}, css: 'text-center', width: 50},
                                                {id: 'sum', header: {text: 'Сумма', css: 'text-right'}, css: 'text-right', width: 100, format: webix.i18n.priceFormat},
                                                {id: 'discount_sum', header: {text: 'Скидка', css: 'text-right'}, css: 'text-right', width: 100, format: webix.i18n.priceFormat},
                                                {id: 'total_sum', header: {text: 'Итого', css: 'text-right'}, css: 'text-right', width: 100, format: webix.i18n.priceFormat},
                                                {id: 'synced', header: {text: 'Синхр.', css: 'text-center'}, width: 70, css: 'text-center'},
                                                // {
                                                //     id: 'print', header: '', width: 100,
                                                //     template: function (obj) {
                                                //         if (obj.closed === null)
                                                //             return '';
                                                //         else return '<div class="btnPrint webix_el_button"><button class="webixtype_base">Печать</button></div>';
                                                //     }
                                                // },
                                                {
                                                    id: 'paymentTypes', header: '', width: 100,
                                                },
                                            ],
                                            scheme: {
                                                $init: function (obj) {
                                                    if (obj.closed == 0) {
                                                        obj.np = '<span class="chgStatus"><i class="webix_icon fa-list-alt"></i></span>' + obj.r_id;
                                                    } else if (obj.closed == 70) {
                                                        obj.np = '<span class="fa-stack chgStatus"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-reply fa-stack-2x"></i></span>' + obj.r_id;
                                                    } else if (obj.closed == 1) {
                                                        obj.np = '<span class="fa-stack chgStatus"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-check fa-stack-2x text-success"></i></span>' + obj.r_id;
                                                    } else if (obj.closed == 71) {
                                                        obj.np = '<span class="fa-stack chgStatus"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-reply fa-stack-2x text-success"></i></span>' + obj.r_id;
                                                    } else if (obj.closed == 66) {
                                                        obj.np = '<span class="fa-stack chgStatus"><i class="webix_icon fa-list-alt fa-stack-1x"></i> <i class="webix_icon fa-times fa-stack-2x text-danger"></i></span>' + obj.r_id;
                                                    } else {
                                                        obj.np = '<span class="webix_icon fa-clock-o"></span> ' + obj.r_id;
                                                    }

                                                    if (obj.synced)
                                                        obj.synced = '<span class="webix_icon fa-check-circle-o text-success"></span>';
                                                    else
                                                        obj.synced = '<span class="webix_icon fa-circle-o text-danger"></span>';
                                                }
                                            },
                                            yCount: 8,
                                            pager: 'pager_records',
                                            on: {
                                                onAfterLoad: function () {
                                                    try {
                                                        hide_progress_icon('record_list');
                                                    } catch (e) {
                                                    }
                                                },
                                                onAfterSelect: function (obj) {
                                                    let item = this.getSelectedItem();
                                                    $$('record_details_list').clearAll();
                                                    if (item.closed !== null)
                                                        getOrderDetails(item.r_id);
                                                },
                                            },
                                            onClick: {
                                                'chgStatus': function (e, id, trg) {
                                                    selected_order = id;
                                                    $$('popupStatus').show(e);
                                                },
                                                'btnPrint': function (e, id, trg) {
                                                    webix.confirm({
                                                        text: 'Напечатать фискальный чек?', ok: 'Да', cancel: 'Нет', type: 'confirm-warning', callback: function (result) {
                                                            let item = $$('record_list').getItem(id);
                                                            webix.ajax().post('/orders/' + item.r_id + '/print/', {is_fiscal: +result})
                                                                .then(function (result) {
                                                                    var res = result.json();
                                                                    if (res.status == 'error')
                                                                        webix.message({type: 'error', text: res.message});
                                                                    else
                                                                        webix.message({type: 'success', text: 'Распечатано!'});
                                                                })
                                                                .fail(function (xhr) {
                                                                    var response = JSON.parse(xhr.response);
                                                                    webix.message({type: 'error', text: response.message});
                                                                });
                                                        }
                                                    });
                                                }
                                            },
                                        },
                                        {
                                            view: 'datatable',
                                            id: 'record_details_list',
                                            height: 200,
                                            autoConfig: true,
                                            select: 'row',
                                            //scroll: false,
                                            math: true,
                                            columns: [
                                                {id: 'created_at', header: {text: 'Дата', css: 'text-center'}, adjust: 'data', format: webix.Date.dateToStr("%d.%m.%Y %H:%i"), css: 'text-center'},
                                                {id: 'name', header: 'Наименование', fillspace: true},
                                                {id: 'price', header: {text: 'Цена', css: 'text-right'}, css: 'text-right', width: 100, format: webix.i18n.priceFormat},
                                                {id: 'quantity', header: {text: 'Кол.', css: 'text-center'}, css: 'text-center', width: 50},
                                                {id: 'sum', header: {text: 'Сумма', css: 'text-right'}, css: 'text-right', width: 100, math: '[$r, price] * [$r, quantity]', format: webix.i18n.priceFormat},
                                                {id: 'discount_amount', header: {text: 'Скидка', css: 'text-right'}, css: 'text-right', width: 100, format: webix.i18n.priceFormat},
                                                {id: 'total', header: {text: 'Итого', css: 'text-right'}, css: 'text-right', width: 100, math: '[$r, sum] - [$r, discount_amount]', format: webix.i18n.priceFormat},
                                            ],
                                            scheme: {
                                                $init: function (obj) {
                                                    //if (obj.serial) obj.product_name = (obj.serial.product.name);
                                                    if (obj.serial) obj.name = (obj.serial.product.name) + ' / ' + (obj.serial.barcode);
                                                    else if (obj.product) obj.name = (obj.product.name)
                                                }
                                            },
                                            on: {
                                                onAfterLoad: function () {
                                                    try {
                                                        hide_progress_icon('record_details_list');
                                                    } catch (e) {
                                                    }
                                                },
                                            }
                                        }
                                    ]
                                },
                            ]

                        }
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            }).show();
            webix.extend($$('record_list'), webix.ProgressBar);
            webix.extend($$('record_details_list'), webix.ProgressBar);
            generateAccountsJournal();
        }

        function generateAccountsJournal() {
            show_progress_icon('record_list');
            var dataValues = $$('frmAccountsJournal').getValues();

            webix.ajax().post('/accounts-journal', dataValues)
                .then(function (result) {
                    var data = result.json();
                    $$('record_list').clearAll();
                    $$('record_list').parse(data);
                    hide_progress_icon('record_list');
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                });
        }

        function getOrderDetails(id) {
            show_progress_icon('record_details_list');

            webix.ajax().get('/order-details/' + id)
                .then(function (result) {
                    var data = result.json();
                    $$('record_details_list').clearAll();
                    $$('record_details_list').parse(data);
                    hide_progress_icon('record_details_list');
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                });
        }

        var return_product_selected, return_serial_selected, return_order_selected;

        function returnProduct() {
            var winReturnProduct = webix.ui({
                view: 'window',
                id: 'winReturnProduct',
                autowidth: true,
                head: {
                    view: 'toolbar',
                    cols: [
                        {
                            margin: 4,
                            rows: [
                                {view: 'label', label: 'Возврат товара', height: -1},
                            ]
                        },
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winReturnProduct").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    width: 350,
                    view: 'form',
                    id: 'frmReturnProduct',
                    scroll: false,
                    padding: 15,
                    elements: [
                        {
                            view: 'text', id: 'returnBarcode', name: 'returnBarcode', value: '', placeholder: 'Баркод или Артикул', popup: 'keypadPopup',
                            on: {
                                onItemClick: function (id, e) {
                                    objValue = $$(id);
                                    titleKeyPad = 'Введите баркод или артикул';
                                    // keyPadOptions.callback = getReturnBarcode;
                                    keyPadOptions.callback = getReturnOrder;
                                },
                                onEnter: function (ev) {
                                    let value = this.getValue();
                                    //getReturnBarcode(value);
                                    getReturnOrder(value);

                                },
                            }
                        },
                        {view: 'label', inputWidth: 300, height: 80, id: 'product-info', css: 'product-info'},
                        //{ view: 'datepicker', label: 'Дата возврата:', name: 'return_date', labelWidth: 120, format: webix.Date.dateToStr("%d.%m.%Y"), stringResult: true, value: new Date() },
                        {
                            view: 'button', type: 'form', value: 'Применить', id: 'btnReturnApply', disabled: true,
                            click: function () {
                                //########################################################################################


                                $$('order_list').add({id: 0, closed: 70}, 0);
                                $$('order_list').refresh();
                                setTimeout(() => {


                                    var or = $$('order_list').getSelectedItem().id
                                    var dtOrderDetails = $$('order_details');
                                    webix.ajax().get('/order-details/' + return_order_selected).then(function (result) {
                                        var res = result.json()
                                        //console.log(res)

                                        for (i = 0; i < res.length; i++) {
                                            data = {
                                                order_id: or,
                                                product_id: res[i].product.id,
                                                price: res[i].price,
                                                quantity: res[i].quantity * -1,
                                                sum: res[i].price * res[i].quantity,
                                                discount_percent: res[i].discount_percent,
                                                discount_amount: res[i].discount_amount * -1,
                                                total: res[i].total,
                                                product: res[i].product,
                                                updated_at: res[i].product.updated_at
                                            };

                                            //console.log(data)
                                            dtOrderDetails.add(data, 0);
                                            dtOrderDetails.refreshColumns();
                                            dtOrderDetails.getPager().select('last');
                                            dtOrderDetails.select(dtOrderDetails.getLastId());
                                        }


                                    })
                                    //dtOrderDetails.refreshColumns();
                                }, 500);
                                this.getTopParentView().close();
                                //########################################################################################
                                // product_selected = return_product_selected;
                                // serial_selected = return_serial_selected;

                                // if (!product_selected && serial_selected)
                                // 	product_selected = serial_selected.product;

                                // product_selected.price = product_selected.price;
                                // product_selected.quantity = -1;
                                // if (return_order_selected) product_selected.updated_at = return_order_selected.updated_at;

                                // if (serial_selected) {
                                // 	serial_selected.price = serial_selected.price;
                                // 	serial_selected.quantity = -1;
                                // 	if (return_order_selected) serial_selected.updated_at = return_order_selected.updated_at;
                                // 	serial_selected.discount_amount *= -1;
                                // }
                                // if (order_item_selected && order_item_selected.closed == 70) {
                                // 	selectSerial();
                                // } else {

                                // 	$$('order_list').add({id: 0, closed: 70}, 0);
                                // 	$$('order_list').refresh();
                                // 	webix.delay(selectSerial, null, null, 1000);
                                // }

                                // return_product_selected = null;
                                // return_serial_selected = null;
                                // return_order_selected = null;
                                // this.getTopParentView().close();
                            }
                        },
                        {
                            view: 'button', value: 'Отмена',
                            click: function () {
                                this.getTopParentView().close();
                            }
                        },
                    ]
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    }
                }
            });
            winReturnProduct.show();
            webix.extend($$('product-info'), webix.ProgressBar);

            $$('frmReturnProduct').focus();
        }

        function getReturnOrder(id) {
            $$('product-info').setHTML('');
            show_progress_icon('product-info');
            webix.ajax().get('/order/' + id).then(function (result) {
                var data = result.json();
                console.log(data)
                var content = '';
                if (data["0"]) {

                    return_order_selected = data["0"].id;
                    $$('btnReturnApply').enable();
                    content += 'Заказ: <strong>' + data["0"].id + '</strong>, Дата: <strong>' + webix.i18n.fullDateFormatStr(data["0"].created_at);
                    $$('product-info').setHTML(content);
                } else {

                    content += 'Такого Ордера не сушествует';
                    $$('product-info').setHTML(content);
                }
            })

        }

        function syncProducts() {
            $progress_win = 'mainview';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/products/').then(function (result) {
                var res = result.json()
                var products = res['Items']
                // var pr =products[0];
                for (var i = 0; i < products.length; i++) {
                    products[i].PLU = leftPad(products[i].PLU, 6)
                }
                webix.ajax().post('/sync/products/', {products}).then(function (result) {

                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: response.message});
                })
            })
        }

        function syncDicount() {
            $progress_win = 'mainview';
            try {
                show_progress_icon($progress_win);
            } catch (err) {
            }
            try {
                show_progress_icon('winAddDiscount')
            } catch (err) {
            }
            try {
                show_progress_icon('winDiscountCards')
            } catch (err) {
            }


            webix.ajax().get('/sync/cards/').then(function (result) {
                var res = result.json()
                var cards = res['Items']

                webix.ajax().post('/sync/cards/', {cards}).then(function (result) {
                    try {
                        hide_progress_icon($progress_win);
                    } catch (err) {
                    }
                    try {
                        hide_progress_icon('winAddDiscount')
                    } catch (err) {
                    }
                    try {
                        hide_progress_icon('winDiscountCards')
                    } catch (err) {
                    }


                    webix.message({type: 'success', text: "успешно синхронизировано"});
                    $$('winDiscountCards').refresh()
                })
            })
        }

        function syncStock() {
            $progress_win = 'winSections';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/stocks/').then(function (result) {
                var res = result.json()
                var stocks = res['Items']
                webix.ajax().post('/sync/stocks/', {stocks}).then(function (result) {

                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: "успешно синхронизировано"});
                })
            })
        }

        function pushOrders() {
            webix.ajax().get('/sync/get-unsynced-orders-and-shifts').then(function (result) {
                const pushOrderWindow = webix.ui({
                    view: 'window',
                    id: 'pushOrderWindow',
                    modal: true,
                    autowidth: true,
                    minHeight: 400,
                    maxHeight: window.screen.height * 0.9,
                    close: true,
                    scroll: true,
                    head: {
                        view: 'toolbar',
                        cols: [
                            {
                                margin: 4,
                                rows: [
                                    {view: 'label', label: 'Выгрузка чеков', height: -1},
                                ]
                            },
                            {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("pushOrderWindow").close();', css: 'text-center'}
                        ]
                    },
                    position: 'center',
                    body: {
                        view: 'datatable',
                        id: 'pushOrderWindowBody',
                        width: 600,
                        maxHeight: window.screen.height * 0.85,
                        scroll: true,
                        minHeight: 400,
                        columns: [
                            {id: 'item', header: {text: '#', css: 'text-center'}, width: 200},
                            {id: 'createdAt', header: {text: 'Дата создания', css: 'text-center'}, width: 200},
                            {id: 'status', header: {text: 'Статус обновления', css: 'text-center'}, width: 200},
                        ],
                        data: result.json().data.map((element) => {
                            return {
                                createdAt: moment(element.createdAt).format('DD.MM.YY HH:mm'),
                                item: (element.type === 'order' ? 'Ордер #' : 'Смена #') + element.id.toString(),
                                status: "Подготовка",
                                type: element.type,
                                sub_id: element.id,
                            }
                        }),
                    },
                    on: {
                        onShow: function () {
                            const body = $$('pushOrderWindowBody');
                            body.eachRow(function (row) {
                                const record = body.getItem(row);
                                record.status = "Отправка";
                                this.updateItem(row, record);
                                webix.ajax().post(`/sync/sync-closed-${record.type}/${record.sub_id}`, {})
                                    .then((result) => {
                                        record.status = "Отправленно";
                                        this.updateItem(row, record);
                                    })
                                    .fail((xhr) => {
                                        record.status = "Ошибка";
                                        this.updateItem(row, record);
                                    });
                            });
                        },
                    }
                });

                pushOrderWindow.show();

            });
        }


        function winDiscountCards() {
            webix.ui({
                view: 'window',
                id: 'winDiscountCards',
                autowidth: true,
                height: 500,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Дисконтные карты'},
                        {},
                        {view: 'button', width: 80, label: 'Отчет', align: 'right', click: "clientReport();", css: 'background-primary'},
                        {view: 'button', width: 80, label: 'Загрузить', align: 'right', click: "syncDicount();", css: 'background-primary'},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winDiscountCards").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'datatable',
                    id: 'discount_cards_view',
                    select: true,
                    clipboard: 'selection',
                    //autowidth: true,
                    width: 1100,
                    fixedRowHeight: false,
                    rowLineHeight: 25,
                    rowHeight: 25,
                    columns: [
                        {id: 'name', header: ['Номер карты', {content: 'textFilter'}], width: 80},
                        {id: 'number', header: ['Л. Номер', {content: 'textFilter'}], width: 150},
                        {id: 'owner_name', header: ['Имя клиента', {content: 'textFilter'}], adjust: 'data'},
                        {id: 'birthday_at', header: {text: 'Дата рождения', css: 'text-center'}, width: 100, css: 'text-center', format: webix.i18n.dateFormatStr},
                        {id: 'phone_mobile', header: [{text: 'Телефон', css: 'text-center'}, {content: 'textFilter'}], css: 'text-center', width: 150},
                        {id: 'percent', header: {text: 'Процент скидки', css: 'text-center'}, adjust: 'header', css: 'text-center', width: 100},
                        {id: 'sum_sales', header: {text: 'Сумма<br>продаж', css: 'text-right multiline'}, css: 'text-right', width: 100},
                        {id: 'comment', header: {text: 'Комментарий', css: 'text-center'}, width: 200},
                        //{ id: 'activated', header: 'Активность', checkValue: 1, uncheckValue: 0, template: custom_checkbox, adjust: 'header', css: 'text-center' },
                    ],
                    url: '/admin/discount-cards/data',
                    on: {
                        onAfterLoad: function () {
                            // webix.delay(function() {
                            // this.myAdjustRowHeight(['full_name', 'comment', 'phone_mobile'], true);
                            this.render();
                            // }, this);
                        },
                        onDataUpdate: function (id, data) {
                            // this.myAdjustRowHeight(['full_name', 'comment', 'phone_mobile'], true, id);
                            this.render();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                }
            }).show();
            webix.extend($$('winDiscountCards'), webix.ProgressBar);
        }

        function clientReport() {
            // var item =$$('discount_cards_view').getSelectedItem()['uuid'];
            if ($$('discount_cards_view').getSelectedItem()) {
                webix.ui({
                    view: 'window',
                    id: 'clientReport',
                    autowidth: true,
                    height: 800,
                    head: {
                        view: 'toolbar',
                        height: 50,
                        cols: [
                            {view: 'label', label: 'Отчёты'},
                            {},
                            {view: 'button', type: 'icon', icon: 'print', width: 80, label: 'Печать', align: 'right', click: printReport, css: 'background-primary'},
                            {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("clientReport").close();', css: 'text-center'}
                        ]
                    },
                    position: 'center',
                    modal: true,
                    body: {
                        cols: [
                            {
                                rows: []
                            },
                            {
                                rows: [
                                    {
                                        view: 'form',
                                        id: 'frmReport2',
                                        elements: [
                                            {
                                                cols: [
                                                    {
                                                        view: 'datepicker',
                                                        id: 'dateFrom',
                                                        name: 'dateFrom',
                                                        width: 200,
                                                        value: lastShift_at,
                                                        label: 'От:',
                                                        labelWidth: 40,
                                                        timepicker: true,
                                                        on: {
                                                            onChange: function (newv, oldv) {
                                                                generateReportClient();
                                                            }
                                                        }
                                                    },
                                                    {
                                                        view: 'datepicker',
                                                        id: 'dateTo',
                                                        name: 'dateTo',
                                                        width: 200,
                                                        value: moment().format('YYYY-MM-DD 21:49:42'),
                                                        label: 'До:',
                                                        labelWidth: 40,
                                                        timepicker: true,
                                                        on: {
                                                            onChange: function (newv, oldv) {
                                                                generateReportClient();
                                                            }
                                                        }
                                                    },

                                                ]
                                            }
                                        ]
                                    },
                                    {view: 'template', id: 'report_view2', scroll: true, width: 600, height: 800,}
                                ]
                            }
                        ]
                    },
                    on: {
                        onShow: function () {
                            windowOpened = true;
                        },
                        onDestruct: function () {
                            windowOpened = false;
                        }
                    }
                }).show();
                webix.extend($$('report_view2'), webix.ProgressBar);
                generateReportClient();
            } else {
                webix.alert("Выберите Клиента");
            }
        }

        function generateReportClient() {
            show_progress_icon('report_view2');
            var dataValues = $$('frmReport2').getValues();
            dataValues.shop_id = 1;
            dataValues.report = 5;
            dataValues.client_uuid = $$('discount_cards_view').getSelectedItem()['number'];

            webix.ajax().post('/report', dataValues)
                .then(function (result) {
                    dataValues.report = 6;
                    var res = result.json()
                    dataValues.clients = res['Items'];
                    webix.ajax().post('/report', dataValues)
                        .then(function (result) {
                            $$('report_view2').setHTML(result.text());
                            hide_progress_icon('report_view2');
                        })
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                });
        }

        function winSections() {
            webix.ui({
                view: 'window',
                id: 'winSections',
                autowidth: true,
                height: 500,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Склады'},
                        {},
                        {view: 'button', width: 80, label: 'Загрузить', align: 'right', click: "syncStock();", css: 'background-primary',},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winSections").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'datatable',
                    id: 'discount_cards_view',
                    select: 'cell',
                    clipboard: 'selection',
                    autowidth: true,
                    fixedRowHeight: false,
                    rowLineHeight: 25,
                    rowHeight: 25,
                    columns: [
                        {id: 'uuid', header: ['UUID', {content: 'textFilter'}], width: 300},
                        {id: 'name', header: ['Имя Секцыий', {content: 'textFilter'}], width: 300},

                    ],
                    url: '/data/sections',
                    on: {
                        onAfterLoad: function () {
                            // webix.delay(function() {

                            this.render();
                            // }, this);
                        },
                        onDataUpdate: function (id, data) {

                            this.render();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                }
            }).show();
            webix.extend($$('winSections'), webix.ProgressBar);
        }

        function winUsers() {
            webix.ui({
                view: 'window',
                id: 'winUsers',
                autowidth: true,
                height: 500,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Пользаватели'},
                        {},
                        {view: 'button', width: 80, label: 'Загрузить', align: 'right', click: "syncUsers();", css: 'background-primary',},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winUsers").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'datatable',
                    id: 'discount_cards_view',
                    select: 'cell',
                    clipboard: 'selection',
                    autowidth: true,
                    fixedRowHeight: false,
                    rowLineHeight: 25,
                    rowHeight: 25,
                    columns: [
                        // { id: 'uuid', header: ['UUID', { content: 'textFilter' }], width: 300 },
                        {id: 'name', header: ['Имя  Пользавателя', {content: 'textFilter'}], width: 300},
                        {id: 'permission', header: ['Уровень Доступа', {content: 'textFilter'}], width: 200},
                    ],
                    url: 'admin/users/data',
                    on: {
                        onAfterLoad: function () {
                            // webix.delay(function() {

                            this.render();
                            // }, this);
                        },
                        onDataUpdate: function (id, data) {

                            this.render();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                }
            }).show();
            webix.extend($$('winUsers'), webix.ProgressBar);
        }

        function syncUsers() {
            $progress_win = 'winUsers';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/users/').then(function (result) {
                var res = result.json()
                var users = res['Items']
                webix.ajax().post('/sync/users/', {users}).then(function (result) {

                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: "успешно синхронизировано"});
                })
            })
        }

        function leftPad(number, targetLength) {
            var output = number + '';
            while (output.length < targetLength) {
                output = '0' + output;
            }
            return output;
        }

        function syncColors() {
            $progress_win = 'winColors';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/colors/').then(function (result) {
                var res = result.json()
                var colors = res['Items']
                for (var i = 0; i < colors.length; i++) {
                    colors[i].Code = leftPad(colors[i].Code, 3)
                }

                webix.ajax().post('/sync/colors/', {colors}).then(function (result) {

                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: "успешно синхронизировано"});
                })
            })
        }

        function syncPictures() {
            $progress_win = 'mainview';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/pictures/').then(function (result) {
                var res = result.json()
                var pictures = res['Items']
                webix.ajax().post('/sync/pictures/', {pictures}).then(function (result) {

                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: "успешно синхронизировано"});
                })
            })
        }

        function winColors() {
            webix.ui({
                view: 'window',
                id: 'winColors',
                autowidth: true,
                height: 500,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Цвета Продуктов'},
                        {},
                        {view: 'button', width: 80, label: 'Загрузить', align: 'right', click: "syncColors();", css: 'background-primary',},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winColors").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'datatable',
                    id: 'colors_view',
                    select: 'cell',
                    clipboard: 'selection',
                    autowidth: true,
                    fixedRowHeight: false,
                    rowLineHeight: 25,
                    rowHeight: 25,
                    columns: [
                        // { id: 'uuid', header: ['UUID', { content: 'textFilter' }], width: 300 },
                        {id: 'barcodeId', header: ['Баркод', {content: 'textFilter'}], width: 300},
                        {id: 'name', header: ['Цвет', {content: 'textFilter'}], width: 200},
                    ],
                    url: '/data/colors',
                    on: {
                        onAfterLoad: function () {
                            // webix.delay(function() {

                            this.render();
                            // }, this);
                        },
                        onDataUpdate: function (id, data) {

                            this.render();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                }
            }).show();
            webix.extend($$('winColors'), webix.ProgressBar);
        }

        function syncSizes() {
            $progress_win = 'winSizes';
            show_progress_icon($progress_win);
            webix.ajax().get('/sync/sizes/').then(function (result) {
                var res = result.json()
                var sizes = res['Items']
                webix.ajax().post('/sync/sizes/', {sizes}).then(function (result) {
                    hide_progress_icon($progress_win);
                    webix.message({type: 'success', text: "успешно синхронизировано"});
                })
            })
        }

        function winSizes() {
            webix.ui({
                view: 'window',
                id: 'winSizes',
                autowidth: true,
                height: 500,
                head: {
                    view: 'toolbar',
                    height: 50,
                    cols: [
                        {view: 'label', label: 'Размеры Продуктов'},
                        {},
                        {view: 'button', width: 80, label: 'Загрузить', align: 'right', click: "syncSizes();", css: 'background-primary',},
                        {view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', click: '$$("winSizes").close();', css: 'text-center'}
                    ]
                },
                position: 'center',
                modal: true,
                body: {
                    view: 'datatable',
                    id: 'sizes_view',
                    select: 'cell',
                    clipboard: 'selection',
                    autowidth: true,
                    fixedRowHeight: false,
                    rowLineHeight: 25,
                    rowHeight: 25,
                    columns: [
                        // { id: 'uuid', header: ['UUID', { content: 'textFilter' }], width: 300 },
                        {id: 'barcodeId', header: ['Баркод', {content: 'textFilter'}], width: 300},
                        {id: 'name', header: ['Размер', {content: 'textFilter'}], width: 200},
                    ],
                    url: '/data/sizes',
                    on: {
                        onAfterLoad: function () {
                            // webix.delay(function() {

                            this.render();
                            // }, this);
                        },
                        onDataUpdate: function (id, data) {

                            this.render();
                        }
                    }
                },
                on: {
                    onShow: function () {
                        windowOpened = true;
                    },
                    onDestruct: function () {
                        windowOpened = false;
                    },
                }
            }).show();
            webix.extend($$('winSizes'), webix.ProgressBar);
        }

        // $$("datails_imageview").setValues({pic:'/images/noPic.png'});
    </script>
@endsection