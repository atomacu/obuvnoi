@extends('layouts.app')

@section('content')
    <div class='home-area'>
        <div id="app-content">
            <p class="text-center"><img src="/images/ignored_dir/logo.png" width='200px'></p>
            <div id="app-details">
                <div>
                    <label>Version:</label>
                    <span>2.1</span>
                </div>
                <div>
                    <label>Licence:</label>
                    <span>123-456-789</span>
                </div>
                <div>
                    <label>Diagnostics:</label>
                    <span>test</span>
                </div>
                <div>
                    <label>Message:</label>
                    <span></span>
                </div>
            </div>
            <div id='app-container'></div>
        </div>
    </div>

    <script>
        keyPadOptions = {
            hide: false,
            callback: onLogin
        };
        popupOpened = true;

        webix.ui({
            id: 'keyPadAuth',
            container: 'app-container',
            rows: [
                {
                    view: 'toolbar', cols: [
                        {view: 'label', label: 'Авторизация'},
                    ]
                },
                {
                    view: 'form',
                    id: 'frm_keypad',
                    elementsConfig: {css: 'keypad'},
                    elements: [
                        {
                            rows: [
                                {
                                    cols: [{
                                        view: 'search',
                                        css: 'inputIcon',
                                        readonly: true,
                                        icon: 'search fa-angle-double-left',
                                        name: 'keypad_value',
                                        placeholder: 'PIN',
                                        height: 60,
                                        type: 'password',
                                        required: true,
                                        on: {
                                            onSearchIconClick: function (e) {
                                                var valOld = $$(this).getValue();
                                                var valNew = valOld.substr(0, valOld.length - 1);
                                                $$(this).setValue(valNew);
                                            },
                                            onAfterRender: function () {
                                                $$(this).focus();
                                            }
                                        }
                                    }]
                                },
                                {
                                    rows: [
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                {view: 'button', value: '7', click: add_value},
                                                {view: 'button', value: '8', click: add_value},
                                                {view: 'button', value: '9', click: add_value}
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                {view: 'button', value: '4', click: add_value},
                                                {view: 'button', value: '5', click: add_value},
                                                {view: 'button', value: '6', click: add_value}
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                {view: 'button', value: '1', click: add_value},
                                                {view: 'button', value: '2', click: add_value},
                                                {view: 'button', value: '3', click: add_value}
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                {view: 'button', value: 'C', click: clear_value},
                                                {view: 'button', value: '0', click: add_value},
                                                {
                                                    view: 'button',
                                                    value: 'OK',
                                                    id: 'btnOK',
                                                    click: function (id, e) {
                                                        webix.storage.session.put('shop_id', 1);
                                                        webix.storage.cookie.put('shop_id', parseInt(1));
                                                        //$that.getTopParentView().hide();
                                                        okKeyPad(id, e, $$('btnOK'));
                                                    }
                                                }
                                            ]
                                        },
                                    ]
                                }
                            ],
                        }
                    ]
                }
            ]
        });
		webix.extend($$('keyPadAuth'), webix.ProgressBar);
        /*var shops = new webix.DataCollection({
            scheme: {
                  $init: function(obj) { obj.value = obj.name; }
            },
            url: '/data/shops'
        });*/
        webix.Touch.limit(true);

        // webix.ui({
        //     view: 'popup',
        //     id: 'select_shop',
        //     body: {
        //     	rows: [
        //     		{ template: 'Выберите магазин', type: 'header', height: 30, css: 'list-header'},
        //     		{
        // 		        view: 'list',
        // 		        url: '/data/shops',
        // 		        template: "#name#",
        // 			    //autoheight: true,
        // 			    select: true,
        // 				on: {
        // 					onItemClick: function(id, e, node) {
        // 						var item = this.getItem(id);
        // 						var $that = this;
        // 				        webix.confirm({text: 'Выбран магазин: <br><span class="text-danger">' + item.name + '</span>', ok: 'Да', cancel: 'Нет', type: 'confirm-warning', callback: function(result) {
        // 				                if (result) {
        // 						            webix.storage.session.put('shop_id', id);
        // 						            webix.storage.cookie.put('shop_id', parseInt(id));
        // 									$that.getTopParentView().hide();
        // 									okKeyPad(id, e, $$('btnOK'));
        // 				                }
        // 				            }
        // 				        });
        // 					}
        // 				},
        // 			}
        // 	    ]
        //     }
        // }).hide();

    </script>
@endsection