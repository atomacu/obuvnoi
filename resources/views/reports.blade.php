@extends('layouts.app')

@section('content')
    <script>
        var report_selected;
        var lastShift_at = moment().format('YYYY-MM-DD 00:00');

        var shops = new webix.DataCollection({
            url: '/data/shops',
            scheme: {
                $init: function (obj) {
                    obj.value = obj.name
                }
            }
        });

        webix.ui({
            view: 'window',
            id: 'winReports',
            autowidth: true,
            fullscreen: true,
            head: {
                view: 'toolbar',
                height: 50,
                cols: [
                    {view: 'label', label: 'Отчёты'},
                    {},
                    {view: 'button', type: 'icon', icon: 'print', width: 80, label: 'Печать', align: 'right', click: printReport, css: 'background-primary'},
                ]
            },
            position: 'center',
            modal: true,
            body: {
                cols: [
                    {
                        rows: [
                            {
                                view: 'list',
                                width: 150,
                                select: true,
                                data: [{id: 1, title: 'Анализ продаж'}, {id: 2, title: 'Сменный отчёт'}],
                                template: '#title#',
                                on: {
                                    onAfterLoad: function () {
                                        try {
                                            hide_progress_icon('report_view');
                                        } catch (e) {
                                        }
                                    },
                                    onAfterSelect: function (id) {
                                        report_selected = id;
                                        generateReport();
                                    },
                                }
                            }
                        ]
                    },
                    {
                        rows: [
                            {
                                view: 'form',
                                id: 'frmReport',
                                elements: [
                                    {
                                        cols: [
                                            {
                                                view: 'datepicker',
                                                id: 'dateFrom',
                                                name: 'dateFrom',
                                                required: true,
                                                width: 200,
                                                value: lastShift_at,
                                                label: 'От:',
                                                labelWidth: 40,
                                                timepicker: true,
                                                on: {
                                                    onChange: function (newv, oldv) {
                                                        generateReport();
                                                    }
                                                }
                                            },
                                            {
                                                view: 'datepicker',
                                                id: 'dateTo',
                                                name: 'dateTo',
                                                required: true,
                                                width: 200,
                                                value: moment().format('YYYY-MM-DD 23:59'),
                                                label: 'До:',
                                                labelWidth: 40,
                                                timepicker: true,
                                                on: {
                                                    onChange: function (newv, oldv) {
                                                        generateReport();
                                                    }
                                                }
                                            },
                                            {
                                                view: 'richselect',
                                                id: 'selShop',
                                                name: 'shop_id',
                                                required: true,
                                                options: [],
                                                on: {
                                                    onChange: function (newv, oldv) {
                                                        generateReport();
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            {view: 'template', id: 'report_view', width: 600, scroll: true}
                        ]
                    }
                ]
            },
            on: {
                onShow: function () {
                    windowOpened = true;
                },
                onDestruct: function () {
                    windowOpened = false;
                }
            }
        }).show();
        webix.extend($$('report_view'), webix.ProgressBar);

        $$('selShop').getPopup().getList().sync(shops);
        webix.delay(function () {
            $$('selShop').setValue($$('selShop').getPopup().getList().getFirstId());
        }, null, null, 300);

        function generateReport() {
            if (!$$('frmReport').validate()) {
                webix.message({type: 'error', text: 'Заполните необходимые поля!'});
                return;
            }

            show_progress_icon('report_view');
            var dataValues = $$('frmReport').getValues();
            dataValues.report = report_selected;

            webix.ajax().post('/report', dataValues)
                .then(function (result) {
                    $$('report_view').setHTML(result.text());
                    hide_progress_icon('report_view');
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                });
        }

        function printReport() {
            var dataValues = $$('frmReport').getValues();
            dataValues.report = report_selected;
            show_progress_icon('report_view');
            webix.ajax().post('/report/print', dataValues)
                .then(function (result) {
                    hide_progress_icon('report_view');
                })
                .fail(function (xhr) {
                    var response = JSON.parse(xhr.response);
                    webix.message({type: 'error', text: response.message});
                    hide_progress_icon('report_view');
                });
        }

    </script>
@endsection