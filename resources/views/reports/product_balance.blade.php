@extends('layouts.report')

@section('content')
	<div class="uk-text-center"><img src="{{ $companyInfo['shop_logo'] }}" width="150"></div>
	<div class="uk-text-center uk-margin-small">{!! $companyInfo['shop_address'] !!}</div>
	<h3 class="uk-text-center uk-margin-remove">Product Balace</h3>
	<div class="uk-text-center">pentru perioada de la {{ $dateFrom }}<br>la {{ $dateTo }}</div>
	<hr class="uk-margin-remove-bottom">
	<div class="uk-text-right uk-text-small">elaborat: {{ date('d.m.y H:i:s') }}</div>
    

       
    <table style="border: 1px solid black;  border-collapse: collapse;  width: 100%;" >
    <tr>
        <th style="border: 1px solid black;"  width="90%" colspan="4">Ноиминования</th> 
        <th style="border: 1px solid black;"  width="10%">Кол.</th> 
        </tr>
   @foreach ($products as $product) 
                            <?php 
                                    $qtyPerProduct=0;                          
                                        foreach($product["StorageListOst"] as $storage){
                                            $aux2=0;
                                            foreach($storage["Sizes"] as $size){
                                                $aux1=0;
                                                foreach($size["Сolors"] as $color){
                                                    $aux1+=$color["TurnroverOut"];
                                                }
                                                $aux2+= $aux1;    
                                            }
                                            $qtyPerProduct+=$aux2;
                                        }
                                ?>
        <tr>
        <td style="border: 1px solid black;"   colspan="4"><div style="float:left;"><b>{{$product["Name"]}}</b></div>  <div align="right" ><b> Price: {{$product["Price"]}}</b></div> </td> 
        <td style="border: 1px solid black;" >{{$qtyPerProduct}}</td> 
        </tr>
        @foreach ($product["StorageListOst"] as $storage) 
        <?php 
            $qtyPerStorage =0;  
             foreach($storage["Sizes"] as $size){
                $aux=0;
                foreach($size["Сolors"] as $color){
                    $aux+=$color["TurnroverOut"];
                }
                $qtyPerStorage+= $aux; 
            }
        ?>
            <tr>
            <td style="border: 1px solid black;"   width="5%" ></td> 
            <td style="border: 1px solid black;"  colspan="3"><b>{{$storage["StorageName"]}}</b></td> 
            <td style="border: 1px solid black;" >{{$qtyPerStorage}}</td> 
            </tr>
            @foreach ($storage["Sizes"] as $size) 
            <?php 
                $qtyPerSize=0;
                 foreach($size["Сolors"] as $color){
                    $qtyPerSize+=$color["TurnroverOut"];
                }
            ?>
                <tr>
                <td style="border: 1px solid black;"  width="10%" colspan="2"></td> 
                <td style="border: 1px solid black;"  colspan="2">Размер:{{$size["SizeName"]}}</td> 
                <td style="border: 1px solid black;" >{{$qtyPerSize}}</td> 
                </tr>
           
            <tr>
                <td style="border: 1px solid black;"  width="15%"colspan="3" ></td> 
                <td style="border: 1px solid black;" ><i>Цвета:
                @foreach ($size["Сolors"] as $color) 
                {{$color["ColorName"]}} : {{$color["TurnroverOut"]}} ,  
                @endforeach
                </i></td> 
                <td style="border: 1px solid black;" ></td> 
                </tr>
            @endforeach
        @endforeach
   @endforeach
   </table>   
   <br>
   <br>
   <br>
   <br>
   <br>
@endsection