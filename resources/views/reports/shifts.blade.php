@extends('layouts.report')

@section('content')
	<div class="uk-text-center"><img src="{{ $companyInfo['shop_logo'] }}" width="150"></div>
	<div class="uk-text-center uk-margin-small">{!! $companyInfo['shop_address'] !!}</div>
	<h3 class="uk-text-center uk-margin-remove">Raport ZI</h3>
	<div class="uk-text-center">pentru perioada de la {{ $dateFrom }} la {{ $dateTo }}</div>
	<hr class="uk-margin-remove-bottom">
	<div class="uk-text-right uk-text-small">elaborat: {{ date('d.m.y H:i:s') }}</div>
	<table class="uk-table uk-table-small uk-table-hover">
    <thead>
        <tr>
            <th>Cec</th>
            <th class="uk-text-right">Suma</th>
            <th class="uk-text-right">Eroare</th>
        </tr>
    </thead>
    <tbody>
		<?php
			$user_name = '';
			$user_total = 0;
			$user_total_error = 0;
			$total = 0;
			$total_error = 0;
			$total_payments = [];
			$total_discount=0;
			$total_sum=0;
		?>
		@foreach($records as $index => $record)
		
			@if ($index == 0 || $user_name != $record->user->name)
				@if ($index > 0)
				<tr class="uk-text-bold">
					<td class="uk-text-right">Total:</td>
					<td class="uk-text-right">{{ number_format($user_total, 2) }}</td>
					<td class="uk-text-right">{{ number_format($user_total_error, 2) }}</td>
				</tr>
				@endif
				<tr>
					<td colspan="3" class="uk-text-bold uk-padding-remove-bottom">Casier: {{ $record->user->name }} - </td>
				</tr>
				<?php
					$user_total = 0;
					$user_total_error = 0;
				?>
			@endif
			<tr class="@if($record->closed == 66) uk-text-warning @endif">
				<td class="uk-padding-remove-vertical">
					<div class="uk-margin-left">
						No. {{ $record->id }} - {{ $record->updated_at->format('d.m.y H:i:s') }} 
						@if ($record->discountCard)
						<div class="uk-margin-left">
							{{$record->discountCard->full_name}}
						</div>
						@endif	
						@if ($record->details)
								@foreach($record->details as $detail)
									<?php
											$total_discount	+=$detail->discount_amount;
											$total_sum 	+=$detail->sum;
									?>
								@endforeach
						@endif
						<!-- @if ($record->details)
							<div class="uk-margin-left">
								@foreach($record->details as $detail)
									@if ($detail->serial)
										{{ $detail->serial->barcode }} ({{ $detail->serial->vendorcode }})
									@endif
									@if ($detail->product)
										{{ $detail->product->barcode }} ({{ $detail->product->vendorcode }})
									@endif
									<br>
								@endforeach
							</div> -->
						<!-- @endif -->
					</div>
				</td>
				<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 1 || $record->closed == 71) ? ($total_sum) : 0), 2) }}</td>
				<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 66) ? $record->amount : 0), 2) }}</td>
			</tr>
							
			<tr class="@if($record->closed == 66) uk-text-warning @endif">
					<td class="uk-padding-remove-vertical uk-text-right">Discount:</td>
					<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 1 || $record->closed == 71) ? $total_discount : 0), 2) }}</td>
					<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 66) ? $total_discount : 0), 2) }}</td>
				</tr>
				<tr class="@if($record->closed == 66) uk-text-warning @endif">
					<td class="uk-padding-remove-vertical uk-text-right">Total:</td>
					<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 1 || $record->closed == 71) ? $record->amount : 0), 2) }}</td>
				<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 66) ? $record->amount : 0), 2) }}</td>
				</tr>
				<tr><td></td></tr>
				<?php 
					$total_discount=0;
					$payment_count=1;
					$total_sum=0;
				?>
			@foreach ($record->paymentTypes as $payment)
				<?php
					if (empty($total_payments[$payment->paymentType->name]))
						$total_payments[$payment->paymentType->name] = 0;
					$total_payments[$payment->paymentType->name] += (($record->closed == 1 || $record->closed == 71) ? $payment->amount : 0);

					if ($payment->paymentType->code != 11) {
						$total += (($record->closed == 1 || $record->closed == 71) ? $payment->amount : 0);
						$total_error += (($record->closed == 66) ? $payment->amount : 0);

						$user_total += (($record->closed == 1 || $record->closed == 71) ? $payment->amount : 0);
						$user_total_error += (($record->closed == 66) ? $payment->amount : 0);
					}
				?>
				
				<tr class="@if($record->closed == 66) uk-text-warning @endif">
					<td class="uk-padding-remove-vertical uk-text-right">P{{$payment_count}} {{ $payment->paymentType->name }}:</td>
					<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 1 || $record->closed == 71) ? $payment->amount : 0), 2) }}</td>
					<td class="uk-text-right uk-padding-remove-vertical">{{ number_format((($record->closed == 66) ? $payment->amount : 0), 2) }}</td>
				</tr>
				<?php $payment_count+=1 ?>
			@endforeach
			<?php
				$user_name = $record->user->name;
			?>
		<tr><td colspan="3"></td></tr>
		@endforeach
			<tr class="uk-text-bold">
				<td class="uk-text-right">Total:</td>
				<td class="uk-text-right">{{ number_format($user_total, 2) }}</td>
				<td class="uk-text-right">{{ number_format($user_total_error, 2) }}</td>
			</tr>
	</tbody>
	<tr><td colspan="3"><hr></td></tr>
	@foreach ($total_payments as $key => $total_payment)
		<tr class="uk-text-bold">
			<td class="uk-padding-remove-vertical uk-text-right">{{ $key }}:</td>
			<td class="uk-padding-remove-vertical uk-text-right">{{ number_format($total_payment, 2) }}</td>
			<td class="uk-padding-remove-vertical uk-text-right"></td>
		</tr>
	@endforeach
	<tr class="uk-text-bold">
		<td class="uk-text-right">Total:</td>
		<td class="uk-text-right">{{ number_format($total, 2) }}</td>
		<td class="uk-text-right">{{ number_format($total_error, 2) }}</td>
	</tr>
	</table>
	<br>
   <br>
   <br>
   <br>
   <br>
@endsection
