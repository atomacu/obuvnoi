@extends('layouts.report')

@section('content')
	<div class="uk-text-center"><img src="{{ $companyInfo['shop_logo'] }}" width="400"></div>
	<div class="uk-text-center uk-margin-small">{!! $companyInfo['shop_address'] !!}</div>
	<h3 class="uk-text-center uk-margin-remove">Recept</h3>
	
	<hr class="uk-margin-remove-bottom">
	<div class="uk-text-right uk-text-small">elaborat: {{ date('d.m.y H:i:s') }}</div>
    
<?php
$total=0;
?>
   	
    <table  class="uk-table uk-table-hover" border="1">
    <tr>
        <td  colspan="5">Name: {{$records['name']}}</td>
    </tr>
    <tr>
        <td colspan="2">Phone:{{$records['phone']}}</td>
        <td colspan="3">Birthday:{{$records['birthday_at']}}</td>
    </tr>
    <tr>
        <td rowspan='2' style="font-size:60px">D</td> 
        <td align="center">R </td> 
        <td align="center" style="font-size:25px">{{$records['Dr_dioptria']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Dr_astig']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Dr_gradus']}} </td> 
    </tr>
    <tr>
        <td align="center">L</td> 
        <td align="center"  style="font-size:25px">{{$records['Dl_dioptria']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Dl_astig']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Dl_gradus']}} </td> 
    </tr>
    <tr>
        <td rowspan='2' style="font-size:60px">R</td> 
        <td align="center">R </td> 
        <td align="center"  style="font-size:25px">{{$records['Rr_dioptria']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Rr_astig']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Rr_gradus']}} </td> 
    </tr>
    <tr>
    <td align="center">L </td> 
        <td align="center"  style="font-size:25px">{{$records['Rl_dioptria']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Rl_astig']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['Rl_gradus']}} </td> 
    </tr>
    <tr>
         <td colspan="2">PD</td> 
        <td align="center"  style="font-size:25px">{{$records['PD_r']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['PD_l']}} </td> 
        <td align="center"  style="font-size:25px">{{$records['PD_gradus']}} </td> 
    </tr>
    <tr>
        <td colspan="5">Remarks: {{$records['comment']}}</td>
    </tr>
    </table>   
    	
    <table  class="uk-table uk-table-hover" border="1">
    <tr>    
        <td><b>Name</b></td> 
        <td><b>Price</b></td> 
        <td><b>Qty</b></td> 
        <td><b>Dicount</b></td> 
        <td><b>Total</b></td> 
        
    </tr>
    @foreach ($orderDetails as $orderDetail)  
        <?php
            $total=$total+$orderDetail['total'];
        ?>   
        <tr>    
            <td>{{$orderDetail['product']['name']}}</td> 
            <td align="center">{{$orderDetail['price']}}</td> 
            <td align="center">{{$orderDetail['quantity']}}</td> 
            <td align="center">{{$orderDetail['discount_amount']}}</td> 
            <td align="center">{{$orderDetail['total']}}</td> 
            
        </tr>
    @endforeach
    <tr>    
        <td colspan="4" >Total</td> 
        <td  align="center">{{$total}}</td> 
        
        
    </tr>
   </table>   
@endsection