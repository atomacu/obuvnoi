@extends('layouts.report')

@section('content')
	<div class="uk-text-center"><img src="{{ $companyInfo['shop_logo'] }}" width="150"></div>
	<div class="uk-text-center uk-margin-small">{!! $companyInfo['shop_address'] !!}</div>
	<h3 class="uk-text-center uk-margin-remove">Clients</h3>
	<div class="uk-text-center">pentru perioada de la {{ $dateFrom }}<br>la {{ $dateTo }}</div>
	<hr class="uk-margin-remove-bottom">
	<div class="uk-text-right uk-text-small">elaborat: {{ date('d.m.y H:i:s') }}</div>

    <table  class="uk-table uk-table-small uk-table-hover">
 @foreach ($clients as $client) 
    <tr><td colspan="4" class="uk-padding-remove-bottom">-------------------------------------------------------------------------------------------------------------</td></tr>
    <tr><td colspan="4" class="uk-text-center" ><b>{{$client["Owner"]}}</b></td>  </tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom" ><b>Birthday:</b> {{$client["Birthday"]}}</td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom" ><b>TelMobil:</b> {{$client["TelMobil"]}}</td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom "><b>Adress:</b> {{$client["Adress"]}}</td></tr>
    <tr><td colspan="4" > </td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom"><b>Begin_DT:</b> {{$client["BeginDebt"]}}</td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom"><b>DT_Out:</b> {{$client["DebtOut"]}}</td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom"><b>DT_In:</b> {{$client["DebtIn"]}}</td></tr>
    <tr><td colspan="4" class="uk-text-left uk-padding-remove-vertical uk-padding-remove-bottom"><b>End_DT:</b> {{$client["EndDebt"]}}</td></tr>
    
   @endforeach
   </table>   
   <br>
   <br>
   <br>
   <br>
   <br>
@endsection