@extends('layouts.report')

@section('content')
	<div class="uk-text-center"><img src="{{ $companyInfo['shop_logo'] }}" width="150"></div>
	<div class="uk-text-center uk-margin-small">{!! $companyInfo['shop_address'] !!}</div>
	<h3 class="uk-text-center uk-margin-remove">Raport ZI</h3>
	<div class="uk-text-center">pentru perioada de la {{ $dateFrom }}<br>la {{ $dateTo }}</div>
	<hr class="uk-margin-remove-bottom">
	<div class="uk-text-right uk-text-small">elaborat: {{ date('d.m.y H:i:s') }}</div>
	
	
	<table class="uk-table uk-table-small uk-table-hover" >
    <tbody>
		<?php
			$section_id = 0;
			$total_section_discount = 0;
			$payments = [];
			$total_section = 0;
			$paymentsTotal = [];
			$total = 0;
			$total_discount =0;
		?>
	@foreach($sections as $section)
	<?php $results=array(); ?>
	<tr><td colspan="4"><b>{{$section->name}}</b></td></tr>
		@foreach($records as $index => $record)
			
			@if ($section->id === $record->section_id)
					<?php 
							foreach ($record->details as $detail) {
											$id = $detail->product_id;
											
				
											if (isset($results[$id])) {
														$results[$id][0]['quantity']=$results[$id][0]['quantity']+$detail->quantity ;
														$results[$id][0]['sum']+=$detail->sum ;
														$results[$id][0]['price']=	$results[$id][0]['sum']/	$results[$id][0]['quantity'] ;
											} else {
															$results[$id] = array($detail);
											}      
							}			
						
					?>
		

			<?php
		
				foreach ($record->paymentTypes as $payment) {
					if (empty($payments[$payment->paymentType->name])) {
						$payments[$payment->paymentType->name]['amount'] = 0;
						$payments[$payment->paymentType->name]['code'] = $payment->paymentType->code;
					}
					$payments[$payment->paymentType->name]['amount'] += $payment->amount;

					if (empty($paymentsTotal[$payment->paymentType->name])) {
						$paymentsTotal[$payment->paymentType->name]['amount'] = 0;
						$paymentsTotal[$payment->paymentType->name]['code'] = $payment->paymentType->code;
					}
					$paymentsTotal[$payment->paymentType->name]['amount'] += $payment->amount;

					if ($payment->paymentType->code != 11)
						$total += $payment->amount;
				}
				
			
			?>
			@endif
		@endforeach
		<?php 
	
	$prices = array();
	foreach (	$results as $key => $result)
	{ 		
		array_push($prices, $result[0]);
			} 
			array_multisort(array_column($prices, 'sum'), SORT_DESC, $prices);
		?> 
		<?php 	foreach (	$prices as $price){	?>			
								<tr>
							<td class="uk-text-left uk-padding-remove-bottom">
							{{$price['product']['name']}}
							</td>
							<td class="uk-text-center uk-padding-remove-bottom">{{$price['product']['price']}}</td>
							<td class="uk-text-center uk-padding-remove-bottom">{{ $price['quantity'] }}</td>
							<td class="uk-text-right uk-padding-remove-bottom">
								{{ number_format($price['sum'], 2) }}
							</td>
						</tr>
						<tr>
							<td class="uk-text-left uk-padding-remove-bottom">    {{$price['product']['barcode']}}</td>
						</tr>
					<?php	$total_section_discount += $price['discount_amount'];	?>
					<?php	}
						$total_discount+=	$total_section_discount	?>

							@foreach ($payments as $key => $value)
											<?php if ($value['code'] != 11) $total_section += $value['amount'];	?>
										@endforeach
									<tr><td colspan="4"></td></tr>
									<tr>
											<td colspan="2" class="uk-padding-remove-vertical uk-text-right">{{$section->name}} - Subtotal:</td>
											<td colspan="2" class="uk-text-right uk-padding-remove-vertical">{{ number_format($total_section+$total_section_discount, 2) }}</td>
										</tr>
										<tr>
											<td colspan="2" class="uk-padding-remove-vertical uk-text-right">{{$section->name}} - Discount:</td>
											<td colspan="2" class="uk-text-right uk-padding-remove-vertical">{{ number_format($total_section_discount, 2) }}</td>
										</tr>
									
										<tr class="uk-text-bold">
											<td colspan="2" class="uk-padding-remove-vertical uk-text-right">{{$section->name}} - Total:</td>
											<td colspan="2" class="uk-text-right uk-padding-remove-vertical">{{ number_format($total_section, 2) }}</td>
										</tr>
										<tr><td colspan="4"></td></tr>
										@foreach ($payments as $key => $value)
											<tr @if($value['code'] == 11) class="uk-text-bold" @endif>
												<td colspan="2" class="uk-padding-remove-vertical uk-text-right">{{$section->name}} - Total {{ $key }}:</td>
												<td colspan="2" class="uk-text-right uk-padding-remove-vertical">{{ number_format($value['amount'], 2) }}</td>
											</tr>
										
										@endforeach
				<?php 
					$total_section_discount = 0;	
					$total_section = 0;
					$payments = [];
				?>
								</tbody>
		@endforeach
			
	<tr><td colspan="4"><hr></td></tr>
	<tr class="uk-text-bold">
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right">Subtotal pe sectii:</td>
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right uk-text-bottom">{{ number_format($total_discount +$total, 2) }}</td>
	</tr>
	<tr class="uk-text-bold">
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right">Discount pe sectii:</td>
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right uk-text-bottom">{{ number_format($total_discount, 2) }}</td>
	</tr>
	<tr class="uk-text-bold">
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right">Total pe sectii:</td>
		<td colspan="2" class="uk-padding-remove-vertical uk-text-right uk-text-bottom">{{ number_format($total, 2) }}</td>
	</tr>
	<tr><td colspan="4"></td></tr>
	@foreach ($paymentsTotal as $key => $value)
		<tr class="uk-text-bold">
			<td colspan="2" class="uk-padding-remove-vertical uk-text-right">Total pe sectii {{ $key }}:</td>
			<td colspan="2" class="uk-text-right uk-padding-remove-vertical uk-text-bottom">{{ number_format($value['amount'], 2) }}</td>
		</tr>
	@endforeach

	</table>
	 <br>
   <br>
   <br>
   <br>
   <br>
@endsection
