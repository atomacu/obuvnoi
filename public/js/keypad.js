var sizeEtalon = (window.innerWidth > 350) ? 350 : window.innerWidth;
var objValue, titleKeyPad;
var popupOpened = false;
var keyPadOptions = {
    hide: true,
    callback: null
}

var keyPadEditor = {
        rows: [
            { 
                view: 'toolbar', cols: [
                    { view: 'label', label: '' },
                    { view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', css: 'text-center' }
                ]
            },
            {
                view: 'form',
                elementsConfig: {css: 'keypad'},
                elements: [
                    {
                        rows: [
                            {
                                cols: [{
                                    view: 'search',
                                    css: 'inputIcon',
                                    readonly: false,
                                    icon: 'search fa-angle-double-left',
                                    name: 'keypad_value',
                                    height: 60,
                                    type: 'text',
                                    required: true,
                                    on: {
                                        onSearchIconClick: function (e) {
                                            var valOld = $$(this).getValue();
                                            var valNew = valOld.substr(0, valOld.length - 1);
                                            $$(this).setValue(valNew);
                                        },
                                        onAfterRender: function () {
                                            $$(this).focus();
                                        }
                                    }
                                }]
                            },
                            {
                                    rows: [
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                { view: 'button', value: '7', click: add_value },
                                                { view: 'button', value: '8', click: add_value },
                                                { view: 'button', value: '9', click: add_value }
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                { view: 'button', value: '4', click: add_value },
                                                { view: 'button', value: '5', click: add_value },
                                                { view: 'button', value: '6', click: add_value }
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                { view: 'button', value: '1', click: add_value },
                                                { view: 'button', value: '2', click: add_value },
                                                { view: 'button', value: '3', click: add_value }
                                            ]
                                        },
                                        {
                                            height: (sizeEtalon / 4),
                                            cols: [
                                                { view: 'button', value: 'C', click: clear_value },
                                                { view: 'button', value: '0', click: add_value },
                                                { view: 'button', value: 'OK' }
                                            ]
                                        },
                                    ]
                            }
                        ],
                    }
                ]
            }
        ]
    };

////////////////////////////////
var keyPad = {
        rows: [
            { 
                view: 'toolbar', cols: [
                    { view: 'label', id: 'titleKeyPad', label: '' },
                    { view: 'button', type: 'icon', icon: 'times', width: 50, align: 'right', css: 'text-center', click: closeKeyPad }
                ]
            },
            {
                view: 'form',
                elementsConfig: {css: 'keypad'},
                elements: [
                    {
                        rows: [
                            {
                                cols: [{
                                    view: 'search',
                                    readonly: false,
                                    icon: 'search fa-angle-double-left',
                                    name: 'keypad_value',
                                    width: 360,
                                    height: 60,
                                    type: 'text',
                                    required: true,
                                    on: {
                                        onSearchIconClick: function(e) {
                                            var valOld = $$(this).getValue();
                                            var valNew = valOld.substr(0, valOld.length - 1);
                                            $$(this).setValue(valNew);
                                        },
                                        onAfterRender: function () {
                                            $$(this).focus();
                                        },
                                        onEnter: function(e) {
                                            okKeyPad(null, null, this);
                                        }
                                    }
                                }]
                            },
                            {
                                rows: [
                                    {
                                        height: (sizeEtalon / 4),
                                        cols: [
                                            { view: 'button', value: '7', click: add_value },
                                            { view: 'button', value: '8', click: add_value },
                                            { view: 'button', value: '9', click: add_value }
                                        ]
                                    },
                                    {
                                        height: (sizeEtalon / 4),
                                        cols: [
                                            { view: 'button', value: '4', click: add_value },
                                            { view: 'button', value: '5', click: add_value },
                                            { view: 'button', value: '6', click: add_value }
                                        ]
                                    },
                                    {
                                        height: (sizeEtalon / 4),
                                        cols: [
                                            { view: 'button', value: '1', click: add_value },
                                            { view: 'button', value: '2', click: add_value },
                                            { view: 'button', value: '3', click: add_value }
                                        ]
                                    },
                                    {
                                        height: (sizeEtalon / 4),
                                        cols: [
                                            { view: 'button', value: 'C', click: clear_value },
                                            { view: 'button', value: '0', click: add_value },
                                            { view: 'button', value: 'OK', click: okKeyPad }
                                        ]
                                    },
                                ]
                            }
                        ],
                    }
                ]
            }
        ]
    };
//////////////////////////////////////////////////
var keypadPopup = {
    view: 'popup',
    id: 'keypadPopup',
    padding: 0,
    modal: true,
    autofocus: true,
    body: keyPad,
    on: {
        onShow: function() {
            var that = this;
            popupOpened = true;
            webix.delay(function() {
                let lblTitle = that.getChildViews()[0].getChildViews()[0].getChildViews()[0];
                lblTitle.setValue(titleKeyPad);
                let formInput = that.getChildViews()[0].getChildViews()[1].getChildViews()[0].getChildViews()[0].getChildViews()[0];
                
                if (objValue) {
                    let value = objValue.getValue();
                    if (value != '') {
                        //value = parseFloat(value);
                        if (isFloat(value)) {
                            value = value.toFixed(2);
                        }
                        formInput.setValue(value);
                    }
                }
                formInput.setValue('');
                formInput.focus();
            });
        },
        onHide: function() {
            popupOpened = false;
        }
    }
};

webix.editors.$popup.keypadPopupEditor = {
    view: 'popup',
    padding: 0,
    modal: true,
    autofocus: true,
    body: keyPadEditor
};

var keypadOldValue, keypadIsNewValue = true;
webix.editors.keypadPopupEditor = webix.extend({
    focus: function() {},
    popupType: 'keypadPopupEditor',
    //destroy: function() { if (popupKeyPad) popupKeyPad.destructor(); },
    setValue: function(value, obj) {
    	keypadIsNewValue = true;
        keypadOldValue = value;
        let formView = this.getInputNode();
        let obj_value = formView.getChildViews()[1].getChildViews()[0].getChildViews()[0].getChildViews()[0];
        let lblTitle = formView.getChildViews()[0].getChildViews()[0];
        lblTitle.setValue(this.config.editorTitle);
        
        obj_value.setValue(value);
        obj_value.refresh();
        
        popupOpened = true;
        this.getPopup().show(this.node);
    },
    getValue: function() {
        var value_out = '';
        let formView = this.getInputNode();
        let obj_value = formView.getChildViews()[1].getChildViews()[0].getChildViews()[0].getChildViews()[0];
        value_out = obj_value.getValue();

        return value_out;
    },
    popupInit: function(popup) {
        let formView = popup.getChildViews()[0];
        let btnOk = formView.getChildViews()[1].getChildViews()[0].getChildViews()[1].getChildViews()[3].getChildViews()[2];
        let btnClose = formView.getChildViews()[0].getChildViews()[1];
        
        btnOk.attachEvent('onItemClick', function() {
            webix.callEvent('onEditEnd', [this.getValue()]);
            popupOpened = false;
            popup.hide(this.node);
        });

        var that = this;
        btnClose.attachEvent('onItemClick', function() {
            that.setValue(keypadOldValue)
            webix.callEvent('onEditStop');
            popupOpened = false;
            popup.hide(this.node);
        });
    }
}, webix.editors.popup);

var popupKeyPad;
webix.ready(function() {
    popupKeyPad = webix.ui(keypadPopup);
});

function add_value(id, e) {
    var frm = this.getFormView();
    var input = frm.elements.keypad_value;

    if (keypadIsNewValue) clear_value(id, e, input);
    var resultString = input.getValue() + this.data.value;
    input.setValue(resultString);
    input.focus();
    keypadIsNewValue = false;
};

function clear_value(id, e, input) {
    if (!input) {
        var frm = this.getFormView();
        input = frm.elements.keypad_value;
    }
	input.setValue('');
}

function okKeyPad(id, e, obj) {
    var obj = obj || this;
    var frm = obj.getFormView();
    var input = frm.elements.keypad_value;
    var value = input.getValue();
    input.setValue('');

    if (objValue) objValue.setValue(value);
    if (keyPadOptions.callback) keyPadOptions.callback(value);
    if (keyPadOptions.hide) {
        obj.getTopParentView().hide();
    }
};

function closeKeyPad() {
    this.getTopParentView().hide();
}

function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}
