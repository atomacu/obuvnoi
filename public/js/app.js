webix.Date.startOnMonday = true;
webix.i18n.timeFormat = '%H:%i';
webix.i18n.dateFormat = '%d.%m.%Y';
webix.i18n.fullDateFormat = '%d.%m.%Y %H:%i';

var priceFormat = webix.Number.numToStr({
    groupSize: 3,
    decimalSize: 2,
    groupDelimiter: '',
    decimalDelimiter: '.'
});
webix.i18n.priceFormat = function (val) {
    return priceFormat(val);
};

webix.i18n.setLocale('ru-RU');
webix.ui.fullScreen();

var accountInfo, globalSettings;
var ttl_session_time = 0;
var idleTime = 0;
var idleInterval;
var windowOpened = false;

// webix.attachEvent('onAjaxError',
//     function(xhr) {
//         if (xhr.status == 500) {
//             webix.message({type: 'error', text: 'Ошибка программы!'});
//         } else if (xhr.status != 401 && xhr.status != 404 && xhr.status != 422) {
//             webix.message({type: 'error', text: 'Ошибка сети!'});
//         }
//     }
// );

webix.extend(webix.ui.datatable, {
    myAdjustRowHeight: function (id, silent, rId) {

        function processRow(that, obj) {
            var h = [that._settings.rowHeight], config;
            for (var i = 0; i < id.length; i++) {
                config = that.getColumnConfig(id[i]);
                d.style.width = config.width + "px";
                d.innerHTML = that._getValue(obj, config, 0);
                h.push(d.scrollHeight);
            }
            obj.$height = Math.max.apply(null, h);
        }

        if (typeof id == 'string') id = [id];

        var d = webix.html.create("div", {"class": "webix_table_cell webix_measure_size webix_cell"}, "");
        d.style.cssText = "height:1px; visibility:hidden; position:absolute; top:0px; left:0px; overflow:hidden;";
        this.$view.appendChild(d);

        if (rId) {
            var obj = this.getItem(rId);
            processRow(this, obj);
        } else {
            this.data.each(function (obj) {
                processRow(this, obj);
            }, this);
        }

        d = webix.html.remove(d);
        if (!silent)
            this.refresh();
    }
});

function onLogin(pin_code) {
    show_progress_icon('keyPadAuth');
    webix.ajax().post('/login', {pin_code: pin_code})
        .then(function (result) {
            hide_progress_icon('keyPadAuth');
            var response = result.json();
            webix.storage.session.put('user', {
                token: response.token,
                data: response.user
            });
            webix.storage.session.put('user_id', response.user.id);
            webix.storage.cookie.put('user_id', parseInt(response.user.id));
            webix.message({type: 'success', text: 'Авторизация прошла успешно.'});
            //location.reload(true);
            location.href = '/cashbox';
            //getAccount();
        })
        .fail(function (xhr) {
            hide_progress_icon('keyPadAuth');
            var response = JSON.parse(xhr.response);
            webix.message({type: 'error', text: response.message});
        });
}

function onLogout() {
    show_progress_icon('mainview');
    webix.ajax().post('/logout', {})
        .then(function (result) {
            var response = result.json();
            webix.message({type: 'success', text: response.message});
            webix.send('/', null, 'get');
            hide_progress_icon('mainview');
        })
        .fail(function (xhr) {
            hide_progress_icon('mainview');
        });
}

function checkAutoLogoff() {
    var countTime;
    if (accountInfo && ttl_session_time > 0) {
        idleTime++;
        if (idleTime > ttl_session_time) {
            countTime = ttl_session_time - idleTime;
            if (countTime <= 0) {
                onLogout();
            }
        }
    } else idleTime = 0;
}

function getAccount() {
    webix.ajax().get('/account')
        .then(function (result) {
            let res = result.json();
            accountInfo = res;

            $$('btnLogout').define('label', accountInfo.name);
            $$('btnLogout').refresh();
            $$('btnLogout').resize();

            if (accountInfo.permission == 2) {
                $$('btnSettings').show();
            }
            getSettings();
        })
        .fail(function (xhr) {
        });
}

function getAdminAccount() {
    webix.ajax().get('/admin/account', {})
        .then(function (result) {
            let res = result.json();

            //document.querySelector('.main-area').classList.remove('hide');
            //let user_info = document.querySelector('.user-info .user-name');
            //user_info.innerHTML = res.name;
        })
        .fail(function (xhr) {
        });
}

function custom_checkbox(obj, common, value) {
    if (value) {
        return '<div class="webix_table_checkbox checked"><i class="fa fa-check-square-o fa-lg" aria-hidden="true"></i></div>';
    } else {
        return '<div class="webix_table_checkbox unchecked"><i class="fa fa-square-o fa-lg" aria-hidden="true"></i></div>';
    }
}

var remove = function (id, obj, callback) {
    var item = obj.getItem(id.row);
    webix.confirm({
        text: 'Удалить ' + item.product_name + ' ' + item.barcode + ' ?', type: 'confirm-warning', ok: 'Да', cancel: 'Нет', callback: function (result) {
            if (result) {
                obj.remove(id);
                obj.refreshColumns();
            }
            if (callback)
                return callback(result);
        }
    });
    //return false;
};

function show_progress_icon(container) {
    $$(container).disable();
    $$(container).showProgress({type: 'icon'});
}

function hide_progress_icon(container) {
    $$(container).hideProgress();
    $$(container).enable();
}

function equals(a, b) {
    a = a.toString().toLowerCase();
    return a.indexOf(b) !== -1;
}

function getSettings() {
    webix.ajax().get('/get-settings')
        .then(function (result) {
            let res = result.json();
            globalSettings = res;
            if (res.last_shift_at) lastShift_at = res.last_shift_at;
            // printerReceipt = res.printer_receipt;
            // printerFiscal = res.printer_fiscal;

            if (globalSettings.settings) {
                if (globalSettings.settings.logoff_timer)
                    ttl_session_time = parseInt(globalSettings.settings.logoff_timer);
            }

            $$('lblShop').define('label', res.shop_name);
            $$('lblShop').refresh();
            $$('lblShop').resize();

            // if (!printerReceipt) {
            //     webix.alert({
            //         type: 'alert-error',
            //         text: 'Внимание!<br>У Вас не настроен <strong>Receipt</strong> принтер!'
            //     });
            // }
            // if (!printerFiscal) {
            //     webix.alert({
            //         type: 'alert-error',
            //         text: 'Внимание!<br>У Вас не настроен <strong>Fiscal</strong> принтер!'
            //     });
            // }

        })
        .fail(function (xhr) {
        });
}

function getUnsyncedOrders() {
    webix.ajax().get('/get-unsynced-orders')
        .then(function (result) {
            try {
                let res = result.json();
            } catch (err) {
            }
            if (res) {
                $$('unsynced_icon').define('badge', res);
                $$('unsynced_icon').refresh();
            } else {
                $$('unsynced_icon').define('badge', '');
                $$('unsynced_icon').refresh();
            }
        })
        .fail(function (xhr) {
        });
}

idleInterval = setInterval(checkAutoLogoff, 1000);
window.addEventListener('mousemove', function () {
    idleTime = 0;
});
window.addEventListener('keydown', function () {
    idleTime = 0;
    try {
        if (!popupOpened && !windowOpened)
            $$('search').focus();
    } catch (e) {
        console.log(e);
    }
});
window.addEventListener('touchstart', function () {
    idleTime = 0;
});
