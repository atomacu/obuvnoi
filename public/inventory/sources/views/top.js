import {JetView, plugins} from "webix-jet";

export default class TopView extends JetView {
	config() {
		var $app = this.app;

		var header = {
			view: "toolbar",
			cols: [
				{ view: 'label', label: this.app.config.name, width: 200 },
				{},
				{ view: 'label', id: 'lblShop', label: '', align: 'center' },
				{},
				{ view: 'button', label: 'в приложение', href: '/cashbox', autowidth: true, click: function() { location.href = base_url + this.config.href; } },
			]

		};

		var sections = new webix.DataCollection({
		    scheme: {
		  		$init: function(obj) { obj.value = obj.name; }
		    },
		    url: base_url + '/data/sections'
		});

		var section = {
			view: "richselect", id: "selSection", placeholder: "Выберите секцию", options: base_url + '/data/sections',
		    /*scheme: {
		  		$init: function(obj) { obj.value = obj.name; }
		    },*/
			on: {
				onChange: function(newv, oldv) {
					webix.storage.session.put("section_id", newv);
					webix.storage.cookie.put("section_id", parseInt(newv));
					section_id = (newv == 999) ? "" : newv;
					$app.show("/top/start");
				},
				onAfterRender: webix.once(function() {
					sections.add({id: 999, name: "Все секции"});
					this.getPopup().getList().sync(sections);
					section_id = webix.storage.session.get("section_id");
					if (section_id) {
						this.setValue(section_id);
					}
				})
			},
		};

		var menu = {
			view: "sidebar", id: "sidebar:menu",
			width: 180,
			data: [
				{ value: "Dashboard", id: "start", icon: "wxi-columns" },
				{ value: "Подтверждения", icon: "wxi-check", data: [
						{ value: "Приход", id: "documents?type=coming", icon: "wxi-download" },
						{ value: "Перемещения", id: "documents?type=move", icon: "wxi-sync" },
					]
				},
				{ value: "Отчет по остаткам", id: "documents?type=invent", icon: "wxi-file" }
			],
		};

		var ui = {
			rows: [
				header,
				{
					type: "clean", paddingX: 3, css: "app_layout", 
					cols: [
						{ paddingX: 3, paddingY: 5, rows: [section, menu]},
						{ type: "wide", paddingY: 5, paddingX: 3, rows: [
							{ $subview: true } 
						]}
					]
				}
			]
		};

		return ui;
	}

	init() {
		this.use(plugins.Menu, "sidebar:menu");
	}
}