import {JetView} from "webix-jet";
import {getDocuments, getProducts, removeDocument, saveRecord, Sections} from "models/records";
var _ = require('lodash');

function show_progress_icon(container) {
    $$(container).disable();
    $$(container).showProgress({ type: 'icon' });
}
function hide_progress_icon(container) {
    $$(container).hideProgress();
    $$(container).enable();
}

export default class DataView extends JetView{
	config() {

        webix.ui.datafilter.qtyColumn = webix.extend({
            refresh: function(master, node, value) {
                var result_total = 0;
                var result_process = 0;
                var result_scanned = 0;
                master.mapCells(null, null, null, null, function(value, row, col, row_ind, col_ind) {
                	switch (col) {
                		case "scanned":
                			if (!isNaN(value))
                        		result_scanned += parseInt(value);
                			break;
                		/*case "quantity_res":
                			if (!isNaN(value))
                        		result_total += value;
                			break;
                		case "quantity":
                			if (!isNaN(value))
                        		result_process += value;
                			break;*/
                		default:
                			break;
                	}
                    return value;
                });
                node.firstChild.innerHTML = " " + result_scanned + "/" + document_selected.products.length + " ";
            }
        }, webix.ui.datafilter.summColumn);

        webix.ui.datafilter.inputBarcode = {
            getInputNode: function(node){ return node.firstChild?node.firstChild.firstChild : { value: null}; },
            getValue: function(node){ return this.getInputNode(node).value;  },
            setValue: function(node, value){ this.getInputNode(node).value = value; },
            refresh: function(master, node, column){
                node.onchange = () => {
                	var in_obj = this.getInputNode(node);
                	var exists = false;

                	if (document_selected.local) {
						if (in_obj == '')
							return false;

						webix.ajax().post(base_url + "/data/serials", {barcode: in_obj.value, get_type: "all"})
							.then(function (result) {
								var resData = result.json()[0];
								master.eachRow( 
								    function (row) {
								    	var item = master.getItem(row);
								        if (resData.barcode == item.barcode) {
								        	webix.message({type: 'error', text: 'Такой продукт уже добавлен!'});
								        	exists = true;
								        	return false;
								        }
								    }
								)
								if (!exists) {
									var data = { document_id: document_selected.id, scanned: 1, vendorcode: resData.vendorcode, barcode: resData.barcode, quantity_res: 0, quantity: 1 };
									if (resData.product) {
										data.serial = resData;
									} else {
										data.product = resData;
									}
									let id = master.add(data, 0);
									master.refreshColumns();

									let item = master.getItem(id);
									let node = master.getItemNode({ row: id, column: "productname" });
								    master.select(id);
								    master.showItem(id);
									showProductDetails(master, item);
								}
							})
							.fail(function (xhr) {
								var response = JSON.parse(xhr.response);
								webix.message({type: 'error', text: response.message});
								return false;
							});
						in_obj.value = "";
                	} else {
		                master.mapCells(null, "barcode", null, 1, function(value, row, col, row_ind, col_ind) {
		                	if (value == in_obj.value) {
								var record = master.getItem(row);
								record["scanned"] = 1;
								record["quantity"] = 1;
								master.updateItem(row, record);
								in_obj.value = "";
								exists = true;
								if (record.product_id) {
									master.editCell(row, "quantity");
								}
								let node = master.getItemNode({ row: row, column: "productname" });
							    master.select(row);
							    master.showItem(row);
								showProductDetails(master, record);
		                	}
		                    return value;
		                });
		                if (!exists) {
		                	webix.message(in_obj.value + "<br>Продукт не найден в данном документе!", "error", 5000, "msg_not_found");
		                	in_obj.value = "";
		                }
	            	}
                };
                node.onclick = webix.html.preventEvent;
            },
            render: function(a, b){
                return "<input type='text' style='width:100%; height:25px; font-size: 1rem;'; placeholder='Баркод' id=" + b.columnId + " autofocus>";
            }
        };

		function custom_checkbox(obj, common, value) {
		    if (value)
		        return "<div class='webix_checkbox checked'> <span class='webix_icon wxi-checkbox-marked'></span> </div>";
		    else
		        return "<div class='webix_checkbox unchecked'> <span class='webix_icon wxi-checkbox-blank'></span> </div>";
		};
		function custom_check(obj, common, value) {
		    if (value)
		        return "<div class='webix_checkbox checked'> <span class='webix_icon wxi-check'></span> </div>";
		    else
		        return "";
		};

		webix.ui({
		    view: "popup", id: "productDetails",
		    position: "center",
		    autoheight: true,
		    autowidth: true,
		    body: {
		  		template: " ",
		  		autoheight: true,
                autowidth: true
		    }
		});

		function showProductDetails(obj, item) {
			let product = item.product || null;
			let serial = item.serial || null;
			if (serial) {
				let details = "<div class='product-details'>";
				details += "<div><span>" + serial.product.name + " " + serial.vendorcode + " (" + serial.number + ")</span></div>";
				details += "<div><strong>Цена: <span>" + serial.price + "</span></strong></div>";
				details += "<div>Вес: <span>" + serial.details.weight + "</span></div>";
				if (serial.details.size) details += "<div>Размер: <span>" + serial.details.size + "</span></div>";
				if (serial.details.color) details += "<div>Цвет: <span>" + serial.details.color + "</span></div>";
				if (serial.details.metals && serial.details.metals.length > 0) {
					details += "<div>Металлы: ";
					serial.details.metals.forEach(function(val, i, arr) {
					    details += "<div class='sub-details'><span>" + val.name + "</span>, <span>проба: " + val.probe + "</span></div>";
					});
					details += "</div>";
				}
				if (serial.details.stones && serial.details.stones.length > 0) {
					details += "<div>Камни: ";
					serial.details.stones.forEach(function(val, i, arr) {
					    details += "<div class='sub-details'><span>" + val.name + "</span></div>";
					});
					details += "</div>";
				}
				details += "</div>";

				let $productDetails = $$('productDetails');
	       		$productDetails.getBody().define('template', details);
	       		$productDetails.getBody().refresh();
	       		$productDetails.adjust();
	       		$productDetails.resize();
				$productDetails.show();
			}
		}
		/*webix.editors.$popup = {
		    productDetails: {
		      view: "popup",
		      body: { 
		      	template: "popup"
		      }
		    },
		};
		webix.editors.productDetails = webix.extend({
		    focus: function() {},
		    popupType: 'productDetails',
		    setValue: function(value, obj) {
		    	this.getPopup().show(this.node);
		    },
		    getValue: function() {},
		    popupInit: function(popup) {}
		}, webix.editors.popup);*/

		return {
			id: "views",
			css: "webix_shadow_medium",
			cells: [
				{
					view: "datatable",
					id: "dtDocuments",
					autoConfig: true,
					select: "row",
					columns: [
						{ id: "localed", header: { text: '<span class="webix_icon wxi-file"></span><span class="text">создать</span>', css: "btn-new"}, width: 110, css: "text-right" },
						{ id: "number", header: "# документа", adjust: "header" },
						{ id: "section_name", header: "Секция", adjust: "data" },
						{ id: "date_at", header: {text: "Дата", css: 'text-center'}, width: 110, css: 'text-center' },
						{ id: "notes", header: "Коментарий", fillspace: true },
						{ id: "products_summa", header: {text: "Сумма", css: 'text-right'}, adjust: "data", css: 'text-right' },
						{ id: "products_qty", header: {text: "Кол. продуктов", css: 'text-center'}, adjust: "header", css: 'text-center' },
						//{ id: "synced", header: "Подтв.", adjust: "header", template: custom_check, css: 'text-center' },
					],
					scheme: {
						$init: function(obj) {
							obj.date_at = webix.i18n.dateFormatStr(new Date(obj.date_at));
							obj.section_name = obj.section.name;
							obj.products_qty = obj.products.length;
							obj.products_summa = getDocumentSum(obj.products);
							let local_content = "";
							if (obj.local) {
								//if (user && user.data.permission == 2) {
									local_content += " <span class='webix_icon wxi-close color-red btn-remove pointer'></span> ";
									//local_content += " <span class='webix_icon wxi-pencil color-green btn-edit pointer'></span> ";
								//}
								local_content += " <span class='webix_icon wxi-angle-double-right color-blue'></span> ";
							}
							obj.localed = local_content;
						}
					},
					on: {
						onItemClick: function(id) {
							if (id.column != "localed") {
								document_selected = this.getSelectedItem();
								doGetProducts(document_selected.id);
							}
						},
						onAfterLoad: function() {
							if (document_selected) {
								this.select(document_selected.id);
								document_selected = this.getSelectedItem();
								doGetProducts(document_selected.id);
							}
						}
					},
					onClick: {
						"btn-new": function() {
							doCreateDocument();
						},
						"btn-remove": function() {
							webix.confirm({title: "Удаление документа", text: "Вы уверены?", type: "confirm-warning",
								ok: "Да", cancel: "Нет",
								callback: (result) => {
									if (result) {
										let sel_item = this.getSelectedItem();
										removeDocument(sel_item.id);
										doGetDocuments(sel_item.type);
									}
								}
							});
						},
						"btn-edit": function() {
							
						},
					}
				},
				{
					view: "datatable",
					id: "dtProducts",
					autoConfig: true,
					select: "row",
					footer: true,
					prerender: true,
					editable: true,
					columns: [
						{ id: "scanned", header: { text: '<span class="webix_icon wxi-menu-left"></span><span class="text">назад</span>', css: "btn-back"}, template: custom_checkbox, readonly: true, css: "text-center", width: 90, footer: { text: "<div class='webix_el_button text-right'><button type='button' class='btn-confirm'>Подтвердить</button></div>", colspan: 6 } },
						{ id: "productname", header: "Продукт", fillspace: true },
						{ id: "vendorcode", header: "Арт.", adjust: "data" },
						{ id: "barcode", header: {text: "Баркод", content: "inputBarcode"}, adjust: "data" },
						{ id: "price", header: {text: "Цена", css: 'text-right'}, adjust: "data", css: 'text-right' },
						{ id: "quantity_res", header: {text: "Кол. Ост.", css: 'text-center'}, adjust: "header", css: 'text-center' },
						{ id: "quantity", header: {text: "Кол.", css: 'text-center'}, footer: { content: "qtyColumn", css: "text-center" }, width: 100, css: "text-center edit-flag", editor: 'keypadPopupEditor', editorTitle: 'Введите количество' },
					],
					scheme: {
						$init: function(obj) {
							obj.productname = (obj.product) ? obj.product.name : obj.serial.product.name;
							obj.vendorcode = (obj.product) ? obj.product.vendorcode : obj.serial.vendorcode;
							obj.barcode = (obj.product) ? obj.product.barcode : obj.serial.barcode;
							obj.price = (obj.product) ? parseFloat(obj.product.price).toFixed(2) : parseFloat(obj.serial.price).toFixed(2);
						}
					},
					on: {
						onBeforeEditStart: function(id) {
							var item = this.getSelectedItem();
							if (id.column == 'quantity' && (!item.scanned || item.serial)) {
								return false;
							}
							return true;
						},
						onItemClick: function (id) {
							if (id.column == "productname") {
								let item = this.getItem(id);
								let node = this.getItemNode(id);
								showProductDetails(this, item);
							}
						}
					},
					onClick: {
						"btn-back": function() {
							document_selected = null;
							$$("views").back();
						},
						"btn-confirm": function() {
							webix.confirm({
								text: 'Вы уверены?', ok: 'Да', cancel: 'Нет', type: 'confirm-warning',
								callback: function(result) {
									if (result) {
										doConfirmDocument(document_selected);
									}
								}
							});
						}
					},
					save: {
						updateFromResponse: true,
						url: saveRecord,
					}
				}
			]
		}

	}

	init(view) {
		//$$("dtDocuments").clearAll();
		//$$("dtDocuments").parse(getDocuments(this.getParam("type")));
		webix.extend($$("views"), webix.ProgressBar);
	}

	urlChange(view) {
		document_selected = null;
		$$("views").back();
		type_selected = this.getParam("type");
		doGetDocuments(type_selected);
	}
}

function doGetDocuments(type) {
	show_progress_icon("views");
	var $dtDocuments = $$("dtDocuments");
	$dtDocuments.clearAll();
	getDocuments(type, section_id).then((res) => {
		var resData = res.json();
		if (resData.status && resData.status == "error") {
			webix.message({type: 'error', text: resData.message});
		} else {
			$dtDocuments.parse(resData);
		}
		hide_progress_icon("views");
	}).fail(function (xhr) { webix.message({type: 'error', text: 'Ошибка программы!'}); hide_progress_icon("views"); });
}

function doGetProducts(document_id) {
	show_progress_icon("views");
	var $dtProducts = $$("dtProducts");
	$dtProducts.clearAll();
	getProducts(document_id).then((res) => {
		var resData = res.json();
		if (resData.status && resData.status == "error") {
			webix.message({type: 'error', text: resData.message});
		} else {
			$dtProducts.parse(resData);
			$dtProducts.show();
		}
		hide_progress_icon("views");
	}).fail(function (xhr) { webix.message({type: 'error', text: 'Ошибка программы!'}); hide_progress_icon("views"); });
}

function doCreateDocument() {
	let type_title = "";
	if (type_selected == "coming")
		type_title = "прихода";
	if (type_selected == "move")
		type_title = "перемещения";
	webix.ui({
	    view: 'window',
	    id: 'winDocument',
	    autowidth: true,
	    position: 'center',
	    modal: true,
	    head: 'Новый документ ' + type_title,
	    body: {
		    view: 'form',
		    id: 'frm_document',
		    elements: [
		        { 
		        	view: 'text', label: 'Номер документа:', name: 'number', labelWidth: 160, required: true, width: 300,
		        	popup: 'keypadPopup',
					on: {
						onItemClick: function(id, e) {
							objValue = $$(id);
							titleKeyPad = 'Введите номер документа';
					        keyPadOptions.callback = null;
						}
					}
		    	},
		    	{ view: 'richselect', label: 'Секция "Отправитель":', labelWidth: 175, name: 'from_section_id', options: Sections, required: (type_selected == "move"), hidden: !(type_selected == "move") },
		    	{ view: 'richselect', label: 'Секция "Получатель":', labelWidth: 175, name: 'section_id', options: Sections, required: true },
		        { view: 'datepicker', label: 'Дата:', name: 'date_at', labelWidth: 160, format: webix.Date.dateToStr("%d.%m.%Y"), required: true, stringResult: true },
		        { view: 'text', label: 'Коментарий:', name: 'notes', labelWidth: 110, required: true },
		        { margin: 5,
			        cols: [
						{ view: 'button', value: 'OK', type: 'form', click: function() {
							if ($$('frm_document').validate()) {
					  			let form_data = $$('frm_document').getValues();

								webix.ajax().post(base_url + '/inv/document/' + type_selected, form_data)
									.then(function (result) {
										let resData = result.json();
										document_selected = resData.response;
										$$('winDocument').close();
										doGetDocuments(type_selected);
									});
		                    } else {
								webix.message({ type: 'error', text: 'Заполните необходимые поля!' });
		                    }
						}},
						{ view: 'button', value: 'Отмена', click: function() {
		                        this.getTopParentView().close();
						}},
					]
				}
		    ]
	    }
	}).show();
	$$('frm_document').focus();
}

function doConfirmDocument(inv_document) {
	show_progress_icon("views");
	webix.ajax().post(base_url + "/inv/confirm-document", inv_document)
		.then(function (result) {
			$$("views").back();
			hide_progress_icon("views");
			document_selected = null;
			doGetDocuments(inv_document.type);
		})
		.fail(function (xhr) {
			var response = JSON.parse(xhr.response);
			webix.message({type: 'error', text: response.message});
			hide_progress_icon("views");
		});
}

function getDocumentSum(products) {
	let summa = _.sumBy(products, function(obj) {
		if (obj.product)
			return parseFloat(obj.product.price * obj.quantity);
		if (obj.serial)
			return parseFloat(obj.serial.price* obj.quantity);
		return 0;
	})

	return summa.toFixed(2);
}

