
export function getDocuments(type, s_id) {
    var s_id = s_id || "";
	return webix.ajax(base_url + "/inv/documents/" + type + "/" + s_id);
}

export function getProducts(document_id) {
	return webix.ajax(base_url + "/inv/products/" + document_id);
}

export function saveRecord(id, operation, data) {
    if (operation === "update") {
        return webix.ajax().put(base_url + "/inv/record", data);
	}

    if (operation === "insert") {
        return webix.ajax().post(base_url + "/inv/record", data);
    }

    return false;

};

export function removeDocument(document_id) {
    return webix.ajax().del(base_url + "/inv/document", {id: document_id});
}

export const Sections = new webix.DataCollection({
        url: base_url + "/data/sections",
        scheme: {
		    $init: function(obj) {
		        obj.value = obj.name
		    }
	    }
    });
