import "./styles/app.css";
import {JetApp, EmptyRouter, HashRouter } from "webix-jet";

export default class App extends JetApp{
	constructor(config){
		var app_name = APPNAME.replace("-", " ")
						.toLowerCase()
					    .split(" ")
					    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
					    .join(" ");
		const defaults = {
			id 		: APPNAME,
			name 	: app_name,
			version : VERSION,
			router 	: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
			routerPrefix: "",
			debug 	: !PRODUCTION,
			start 	: "/top/start"
		};

		window.base_url = PRODUCTION ? '' : 'http://giuvaier.test';
		window.section_id = webix.storage.session.get("section_id");
		window.user = webix.storage.session.get("user");

		window.document_selected = null;
		window.type_selected = null;

		function getAccount() {
			webix.ajax().get(base_url + '/account')
				.then(function (result) {
					let res = result.json();
		            webix.storage.session.put("user", res);
				})
				.fail(function (xhr) {
				});
		}

		webix.ready(function () {
			//getAccount();
		});

		super({ ...defaults, ...config });
	}
}

webix.i18n.setLocale('ru-RU');
webix.i18n.dateFormat = '%d.%m.%Y';
webix.i18n.fullDateFormat = '%d.%m.%Y %H:%i';
//webix.Touch.limit(true);
/*webix.attachEvent('onAjaxError',
    function(xhr) {
        if (xhr.status == 401) {
        	//location.href = base_url;
        }
    }
);*/

window.addEventListener('keydown', function() {
    try {
   		if (!popupOpened)
   			document.getElementById('barcode').focus();
    } catch(e) {}
});

if (!BUILD_AS_MODULE){
	webix.ready(() => new App().render() );
}
